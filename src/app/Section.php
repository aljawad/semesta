<?php

namespace App;

use App\Feedback;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
  use SoftDeletes, CascadeSoftDeletes;

  protected $fillable = ['chapter_id', 'name', 'description'];

  protected $cascadeDeletes = ['feedbacksdel'];

  public function chapter(){
      return $this->belongsTo('App\Chapter');
  }

  public function contents(){
    return $this->belongsToMany('App\Content','section_contents');
  }

  public function counter(){
    return $this->hasMany('App\Counter');
  }

  public function section_contents(){
    return $this->hasMany('App\SectionContent');
  }

  public function feedbacks(){
    return $this->hasMany('App\Feedback');
  }

   public function rates(){
    return $this->hasMany('App\Rate');
  }

  public function feedbacksdel()
    {
        return $this->hasMany(Feedback::class);
    }
}
