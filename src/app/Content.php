<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
	protected $fillable = ['name', 'description', 'type', 'file', 'url', 'category'];

	// public function section_content()
	// {
	// 	return $this->hasOne('App\SectionContent');
	// }

	public function section_contents(){
      return $this->hasMany('App\SectionContent');
    }

	public function sections(){
      return $this->belongsToMany('App\Content','section_contents')->withPivot('section_contents.name');
    }
}
