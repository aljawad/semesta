<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionContent extends Model
{

  protected $fillable = ['section_id', 'content_id', 'name', 'description'];

  public function section(){
  	return $this->belongsTo('App\Section');
  }

  public function content(){
  	return $this->belongsTo('App\Content');
  }
}
