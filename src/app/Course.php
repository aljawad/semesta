<?php

namespace App;

use App\Chapter;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{

    use SoftDeletes, CascadeSoftDeletes;

    protected $fillable = ['name', 'subject_id', 'ladder_id', 'teacher_id', 'description'];

    protected $dates = ['deleted_at'];

    protected $cascadeDeletes = ['chapterdel'];

    public function chapters(){
      return $this->hasMany('App\Chapter');
    }

    public function sections()
    {
        return $this->hasManyThrough('App\Section', 'App\Chapter');
    }

    public function ladder(){
      return $this->belongsTo('App\Ladder');
    }

    public function subject(){
      return $this->belongsTo('App\Subject');
    }

    public function teacher(){
      return $this->belongsTo('App\User');
    }

    public function students(){
      return $this->belongsToMany('App\User', 'course_student');;
    }

    public function chapterdel()
    {
        return $this->hasMany(Chapter::class);
    }
}
