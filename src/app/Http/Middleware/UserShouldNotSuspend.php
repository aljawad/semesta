<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Session;

class UserShouldNotSuspend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = $request->email;
        $response = $next($request);
        if (User::check() && !User()->suspended) {
            
        Session::flash("flash_notification", [
                "level"   => "warning",
                "message" => "Your Account is suspended."
            ]);

            return redirect('/login');
        }

        return $response;
    }
}
