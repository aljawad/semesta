<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SocialAccountService;
use App\GoogleAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    // redirect function
    public function redirect(){
      return Socialite::driver('facebook')->redirect();
    }

    // callback function
    public function callback(SocialAccountService $service){
      // when facebook call us a with token
      $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
      //$active = User::auth()->update(['suspended' => true]);
      auth()->login($user);
      return redirect()->to('/');
    }

    public function googleauth(){
      return Socialite::driver('google')->redirect();
    }

    // callback function
    public function googlecallback(GoogleAccountService $service){
      // when facebook call us a with token
      $user = $service->createOrGetUser(Socialite::driver('google')->user());
      //$active = User::auth()->update(['suspended' => true]);
      auth()->login($user);
      return redirect()->to('/');
    }
}
