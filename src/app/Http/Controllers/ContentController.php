<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SectionContent;
use App\Section;
use App\Content;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\File;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response;
use Zipper;
use Auth;
use Mail;
use File;

class ContentController extends Controller
{
    //
    public function index(){
        $search = \Request::get('search');

        $contents = Content::where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC')->paginate(10);
        return view('repo.list', compact('contents'));
    }

    public function add(){
    	return view('repo.add');
    }

    public function updateSectionContent(Request $request, $id){
        $sectionContent = SectionContent::find($id);
         // var_dump($request->content_id); die();
        $this->validate($request,[
            'name' => "required",
            'description' => 'required',
            'content_id' =>'required',
            ]);

        $sectionContent->fill([
            'name' => $request->name,
            'description' => $request->description,
            'content_id' => $request->content_id_id,
            ]);
        $sectionContent->save();
        
        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function htmlstore(Request $request){
        $this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimetypes:application/zip'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //var_dump($uploaded_file->getClientOriginalName(), '.'.$uploaded_file->getClientOriginalExtension()); die();
            $name = $uploaded_file->getClientOriginalName();

            $filename = pathinfo($name, PATHINFO_FILENAME);
            $extension = pathinfo($name, PATHINFO_EXTENSION);

            //var_dump($filename, $extension); die();
            //$filename = time() . '.' .$extension;
            $newDir = public_path('videos/' . $uploaded_file->getFileName());
            File::makeDirectory( $newDir, 0755, true);
            Zipper::make($request->file('file'))->folder($filename)->extractTo($newDir);
            //Storage::disk('public')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $uploaded_file->getClientOriginalName();
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $filename.'.html';
            $content->url = $uploaded_file->getFileName(). '/';
            $content->category = $request->category;
            $content->save();

            $sectionContent = new SectionContent();
            $sectionContent->name = $request->name;
            $sectionContent->description = $request->description;
            $sectionContent->section_id = $request->section_id;
            $sectionContent->content_id = $content->id;
            $sectionContent->save();
        }

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name. '' );
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function quizstore(Request $request){
        $this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimetypes:application/zip'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //var_dump($uploaded_file->getClientOriginalName(), '.'.$uploaded_file->getClientOriginalExtension()); die();
            $name = $uploaded_file->getClientOriginalName();

            $filename = pathinfo($name, PATHINFO_FILENAME);
            $extension = pathinfo($name, PATHINFO_EXTENSION);

            //var_dump($filename, $extension); die();
            //$filename = time() . '.' .$extension;
            $newDir = public_path('quiz/' . $uploaded_file->getFileName());
            File::makeDirectory( $newDir, 0755, true);
            Zipper::make($request->file('file'))->extractTo($newDir);
            //Storage::disk('public')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $uploaded_file->getClientOriginalName();
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $filename.'.htm';
            $content->url = $uploaded_file->getFileName(). '/';
            $content->category = $request->category;
            $content->save();

            $sectionContent = new SectionContent();
            $sectionContent->name = $request->name;
            $sectionContent->description = $request->description;
            $sectionContent->section_id = $request->section_id;
            $sectionContent->content_id = $content->id;
            $sectionContent->save();
        }

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name.'');
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function detil($id){
        $content = SectionContent::find($id);
        return view('section.detil')
            ->with('content', $content);
    }

    public function detilquiz($id){
        $content = SectionContent::find($id);
        return view('section.quiz')
            ->with('content', $content);
    }

    public function pdfstore(Request $request){

    	$this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimes:pdf'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //$filename = time() . '.' .$extension;

            Storage::disk('public')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $uploaded_file->getClientOriginalName();
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $uploaded_file->getFileName() . '.' .$extension;
            $content->category = $request->category;
            $content->save();

            $sectionContent = new SectionContent();
            $sectionContent->name = $request->name;
            $sectionContent->description = $request->description;
            $sectionContent->section_id = $request->section_id;
            $sectionContent->content_id = $content->id;
            $sectionContent->save();
        }

        //var_dump($content->id); die();

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name.'');
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function videostore(Request $request){

        $this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimes:mp4'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //$filename = time() . '.' .$extension;

            Storage::disk('video')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $uploaded_file->getClientOriginalName();
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $uploaded_file->getFileName() . '.' .$extension;
            $content->category = $request->category;
            $content->save();

            $sectionContent = new SectionContent();
            $sectionContent->name = $request->name;
            $sectionContent->description = $request->description;
            $sectionContent->section_id = $request->section_id;
            $sectionContent->content_id = $content->id;
            $sectionContent->save();
        }

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name.'');
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function view($id){
    	$content = Content::find($id);
    	$fileContents = Storage::disk('public')->get($content->file);

    	//var_dump($content->type); die();

    	$response = Response::make($fileContents, 200);
        $response->header('Content-Type', $content->type);
    return $response;
    }

    public function getVideo($id)
    {   
        $content = Content::find($id);
        //$name = $content->file;
        $fileContents = Storage::disk('video')->get($content->file);
        $response = Response::make($fileContents, 200);
        $response->header('Content-Type', $content->type);
    return $response;
    }

    public function deletepdf(Request $request, $id)
    {
        $content = Content::find($id);
        $file = $content->file;
        $filepath = storage_path() .'/app/public/pdf/';
        

        try {
            $content->delete();
            File::delete($filepath . $file);
            } 
        catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                    return redirect()->back()
                      ->with('DELETE.DEACTIVATE', true);
                }
            }
            return redirect()->back()
                    ->with('DELETE.OK', true);

    }

    public function deletevideo(Request $request, $id)
    {
        $content = Content::find($id);
        $file = $content->file;
        $filepath = storage_path() .'/app/public/video/';
        try {
            $content->delete();
            File::delete($filepath . $file);
            } 
        catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                    return redirect()->back()
                      ->with('DELETE.DEACTIVATE', true);
                }
            }
            return redirect()->back()
                    ->with('DELETE.OK', true);
    }

    public function deletehtml(Request $request, $id)
    {
        $content = Content::find($id);
        $dir = $content->url;
        $filepath = public_path() .'/videos/';
        try {
            $content->delete();
            File::deleteDirectory($filepath . $dir);
            } 
        catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                    return redirect()->back()
                      ->with('DELETE.DEACTIVATE', true);
                }
            }
            return redirect()->back()
                    ->with('DELETE.OK', true);

    }

    public function deletequiz(Request $request, $id)
    {
        $content = Content::find($id);
        $dir = $content->url;
        $filepath = public_path() .'/quiz/';
        try {
            $content->delete();
            File::deleteDirectory($filepath . $dir);
            } 
        catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                    return redirect()->back()
                      ->with('DELETE.DEACTIVATE', true);
                }
            }
            return redirect()->back()
                    ->with('DELETE.OK', true);
    }

    public function deleteSectionContent($id){
        $sectionContent = SectionContent::find($id);
        $sectionContent->delete();
        return redirect()->back()
                    ->with('DELETE.OK', true);
    }

    public function picklist(Request $request)
    {
    $q = $request->q;
    $lists = Content::where('name', 'like', "%$q%")
        ->orderBy('id')->paginate('5');
    return view('chapter._content_list', ['lists' => $lists]);

    }

    public function existing(Request $request){
        $this->validate($request, [
            'name'     => 'required',
            'content_id' => 'required',
            //'description' => 'required'
        ]);
            $content = new SectionContent();
            $content->name = $request->name;
            $content->section_id = $request->section_id;
            $content->description = $request->description;
            $content->content_id = $request->content_id_id;
            $content->save();

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name.'');
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }
}
