<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use Auth;
use Laratrust;
use App\Slider;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Laratrust::hasRole(['admin', 'sekolah']))
        {
        $user = User::Where('role', '=', 'member')->count();
        $teacher = User::Where('role', '=', 'teacher')->count();
        $sekolah = User::Where('role', '=', 'sekolah')->count();
        $course = Course::count();
        return view('home')
        ->with('user', $user)
        ->with('teacher', $teacher)
        ->with('sekolah', $sekolah)
        ->with('course', $course);
        }elseif(Laratrust::hasRole('teacher')){
        $id = Auth::user()->id;
        $course = Course::Where('teacher_id', $id)->count();
        return view('home')
        ->with('course', $course);
        }elseif(Laratrust::hasRole('sekolah')){
        $id = Auth::user()->id;
        $teacher = User::Where('role', '=', 'teacher')->Where('sekolah_id', '=', $id)->count();
        return view('home')
        ->with('teacher', $teacher);
        }else{
            return view('errors.403');
        }
    }

    public function frontend(){
        $sliders = Slider::get();
        //return response()->json($sliders);
        return view('frontend.index',compact('sliders'));
    }

    public function about(){
        return view('frontend.about_us');
    }
}
