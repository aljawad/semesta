<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\Subject;	
use App\Slider;
use Storage;
use File;
use Response;

class ConfigController extends Controller
{
    //
    public function featured(){
    	$featured = Setting::find(1);

    	$featured_subject_1 = Subject::find($featured->featured_subject_1);
    	$featured_subject_2 = Subject::find($featured->featured_subject_2);
    	$featured_subject_3 = Subject::find($featured->featured_subject_3);
    	$featured_subject_4 = Subject::find($featured->featured_subject_4);
    	//var_dump($featured_subject_1->name);

    	//var_dump($featured->featured_subject_2);
    	return view('config.featured')
    	->with('featured', $featured)
    	->with('featured_subject_1', $featured_subject_1)
    	->with('featured_subject_2', $featured_subject_2)
    	->with('featured_subject_3', $featured_subject_3)
    	->with('featured_subject_4', $featured_subject_4);
    }

    public function featuredStore(Request $request){
    	$featured = Setting::find(1);

    	$featured ->fill([
            'featured_subject_1' => $request->featured_subject_1,
            'featured_subject_2' => $request->featured_subject_2,
            'featured_subject_3' => $request->featured_subject_3,
            'featured_subject_4' => $request->featured_subject_4,
        ]);
        $featured->save();

        return redirect()->back()
        	->with('UPDATE.OK', true);
    }

    public function website(){
    	$featured = Setting::find(1);

    	return view('config.website')
    	->with('featured', $featured);

    }

    public function websiteStore(Request $request){
    	$featured = Setting::find(1);

    	$video = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $request->video_url);

    	$featured->fill([
    		// 'address_1' => $request->address_1,
    		// 'address_2' => $request->address_2,
    		// 'address_3' => $request->address_3,
    		'phone' => $request->phone,
    		'fax' => $request->fax,
    		'email' => $request->email,
    		'facebook' => $request->facebook,
    		'twitter' => $request->twitter,
    		'youtube' => $request->youtube,
    		'instagram' => $request->instagram,
    		'video_url' => $video,

    		]);
    	$featured->save();

    	return redirect()->back()
        	->with('UPDATE.OK', true);
    }

    public function slider(){
    	$sliders = Slider::get();

    	return view('config.slider', compact('sliders'));

    }

    public function sliderStore(Request $request){

    	$this->validate($request, [
	    'name'=>'required|unique:sliders,name',
	    'image' => 'required|image',
	    ]);

    	if ($request->hasFile('image')) {
            # code...      
            $uploaded_file = $request->file('image');
            $extension = $uploaded_file->getClientOriginalExtension();
            //$filename = time() . '.' .$extension;

            Storage::disk('slider')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $slider = new Slider();
            $slider->name = $request->name;
            $slider->description = $request->description;
            $slider->image = $uploaded_file->getFileName() . '.' .$extension;
            $slider->status = $request->status;
            $slider->save();
        }

        return redirect()->back()->with('UPDATE.OK', true);
    }

    public function images($images){

            $path = storage_path() . '/app/public/slider/' . $images;

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;

    }

    public function editSlider($id){
    	$slider = Slider::findOrFail($id);

    	// var_dump($slider->id); die();

    	return view('config.editslider', compact('slider'));
    }


    public function updateSlider(Request $request, $id){
       $slider = Slider::findOrFail($id);

       // var_dump($request->status); die();
       
       $this->validate($request, [
        'name'=>'required|unique:sliders,name,' . $id,
        'image' => 'image',
        ]);

       if ($request->hasFile('image')) {
                # code...      
        $uploaded_file = $request->file('image');
        $extension = $uploaded_file->getClientOriginalExtension();
                //$filename = time() . '.' .$extension;

        Storage::disk('slider')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

        $slider->fill([

            'name' => $request->name,
            'description' => $request->description,
            'image' => $uploaded_file->getFileName() . '.' .$extension,
            'status' => $request->status,

            ]); 
        $slider->save();
        }
        else{
            $slider->fill([

            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,

            ]); 
        $slider->save();
        }

    return redirect()->action('ConfigController@slider')
    ->with('UPDATE.OK', true);
    }


    public function deleteSlider($id){

        $slider = Slider::find($id);
        $file = $slider->image;
        $filepath = storage_path() .'/app/public/slider/';
        File::delete($filepath . $file);
        $slider->delete();


        return redirect()->back()
            ->with('DELETE.OK', true);
    }
}
