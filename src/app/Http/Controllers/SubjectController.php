<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Ladder;
use Laratrust;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response;
use Image;

class SubjectController extends Controller
{
    //
    public function index(){
        $search = \Request::get('search');

        if (Laratrust::hasRole('admin')){
           $subjects = Subject::where('name','like','%'.$search.'%')
           ->orderBy('id', 'DESC')->paginate(10);

           return view('subject.list', compact('subjects'));
       }
       return view('errors.403');
   }

   public function add(){
    if (Laratrust::hasRole('admin')){
    	return view('subject.add');
    }
    return view('errors.403');
}

    public function store(Request $request){

         // $sub_youtube = substr($request->url,32);
         $youtube = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $request->url);

        //var_dump($youtube); die();

      $this->validate($request, [
        'name'=>'required|unique:subjects,name',
        'images' => 'required|image',
        ]);

      if ($request->hasFile('images')) {
                # code...      
        $uploaded_file = $request->file('images');
        $extension = $uploaded_file->getClientOriginalExtension();
                //$filename = time() . '.' .$extension;

        Storage::disk('thumbnail')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

        $subject = new Subject();
        $subject->name = $request->name;
        $subject->url = $youtube;
        $subject->images = $uploaded_file->getFileName() . '.' .$extension;
        $subject->save();

    }

    return redirect()->action('SubjectController@index')
    ->with('UPDATE.OK', true);
    }

    public function edit(Request $request, $id){
        $subject = Subject::find($id);
        return view('subject.edit')
        ->with('subject', $subject);
    }

    public function update(Request $request, $id){
       $subject = Subject::find($id);

       $youtube = str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $request->url);
       
       $this->validate($request, [
        'name'=>'required|unique:subjects,name,' . $id,
        'images' => 'image',
        ]);

       if ($request->hasFile('images')) {
                # code...      
        $uploaded_file = $request->file('images');
        $extension = $uploaded_file->getClientOriginalExtension();
                //$filename = time() . '.' .$extension;

        Storage::disk('thumbnail')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

        $subject->fill([

            'name' => $request->name,
            'url' => $youtube,
            'images' => $uploaded_file->getFileName() . '.' .$extension,

            ]); 
        $subject->save();
        }
        else{
            $subject->fill([

            'name' => $request->name,
            'url' => $youtube,

            ]); 
        $subject->save();
        }

    return redirect()->action('SubjectController@index')
    ->with('UPDATE.OK', true);
    }

    public function picklist(Request $request)
    {
        $q = $request->q;
        $lists = Subject::where('name', 'like', "%$q%")
        ->orderBy('id')->paginate('5');

            //var_dump($lists); die();
        return view('subject._subject_list', ['lists' => $lists]);

    }

    public function delete($id){
        $subject = Subject::findOrFail($id);
        
        try {
            $subject->delete();
        } 
        catch (\Illuminate\Database\QueryException $e) {

                    if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                        // return error to user here
                        return redirect()->back()
                        ->with('DELETE.DEACTIVATE', true);
                    }

                }
                return redirect()->back()
                ->with('DELETE.OK', true);                
            }

    public function all()
    {
        $subjects = Subject::where('status', '=', 'active')->get();

        return view('frontend.mapel', compact('subjects'));
    }

    public function show($id)
    {
        $subject = Subject::find($id);
        $ladders = Ladder::where('status', '=', 'active')->get();

        if ($subject->status == 'inactive') {
            # code...
            return view('errors.403');
        }

        return view('frontend.mapel_selected', compact('subject', 'ladders', 'id'));
    }

    public function images($images){

        $path = storage_path() . '/app/public/thumbnail/' . $images;

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;

    }

    public function active($id){
        $subject = Subject::findOrFail($id);

        $subject = Subject::where('id', $id)->update(['status' => 'active']);

        return redirect()->back()->with('UNSUSPEND.OK', true);
    }

    public function inactive($id){
        $subject = Subject::findOrFail($id);

        $subject = Subject::where('id', $id)->update(['status' => 'inactive']);

        return redirect()->back()->with('SUSPEND.OK', true);
    }
}