<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Ladder;
use App\Subject;
use App\User;
use Laratrust;
use Role;
use Auth;

class CourseController extends Controller
{
    //
    public function index(Request $request){

        $subject = $request->subject;
        $search = $request->search;

        $id = Auth::user()->id;
        if (Laratrust::hasRole(['admin', 'sekolah'])){
        $courses = Course::where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC');
            if($search){
            $courses = $courses->where(function($q) use ($search) {
                $q->where('role', '=', 'teacher')
                ->where('name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
            });
        }
        if($subject && $subject != 'ALL'){
            $courses = $courses->where('subject_id','=',$subject);
        }
          $courses = $courses->paginate(10)->appends($request->all());
        return view('course.list',compact('courses'));
        }elseif (Laratrust::hasRole('teacher')){
        $courses = Course::where('teacher_id', '=', $id)
        ->where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC');
            if($search){
            $courses = $courses->where(function($q) use ($search) {
                $q->where('role', '=', 'teacher')
                ->where('name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
            });
        }
        if($subject && $subject != 'ALL'){
            $courses = $courses->where('subject_id','=',$subject);
        }
          $courses = $courses->paginate(10)->appends($request->all());
        return view('course.list',compact('courses'));
        }

		return view('errors.403');
    }

    public function trash(){

        $id = Auth::user()->id;
        if (Laratrust::hasRole('admin')){
        $search = \Request::get('search');
        $course = Course::onlyTrashed()
            ->where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC')->paginate(10);
        }else{
        $search = \Request::get('search');
        $course = Course::onlyTrashed()
            ->where('teacher_id', '=', $id)
            ->where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC')->paginate(10);
        }
        return view('course.trash',compact('course'));
    }

    public function show($id){
      $course = Course::with('ladder','chapters.sections','subject','students')->findOrFail($id);
      $course->is_joined = false;

      foreach ($course->students as $student) {
        # code...
        if($student->id == Auth::user()->id){
          $course->is_joined = true;
        }
      }

      if (($course->ladder->status == 'inactive') or ($course->subject->status == 'inactive') ) {
          # code...
        return view('errors.403');
      }
    //  return response()->json($course);
      return view('frontend.courses',compact('course'));
    }

    public function join($id){
      $course = Course::findOrFail($id);
      $member = $course->students()->where('users.id', Auth::user()->id)->count();
      if($member>0){
      }else{
        $course->students()->attach(Auth::user()->id);
      }
      return redirect()->action(
          'CourseController@show', ['id' => $id]
      );
    }


    public function leave($id){
        $course = Course::findOrFail($id);
        $course->students()->detach(Auth::user()->id);
        return redirect()->action(
            'CourseController@show', ['id' => $id]
        );
      }


    public function add(){
       if (Laratrust::hasRole(['admin', 'teacher', 'sekolah']))
            {
    	       return view('course.add');
           }
           return view('errors.403');
    }

    public function store(Request $request){
    	$this->validate($request, [
            'name'=>'required',
            'subject_id' =>'required',
            'teacher_id' => 'required',
            'ladder_id' =>'required',
            ]);

    	$course = Course::create([
            'name' => $request->name,
            'subject_id' => $request->subject_id,
            'ladder_id' => $request->ladder_id,
            'description' => $request->description,
            'teacher_id' => $request->teacher_id,
        ]);

        return redirect()->action('CourseController@index')
                        ->with('UPDATE.OK', true);
    }

    public function edit(Request $request, $id){
        $course = COurse::find($id);
        if(Laratrust::hasRole(['admin', 'sekolah']))
        {
            return view('course.edit')->with('course', $course);
        }
        $id_teacher = Auth::user()->id;
        if($course->teacher_id == $id_teacher){
        return view('course.edit')->with('course', $course);
        }
        return view('errors.403');
    }

    public function update(Request $request, $id){
    	$course = Course::find($id);

        //var_dump($request); die();

    	$this->validate($request, [
            'name'=>'required',
            'subject_id' =>'required',
            'ladder_id' =>'required',
            ]);

    	$course ->fill([
            'name' => $request->name,
            'subject_id' => $request->subject_id,
            'ladder_id' => $request->ladder_id,
            'description' => $request->description,
            'teacher_id' => $request->teacher_id,
        ]);
        $course->save();

        return redirect()->action('CourseController@index')
                        ->with('UPDATE.OK', true);
    }

    public function delete($id){
        if(Laratrust::hasRole(['admin', 'sekolah'])){
            $course = Course::withTrashed()->findOrFail($id);
            $course->forcedelete();


        return redirect()->action('CourseController@trash')
                        ->with('DELETE.OK', true);
        }

        $id_teacher = Auth::user()->id;
        if($course->teacher_id == $id_teacher){
            $ $course = Course::withTrashed()->findOrFail($id);
            $course->forcedelete();


        return redirect()->action('CourseController@trash')
                        ->with('DELETE.OK', true);
        }

        return view('errors.403');


    }

    public function softdelete($id){
        $course = Course::findOrFail($id);
        if(Laratrust::hasRole('admin')){
            $course->delete();
            return redirect()->action('CourseController@index')
                        ->with('MOVE.OK', true);
        }
        $id_teacher = Auth::user()->id;
        if($course->teacher_id == $id_teacher){
        $course->delete();

        return redirect()->action('CourseController@index')
                        ->with('MOVE.OK', true);
        }
        return view('errors.403');
    }

    public function restore($id){
        $teacher_id = Auth::user()->id;
        if(Laratrust::hasRole(['admin', 'sekolah'])){
        $course = Course::withTrashed()->findOrFail($id)->restore();;

            return redirect()->action('CourseController@trash')
                        ->with('RESTORE.OK', true);
        }
        if($course->teacher_id == $teacher_id){
            $course = Course::withTrashed()->findOrFail($id)->restore();;

            return redirect()->action('CourseController@trash')
                        ->with('RESTORE.OK', true);
        }
            return view('errors.403');

    }

    public function mystudent(Request $request, $id){
        $search = $request->search;
        $course = Course::findOrFail($id);

        $count = Course::where('name', $course->name)->first()->students()->count();
        if ($search) {
            # code...
            $students = Course::where('name', $course->name)->first()->students()->where('name', 'like', '%'.$search.'%')->get();
        }else{
            $students = Course::where('name', $course->name)->first()->students;
        }

        // var_dump($students->count()); die();

        return view('course.student', compact(['course', 'students', 'count']));

    }

}
