<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use App\Section;
use Auth;
use Laratrust;
use Session;
use Mail;

class FeedbackController extends Controller
{
    //
    public function index(Request $request){
        $section = $request->section;
        $chapter = $request->chapter;
        $course = $request->course;
        $subject = $request->subject;
        $status = $request->status;

        $a = Feedback::find(2);

        // var_dump($a->section->chapter->course->subject->name); die();
        
    	if (Laratrust::hasRole('admin')){
           $feedbacks = Feedback::orderBy('id', 'DESC')->where('parent_id','=', 0);

            if($section && $section != 'ALL'){
            $feedbacks = $feedbacks->where('section_id','=',$section);
        }
        //     if($chapter && $chapter != 'ALL'){
        //     $feedbacks = $feedbacks sdfsdgldfmglkdsfhskj;
        // }

            if($status && $status != 'ALL'){
            $feedbacks = $feedbacks->where('status','=',$status);
        }

          $feedbacks = $feedbacks->paginate(10)->appends($request->all());

           return view('feedback.list_feedback', compact('feedbacks'));
       }
       return view('errors.403');
    }

    public function store(Request $request, $id){
		
		//var_dump($request->section_id);

		$this->validate($request, [
            'comment'=>'required'
            ]);

		$feedbacks = Feedback::create([
			'section_id' => $request->section_id,
			'user_id' => Auth::user()->id,
            'comment' => $request->comment,
            'status' => 'pending',
        ]);

        Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Thank you, your feedback is waiting for moderation."
            ]);

        return redirect()->back()
                        ->with('UPDATE.OK', true);
	}

    public function approve($id){
        $feedbacks = Feedback::find($id);
        $feedbacks = Feedback::where('id', $id)->update(['status' => 'published']);

        return redirect()->action('FeedbackController@index')
                        ->with('APPROVE.OK', true);
    }

    public function unapprove($id){
        $feedbacks = Feedback::find($id);
        $feedbacks = Feedback::where('id', $id)->update(['status' => 'pending']);

        return redirect()->action('FeedbackController@index')
                        ->with('UNUPROVE.OK', true);
    }

    public function view($id){
    	$feedbacks = Feedback::findOrFail($id);
    	// $section = Section::find($id);

    	// var_dump($section); die();

        
    	$reply = Feedback::where('parent_id', '=', $id)->orderBy('id')->get();
    	// var_dump($feedbacks); die();

    	return view('feedback.reply')
    	->with('feedbacks',$feedbacks)
    	->with('reply', $reply);
    }

    public function delete($id){
    	$feedbacks = Feedback::findOrFail($id);
    	$feedbacks->delete();

    	return redirect()->back()
                    ->with('DELETE.OK', true);
    }

    public function reply(Request $request, $id){
    	$feedbacks1 = Feedback::findOrFail($id);
    	
    	$this->validate($request, [
    		'comment'=>'required'
    		]);

    	$feedbacks = Feedback::create([
    		'section_id' => $feedbacks1->section_id,
    		'comment' => $request->comment,
    		'user_id' => Auth::user()->id,
    		'status' => 'published',
    		'parent_id' => $feedbacks1->id
    		]);
    	$feedbacks1 = Feedback::where('id', $id)->update(['status' => 'published']);

        $user = Feedback::findOrFail($id);

        Mail::send('mail.feedback', compact('user', 'feedbacks'), function ($m) use ($user) {
            $m->to($user->user->email, $user->user->name)->subject('[Semesta Web TV] Your feedback was replied ');
        });

    	return redirect()->back()
                    ->with('UPDATE.OK', true);
    }

    public function updateReply(Request $request, $id){
        $reply = Feedback::findOrFail($id);

        $reply->fill([
            'comment' => $request->comment,
            ]);

        $reply->save();

        return redirect()->back()->with('UPDATE.OK', true);
    }

    public function deleteReply($id){
        $reply = Feedback::findOrFail($id);

        $reply->delete();

        return redirect()->back()->with('DELETE.OK', true);
    }
}
