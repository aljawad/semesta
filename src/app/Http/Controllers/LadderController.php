<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ladder;
use Laratrust;

class LadderController extends Controller
{
    //
    public function index(){
        $search = \Request::get('search');
        if (Laratrust::hasRole('admin')){
        $ladders = Ladder::where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC')->paginate(10);

		return view('ladder.list',compact('ladders'));
        }
        return view('errors.403');
	}

	public function add(){
        if (Laratrust::hasRole('admin')){
		return view('ladder.add');
        }
        return view('errors.403');
	}

	public function store(Request $request){
		$this->validate($request, [
            'name'=>'required|unique:ladders,name'
            ]);

		$ladder = Ladder::create([
            'name' => $request->name,
        ]);

        return redirect()->action('LadderController@index')
                        ->with('UPDATE.OK', true);
	}

	public function edit(Request $request, $id){
        $ladder = Ladder::find($id);
        return view('ladder.edit')->with('ladder', $ladder);
    }

    public function update(Request $request, $id){
    	$ladder = Ladder::find($id);
		
		$this->validate($request, [
            'name'=>'required|unique:ladders,name,' . $id,
            ]);

        $ladder->fill([
        	'name' => $request->name,
        ]);
      	$ladder->save();

      	return redirect()->action('LadderController@index')
                      ->with('UPDATE.OK', true);
    }

    public function picklist(Request $request)
    {
    $q = $request->q;
    $lists = Ladder::where('name', 'like', "%$q%")
        ->orderBy('id')->paginate('5');

        //var_dump($lists); die();
    return view('ladder._ladder_list', ['lists' => $lists]);

    }

    public function delete($id){
        $ladder = Ladder::find($id);
        //if($ladder->delete()){
            
        //    return redirect()->back()
        //                ->with('DELETE.DEACTIVATE', true);
        //}
        //$ladder->delete();

        try {
             $ladder->delete();
            } 
        catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                    return redirect()->back()
                      ->with('DELETE.DEACTIVATE', true);
                }
            }
            return redirect()->back()
                    ->with('DELETE.OK', true);
        
    }

    public function active($id){
        $ladder = Ladder::findOrFail($id);

        $ladder = Ladder::where('id', $id)->update(['status' => 'active']);

        return redirect()->back()->with('UNSUSPEND.OK', true);
    }

    public function inactive($id){
        $ladder = Ladder::findOrFail($id);

        $ladder = Ladder::where('id', $id)->update(['status' => 'inactive']);

        return redirect()->back()->with('SUSPEND.OK', true);
    }    	
    
}
