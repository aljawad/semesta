<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SectionContent;
use App\Section;
use App\Content;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\File;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response;
use Zipper;
use Auth;
use Mail;
use File;

class RepoController extends Controller
{
    //
	public function index(Request $request){
		$category = $request->category;
        $search = $request->search;

        $contents = Content::where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC');
            if($search){
            $contents = $contents->where(function($q) use ($search) {
                $q->where('name', 'like', "%$search%");
                });
            }
            if($category && $category != 'ALL'){
            $contents = $contents->where('category','=',$category);
            }
            $contents = $contents->paginate(10)->appends($request->all());
        return view('repo.list', compact('contents'));
	}

    public function htmlstore(Request $request){
        $this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimetypes:application/zip'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //var_dump($uploaded_file->getClientOriginalName(), '.'.$uploaded_file->getClientOriginalExtension()); die();
            $name = $uploaded_file->getClientOriginalName();

            $filename = pathinfo($name, PATHINFO_FILENAME);
            $extension = pathinfo($name, PATHINFO_EXTENSION);

            //var_dump($filename, $extension); die();
            //$filename = time() . '.' .$extension;
            $newDir = public_path('videos/' . $uploaded_file->getFileName());
            File::makeDirectory( $newDir, 0755, true);
            Zipper::make($request->file('file'))->folder($filename)->extractTo($newDir);
            //Storage::disk('public')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $request->name;
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $filename.'.html';
            $content->url = $uploaded_file->getFileName(). '/';
            $content->category = $request->category;
            $content->save();
        }

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name. '' );
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function quizstore(Request $request){
        $this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimetypes:application/zip'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //var_dump($uploaded_file->getClientOriginalName(), '.'.$uploaded_file->getClientOriginalExtension()); die();
            $name = $uploaded_file->getClientOriginalName();

            $filename = pathinfo($name, PATHINFO_FILENAME);
            $extension = pathinfo($name, PATHINFO_EXTENSION);

            //var_dump($filename, $extension); die();
            //$filename = time() . '.' .$extension;
            $newDir = public_path('quiz/' . $uploaded_file->getFileName());
            File::makeDirectory( $newDir, 0755, true);
            Zipper::make($request->file('file'))->extractTo($newDir);
            //Storage::disk('public')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $request->name;
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $filename.'.htm';
            $content->url = $uploaded_file->getFileName(). '/';
            $content->category = $request->category;
            $content->save();
        }

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name.'');
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function detil($id){
        $content = SectionContent::find($id);
        return view('section.detil')
            ->with('content', $content);
    }

    public function detilquiz($id){
        $content = SectionContent::find($id);
        return view('section.quiz')
            ->with('content', $content);
    }

    public function pdfstore(Request $request){

    	$this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimes:pdf'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //$filename = time() . '.' .$extension;

            Storage::disk('public')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $request->name;
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $uploaded_file->getFileName() . '.' .$extension;
            $content->category = $request->category;
            $content->save();
        }

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name.'');
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function videostore(Request $request){

        $this->validate($request, [
            'name'     => 'required',
            'file'     => 'required|mimes:mp4'
            //'description' => 'required'
        ]);
        if ($request->hasFile('file')) {
            # code...      
            $uploaded_file = $request->file('file');
            $extension = $uploaded_file->getClientOriginalExtension();
            //$filename = time() . '.' .$extension;

            Storage::disk('video')->put($uploaded_file->getFileName().'.'.$extension,  File::get($uploaded_file));

            $content = new Content();
            $content->name = $request->name;
            $content->description = $request->description;
            $content->type = $uploaded_file->getClientMimeType();
            $content->file = $uploaded_file->getFileName() . '.' .$extension;
            $content->category = $request->category;
            $content->save();
        }

        $teacher = Auth::user()->name;
        $section = $request->section_id;

        Mail::send('mail.notif', compact('content', 'teacher', 'section'), function ($m) use ($content) {
            $m->to('admin@semestaweb.tv', 'Admin Semesta')->subject('[Semesta Web TV] New Content Uploaded from '.Auth::user()->name.'');
        });

        return redirect()->back()
            ->with('UPDATE.OK', true);
    }

    public function view($id){
    	$content = SectionContent::find($id);
    	$fileContents = Storage::disk('public')->get($content->file);

    	//var_dump($content->type); die();

    	$response = Response::make($fileContents, 200);
        $response->header('Content-Type', $content->type);
    return $response;
    }

    public function getVideo($id)
    {   
        $content = SectionContent::find($id);
        //$name = $content->file;
        $fileContents = Storage::disk('video')->get($content->file);
        $response = Response::make($fileContents, 200);
        $response->header('Content-Type', $content->type);
    return $response;
    }

    public function update(Request $request, $id){
        $content = Content::findOrFail($id);

        $this->validate($request,[
            'name'     => 'required',
            'description' => 'required'
            ]);

        $content->fill([
            'name' => $request->name,
            'description' => $request->description,
            ]);
        $content->save();

        return redirect()->back()->with('UPDATE.OK', true);
    }

    public function deletepdf(Request $request, $id)
    {
        $content = SectionContent::find($id);
        $file = $content->file;
        $filepath = storage_path() .'/app/public/pdf/';
        File::delete($filepath . $file);
        $content->delete();


        return redirect()->back()
            ->with('DELETE.OK', true);
    }

    public function deletevideo(Request $request, $id)
    {
        $content = SectionContent::find($id);
        $file = $content->file;
        $filepath = storage_path() .'/app/public/video/';
        File::delete($filepath . $file);
        $content->delete();


        return redirect()->back()
            ->with('DELETE.OK', true);
    }

    public function deletehtml(Request $request, $id)
    {
        $content = SectionContent::find($id);
        $dir = $content->url;
        $filepath = public_path() .'/videos/';
        File::deleteDirectory($filepath . $dir);
        $content->delete();


        return redirect()->back()
            ->with('DELETE.OK', true);
    }

    public function deletequiz(Request $request, $id)
    {
        $content = SectionContent::find($id);
        $dir = $content->url;
        $filepath = public_path() .'/quiz/';
        File::deleteDirectory($filepath . $dir);
        $content->delete();


        return redirect()->back()
            ->with('DELETE.OK', true);
    }
}
