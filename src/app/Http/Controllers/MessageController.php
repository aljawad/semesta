<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{
    //
    public function index(){
    	$message = Message::orderBy('id', 'DESC')->paginate(10);

    	return view('message.list', compact('message'));
    }

    public function store(Request $request){
      $out = new \stdClass();
      try {
        $message = Message::create([
          'name' => $request->json()->get('name'),
          'email' => $request->json()->get('email'),
          'message' => $request->json()->get('message'),
          ]);
        $out->message = "Your message succesfully sent!";
        $out->status = "success";
      } catch (\Illuminate\Database\QueryException $e){
          $out->message = "Please fill all field(s)";
          $out->status = "warning";
      } catch (\Exception $e) {
        $out->message = $e;
        $out->status = "warning";
      }



    	return response()->json($out);
    }
}
