<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate;
use Auth;

class RateControllers extends Controller
{
    //
    function index(Request $request){
      $out = array();
        if($request->sect_id  != null ||$request->sect_id  != ''){
            $rate = Rate::where('section_id',$request->sect_id);
            if($request->withUser  == 'true'){
                $rate = $rate->where('user_id',Auth::user()->id);
                $out['user_id'] = Auth::user()->id;
            }
            $out['section_id'] = $request->sect_id;
            $out['rate'] = ($rate->avg('value')?$rate->avg('value'):0);
            return response()->json($out);
        }else{
        }
    }

    function update(Request $request){
      $user_id = Auth::user()->id;
      $section_id = $request->json()->get('section_id');
      $user_rate = $request->json()->get('value');
      Rate::where('section_id',$section_id)->where('user_id',$user_id)->delete();
      $rate =  new Rate;
      $rate->section_id = $section_id;
      $rate->user_id = $user_id;
      $rate->value = $user_rate;
      $rate->save();
      $rate->where('section_id',$section_id);
      return response()->json(array('status'=>'ok','rate'=>$rate->avg('value')));
    }
}
