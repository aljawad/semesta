<?php

namespace App;

use App\Section;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chapter extends Model
{
	use SoftDeletes, CascadeSoftDeletes;

    protected $fillable = ['name', 'course_id', 'description'];

    protected $cascadeDeletes = ['sectiondel'];

    public function course(){
      return $this->belongsTo('App\Course');
    }

    public function sections(){
    	return $this->hasMany('App\Section');
    }

    public function sectiondel()
    {
        return $this->hasMany(Section::class);
    }
}
