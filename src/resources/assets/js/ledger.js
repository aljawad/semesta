$(function(){
	var ledger_accounts_add = JSON.parse($('#ledger-accounts-add').val());
	var transaction_add = JSON.parse($('#transaction-add').val());
	var bookId = parseInt($("#bookId").val() ,10);
	var accounts = [];
	var ledger_lists = [];
	var check_book = [];

	$("#filter_account").off('keyup').on('keyup', filterAccountAuto);

	function render() 
	{
		ledger_accounts_edit = [];
		transaction_edit = [];
		getLedgerData();
		getCheckBook();
		getIndex();
		$('#ledger-accounts-add').val(JSON.stringify(ledger_accounts_add));
		$('#transaction-add').val(JSON.stringify(transaction_add));
		$('#ledger-list').val(JSON.stringify(ledger_lists));
		$('#ledger-accounts-edit').val(JSON.stringify(ledger_accounts_edit));
		$('#transaction-edit').val(JSON.stringify(transaction_edit));
		var tmpl = $('#tmplate').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { ledger_add : ledger_accounts_add, transaction_add: transaction_add, ledger_list: ledger_lists, check_book: check_book });
		$('#table-data').html(html);

		bind();
		hitungBalance();
	}

	function bind(){
		inputDate(); // from app.js
		$(".input-edit-account").on('keyup', accountEditAutoComplete);
		$(".input-trans-date").on('keyup',saveNewTrans);
		$(".input-trans-desc").on('keyup',saveNewTrans);
		$("#saveTrans").off('click').on('click', saveNewTrans);
		$(".input-save").on('keyup', saveNewTrans);
		$(".remove-ledger-account").off('click').on('click', deleteAccountLedger);
		$(".input-new").on('keyup', hitungBalance);
		$(".count-edit-balance").on('keyup', hitungBalanceEdit);
		$(".input-change").on('keyup', checkAddNewLedger);
		$(".input-edit-change").on('keyup', checkEditLedger);
		$(".add-new").off('click').on('click', addFormLedgerAccount);
		$("#saveAll").off('click').on('click', saveNewLedger);
		$(".tmbl-save-edit-ledger").off('click').on('click', saveEditLedger);

		$(".edit-transaction").off('click').on('click', showEdit);
		//$(".cancel-transaction").on('click', cancelEdit);
		$(".cancel-transaction").off('click').on('click', render);
		$("#filterBtn").off('click').on('click', render);
		$(".save-transaction").off('click').on('click', saveEdit);
		$(".delete-transaction").off('click').on('click', deleteConfTrans);
		$("#btn_delete_transaction").off('click').on('click', deleteTrans);

		$("#btn_hide_add_ref").off('click').on('click', hideAddRefForm);
		$("#AddRefBtn").off('click').on('click', showAddRefForm);
		$("#SaveRefBtn").off('click').on('click', submitSelectRefCallback);
		$("#btn_save_add_ref").off('click').on('click', submitAddRefCallback);
		$(".input-ref").off('click').on('click', render_reference);

		if(book == true){
			if(transaction_add[0]['date'] == ''){
				$("#date").focus();
			}else if(transaction_add[0]['description'] == ''){
				$("#transactionDescription").focus();
			}else{
				$("#account_trans").focus();
			}
		}

		$(".input-spend").off('keydown').on('keydown', function(e){
			var keycode = (e.keyCode ? e.keyCode : e.which);

			if(keycode == '13') {
			}
			if(keycode == '9') {
				//addFormLedgerAccount();
			}
		});

		if(check_book.length == 0){
			if(!$("#add_transaction_row").hasClass('hidden'))
				$("#add_transaction_row").addClass('hidden');
		}else{
			if($("#add_transaction_row").hasClass('hidden'))
				$("#add_transaction_row").removeClass('hidden');
		}

		// input account autocomplete 
		$("#account_trans").on('keyup', accountAutoComplete);
		$(".input-account").on('keyup', accountAutoComplete);
	}

	function saveNewLedger(){
		if(!$("#msg_unbalance").hasClass('hidden'))
			$("#msg_unbalance").addClass('hidden');
		if(!$("#msg_less_than_one").hasClass('hidden'))
			$("#msg_less_than_one").addClass('hidden');

		for(x in ledger_accounts_add){
			if($("#form_balance_" + x).hasClass('has-error'))
				$("#form_balance_" + x).removeClass('has-error');
		}

		if(ledger_accounts_add.length < 2){
			if($("#msg_less_than_one").hasClass('hidden'))
				$("#msg_less_than_one").removeClass('hidden');
			$("#account_trans").focus();
			return false;
		}

		//var total_balance = parseInt($("#balance_trans_" + (ledger_accounts_add.length - 1)).val(), 10);
		var total_balance = 0;
		for(x in ledger_accounts_add){
			total_balance = total_balance + (ledger_accounts_add[x]['receive'] - ledger_accounts_add[x]['spend']);
		}

		if(total_balance != 0){
			if($("#msg_unbalance").hasClass('hidden'))
				$("#msg_unbalance").removeClass('hidden');
			if(!$("#balance_trans_" + (ledger_accounts_add.length - 1)).hasClass('has-error'))
				$("#balance_trans_" + (ledger_accounts_add.length - 1)).addClass('has-error');
			return false;
		}

		// render to make sure json string for the last
		render();

		var $form2 = $('#checkBeforeSaveForm'),
			url2 = $form2.attr( "action" );

		var formData2 = {
			_token: token,
			date : transaction_add[0]['date']
		};

		var request = $.ajax({
			url: url2,
			method: "GET",
			data: formData2,
			dataType: "json",
			success: function (data) {
				console.log(data[0]);
				$("#msgInformationRollBack").html('');
				if(data[0] == 'date_out_of_range'){
					if(!$("#trans_date_msg").hasClass('has-error'))
						$("#trans_date_msg").addClass("has-error");
					if($("#trans_date_msg > span").hasClass('hidden'))
						$("#trans_date_msg > span").removeClass("hidden");
					$("#trans_date_msg > span").html('Tanggal tidak ditemukan di semua buku.');
					return false;
				}else if(data[0] == 'book_not_active'){
					if(!$("#trans_date_msg").hasClass('has-error'))
						$("#trans_date_msg").addClass("has-error");
					if($("#trans_date_msg > span").hasClass('hidden'))
						$("#trans_date_msg > span").removeClass("hidden");
					$("#trans_date_msg > span").html('Tanggal yang dimasukan termasuk buku yang sudah ditutup (' + data[1] + ').');
					$("#msgInformationRollBack").html('Warning!!! Tanggal yang dimasukan termasuk buku yang sudah ditutup (' + data[1] + '). <strong style="color: red;">Buku pada tanggal yang dimasukan dan buku selanjutnya akan berubah!!!</strong>');
				}else if(data[0] == 'book_not_found'){
					return false;
				}

				if(data[0] == 'book_not_active'){
					var formData = {
						_token: token,
						transaction : $("#transaction-add").val(),
						ledger_accounts : $("#ledger-accounts-add").val(),
						book_roll_back_id : data[2]
					};
				}else if(data[0] == 'book_is_oke'){
					var formData = {
						_token: token,
						transaction : $("#transaction-add").val(),
						ledger_accounts : $("#ledger-accounts-add").val()
					};
				}

				$("#confirmRollBackModal").modal('show');

				$("#btn_ok_rollback").off('click').on('click', function(){
					// ajax post
					var $form = $('#ledgerForm'),
						url = $form.attr( "action" );

					var request = $.ajax({
						url: url,
						method: "POST",
						data: formData,
						dataType: "json",
						success: function (data) {
							if(data == 'date_out_of_range'){
								if(!$("#trans_date_msg").hasClass('has-error'))
									$("#trans_date_msg").addClass("has-error");
								if($("#trans_date_msg > span").hasClass('hidden'))
									$("#trans_date_msg > span").removeClass("hidden");
								$("#trans_date_msg > span").html('Date is out of range.');
							}else{
								ledger_accounts_add = [];
								transaction_add[0]['date'] = '';
								transaction_add[0]['description'] = '';
								render();
							}
						},
						error: function (data) {
							console.log(formData);
						} 
					});

					render();
				});
			},
			error: function (data) {
				return false;
			} 
		});	

		render();
	}

	function saveNewTrans(){
		var date = $("#date").val();
		var description = $("#transactionDescription").val();

		// validation
		if(!checkTransaction()){
			return false;
		}

		transaction_add[0]['date'] = date;
		transaction_add[0]['description'] = description;
	}

	function getLedgerData(){
		var $form = $('#getAllDataForm'),
			url = $form.attr( "action" );

		var account = $("#filter_account").val();
		var account_id = parseInt($("#filter_account_id").val(), 10);

		var data = {
			account_id: account_id
		};
		console.log(account_id);

		if(isNaN(account_id)){
			var request = $.ajax({
				url: url,
				method: "GET",
				async: false,
				dataType: "json",
				error: function (data) {
					console.log('error');
				} 
			});
		}else{
			var request = $.ajax({
				url: url,
				method: "GET",
				async: false,
				data: data,
				dataType: "json",
				error: function (data) {
					console.log('error');
				} 
			});
		}

		ledger_lists = JSON.parse(request.responseText);
	}

	function getCheckBook(){
		var $form = $('#getCheckBookForm'),
			url = $form.attr( "action" );

		var request = $.ajax({
			url: url,
			method: "GET",
			async: false,
			dataType: "json",
			error: function (data) {
				console.log('error');
			} 
		});

		check_book = JSON.parse(request.responseText);
	}

	function checkTransaction(){
		var date = $("#date").val();
		var description = $("#transactionDescription").val();

		// Reset classes
		if($("#trans_date_msg").hasClass('has-error'))
			$("#trans_date_msg").removeClass("has-error");
		if(!$("#trans_date_msg > span").hasClass('hidden'))
			$("#trans_date_msg > span").addClass("hidden");
		if($("#trans_desc_msg").hasClass('has-error'))
			$("#trans_desc_msg").removeClass("has-error");
		if(!$("#trans_desc_msg > span").hasClass('hidden'))
			$("#trans_desc_msg > span").addClass("hidden");

		// check transaction date
		if(date == ''){
			if(!$("#trans_date_msg").hasClass('has-error'))
				$("#trans_date_msg").addClass("has-error");
			if($("#trans_date_msg > span").hasClass('hidden'))
				$("#trans_date_msg > span").removeClass("hidden");
			$("#trans_date_msg > span").html('You must fill transaction date.');
			$("#date").focus();
			return false;
		}

		// check transaction description
		if(description == ''){
			if(!$("#trans_desc_msg").hasClass('has-error'))
				$("#trans_desc_msg").addClass("has-error");
			if($("#trans_desc_msg > span").hasClass('hidden'))
				$("#trans_desc_msg > span").removeClass("hidden");
			$("#transactionDescription").focus();
			return false;
		}

		return true;
	}

	function hitungBalance(i = null){
		if (i == null)
			var i = parseInt($(this).data('id'), 10);
			
		if (isNaN(i)){
			var receive = $('#receive_trans').val();
			var spend = $('#spend_trans').val();
			/*var balance = Number(receive) - Number(spend);*/
			var balance = 0;
			for(var s in ledger_accounts_add){
				balance = balance + (Number(ledger_accounts_add[s].receive) - Number(ledger_accounts_add[s].spend));
				ledger_accounts_add[s].balance = balance;
				$("#balance_trans_" + s).val(balance);
			}

			balance = balance + (Number(receive) - Number(spend));
			
			$('#balance_trans').val(balance);
			$('#total_balance').val(balance);
		}else{
			var balance = 0;
			for(var s in ledger_accounts_add){
				if(i != s){
					balance = balance + (Number(ledger_accounts_add[s].receive) - Number(ledger_accounts_add[s].spend));
				}else{
					var receive = $('#receive_trans_' + i).val();
					var spend = $('#spend_trans_' + i).val();

					balance = balance + (Number(receive) - Number(spend));
				}
				ledger_accounts_add[s].balance = balance;
				$("#balance_trans_" + s).val(balance);
			}

			$('#balance_trans').val(balance);
			$('#total_balance').val(balance);
		}
	}

	function checkAddNewLedger(event){
		var x = parseInt($(this).data('id'), 10);

		var acc_id_arr = [];
		var acc_code_name_arr = [];
		for(var i in accounts){
			acc_id_arr[i] = accounts[i].id;
			acc_code_name_arr[i] = accounts[i].code_name;
		}

		var id_list = [];
		for(var i in ledger_accounts_add){
			id_list[i] = parseInt(ledger_accounts_add[i].account_id, 10);
			if($("#form_account_" + i).hasClass('has-error'))
				$("#form_account_" + i).removeClass('has-error');
			if(!$("#form_account_" + i + " > p").hasClass('hidden'))
				$("#form_account_" + i + " > p").addClass('hidden');
			if(!$("#form_receive_" + i + " > p").hasClass('hidden'))
				$("#form_receive_" + i + " > p").addClass('hidden');
			if(!$("#form_spend_" + i + " > p").hasClass('hidden'))
				$("#form_spend_" + i + " > p").addClass('hidden');
		}

		// Reset classes
		if($("#trans_date_msg").hasClass('has-error'))
			$("#trans_date_msg").removeClass("has-error");
		if(!$("#trans_date_msg > span").hasClass('hidden'))
			$("#trans_date_msg > span").addClass("hidden");
		if($("#trans_desc_msg").hasClass('has-error'))
			$("#trans_desc_msg").removeClass("has-error");
		if(!$("#trans_desc_msg > span").hasClass('hidden'))
			$("#trans_desc_msg > span").addClass("hidden");

		if(!$("#msg_account_valid").hasClass('hidden'))
			$("#msg_account_valid").addClass('hidden');
		if(!$("#msg_add_item").hasClass('hidden'))
			$("#msg_add_item").addClass('hidden');
		if(!$("#msg_number_valid").hasClass('hidden'))
			$("#msg_number_valid").addClass('hidden');
		if(!$("#msg_receive_spend").hasClass('hidden'))
			$("#msg_receive_spend").addClass('hidden');
		if(!$("#msg_unbalance").hasClass('hidden'))
			$("#msg_unbalance").addClass('hidden');
		if($("#form_add_account").hasClass('has-error'))
			$("#form_add_account").removeClass('has-error');
		if($("#form_add_receive").hasClass('has-error'))
			$("#form_add_receive").removeClass('has-error');
		if($("#form_add_spend").hasClass('has-error'))
			$("#form_add_spend").removeClass('has-error');
		if($("#form_total_balance").hasClass('has-error'))
			$("#form_total_balance").removeClass('has-error');

		if(isNaN(x)){
			var account_name = $("#account_trans").val();
			var account_id = $("#account_id_trans").val();
			var receive = parseInt($("#receive_trans").val());
			var spend = parseInt($("#spend_trans").val());

			// parse account_id to integer type
			account_id = parseInt(account_id, 10);

			// check transaction date
			if(transaction_add[0]['date'] == ''){
				if(!$("#trans_date_msg").hasClass('has-error'))
					$("#trans_date_msg").addClass("has-error");
				if($("#trans_date_msg > span").hasClass('hidden'))
					$("#trans_date_msg > span").removeClass("hidden");
				$("#date").focus();
				return false;
			}

			// check transaction description
			if(transaction_add[0]['description'] == ''){
				if(!$("#trans_desc_msg").hasClass('has-error'))
					$("#trans_desc_msg").addClass("has-error");
				if($("#trans_desc_msg > span").hasClass('hidden'))
					$("#trans_desc_msg > span").removeClass("hidden");
				$("#transactionDescription").focus();
				return false;
			}

			// check if account id is not ''
			if(account_id == ''){
				if($("#msg_account_valid").hasClass('hidden'))
					$("#msg_account_valid").removeClass('hidden');
				if(!$("#form_add_account").hasClass('has-error'))
					$("#form_add_account").addClass('has-error');
				$("#account_trans").focus();
				return false;
			}

			// check if account_name id is not ''
			if(account_name == ''){
				if($("#msg_account_valid").hasClass('hidden'))
					$("#msg_account_valid").removeClass('hidden');
				if(!$("#form_add_account").hasClass('has-error'))
					$("#form_add_account").addClass('has-error');
				$("#account_trans").focus();
				return false;
			}

			// check valid account
			if(jQuery.inArray(account_id, acc_id_arr) != -1){
				if(jQuery.inArray(account_name, acc_code_name_arr) == -1){
					if($("#msg_account_valid").hasClass('hidden'))
						$("#msg_account_valid").removeClass('hidden');
					if(!$("#form_add_account").hasClass('has-error'))
						$("#form_add_account").addClass('has-error');
					$("#account_trans").focus();
					return false;
				}
			}else{
				if($("#msg_account_valid").hasClass('hidden'))
					$("#msg_account_valid").removeClass('hidden');
				if(!$("#form_add_account").hasClass('has-error'))
					$("#form_add_account").addClass('has-error');
				$("#account_trans").focus();
				return false;
			}

			// check if account already exist in list
			// if(jQuery.inArray(account_id, id_list) != -1){
			// 	var i = jQuery.inArray(account_id, id_list);

			// 	if($("#msg_add_item").hasClass('hidden'))
			// 		$("#msg_add_item").removeClass('hidden');
			// 	if(!$("#form_account_" + i).hasClass('has-error'))
			// 		$("#form_account_" + i).addClass('has-error');
			// 	if(!$("#form_add_account").hasClass('has-error'))
			// 		$("#form_add_account").addClass('has-error');
			// 	$("#account_trans").focus();
			// 	return false;
			// }

			// check if receive is valid number
			if(isNaN(receive)){
				if($("#msg_number_valid").hasClass('hidden'))
					$("#msg_number_valid").removeClass('hidden');
				if(!$("#form_add_receive").hasClass('has-error'))
					$("#form_add_receive").addClass('has-error');
				$("#receive_trans").focus();
				return false;
			}

			// check if spend is valid number
			if(isNaN(spend)){
				if($("#msg_number_valid").hasClass('hidden'))
					$("#msg_number_valid").removeClass('hidden');
				if(!$("#form_add_spend").hasClass('has-error'))
					$("#form_add_spend").addClass('has-error');
				$("#spend_trans").focus();
				return false;
			}

			// check receive or spend
			if((receive > 0 && spend > 0) || (receive == 0 && spend == 0)){
				if($("#msg_receive_spend").hasClass('hidden'))
					$("#msg_receive_spend").removeClass('hidden');
				if(!$("#form_add_receive").hasClass('has-error'))
					$("#form_add_receive").addClass('has-error');
				if(!$("#form_add_spend").hasClass('has-error'))
					$("#form_add_spend").addClass('has-error');
				$("#receive_trans").focus();
				return false;
			}

			var total = Number($("#total_balance").val());
			if(total != 0 && ledger_accounts_add.length > 0){
				if($("#msg_unbalance").hasClass('hidden'))
					$("#msg_unbalance").removeClass('hidden');
				if(!$("#form_add_receive").hasClass('has-error'))
					$("#form_add_receive").addClass('has-error');
				if(!$("#form_add_spend").hasClass('has-error'))
					$("#form_add_spend").addClass('has-error');
				if(!$("#form_total_balance").hasClass('has-error'))
					$("#form_total_balance").addClass('has-error');
				$("#receive_trans").focus();
			}
		}else{
			hitungBalance(x);
			var keycode = (event.keyCode ? event.keyCode : event.which);

			// check if data change
			var id = parseInt($("#account_id_trans_" + x).val(), 10);
			var name = $("#account_trans_" + x).val();
			if(ledger_accounts_add[x].account_id != id || ledger_accounts_add[x].account_name != name){
				if($("#form_account_" + x + " > p").hasClass('hidden'))
					$("#form_account_" + x + " > p").removeClass('hidden');
			}

			var receive = $("#receive_trans_" + x).val();
			if(ledger_accounts_add[x].receive != receive){
				if($("#form_receive_" + x + " > p").hasClass('hidden'))
					$("#form_receive_" + x + " > p").removeClass('hidden');
			}

			var spend = $("#spend_trans_" + x).val();
			if(ledger_accounts_add[x].spend != spend){
				if($("#form_spend_" + x + " > p").hasClass('hidden'))
					$("#form_spend_" + x + " > p").removeClass('hidden');
			}

			var note = $("#note_trans_" + x).val();
			if(ledger_accounts_add[x].note != note){
				if($("#form_note_" + x + " > p").hasClass('hidden'))
					$("#form_note_" + x + " > p").removeClass('hidden');
			}

			if(keycode == '16'){
				// check if account id is not ''
				if(id == ''){
					if($("#msg_account_valid").hasClass('hidden'))
						$("#msg_account_valid").removeClass('hidden');
					if(!$("#form_account_" + x).hasClass('has-error'))
						$("#form_account_" + x).addClass('has-error');
					$("#account_trans_" + x).focus();
					return false;
				}

				// check if account_name id is not ''
				if(name == ''){
					if($("#msg_account_valid").hasClass('hidden'))
						$("#msg_account_valid").removeClass('hidden');
					if(!$("#form_account_" + x).hasClass('has-error'))
						$("#form_account_" + x).addClass('has-error');
					$("#account_trans_" + x).focus();
					return false;
				}

				// check valid account
				if(jQuery.inArray(id, acc_id_arr) != -1){
					if(jQuery.inArray(name, acc_code_name_arr) == -1){
						if($("#msg_account_valid").hasClass('hidden'))
							$("#msg_account_valid").removeClass('hidden');
						if(!$("#form_account_" + x).hasClass('has-error'))
							$("#form_account_" + x).addClass('has-error');
						$("#account_trans_" + x).focus();
						return false;
					}
				}else{
					if($("#msg_account_valid").hasClass('hidden'))
						$("#msg_account_valid").removeClass('hidden');
					if(!$("#form_account_" + x).hasClass('has-error'))
						$("#form_account_" + x).addClass('has-error');
					$("#account_trans_" + x).focus();
					return false;
				}

				// check if account already exist in list
				var lists = id_list;
				lists.splice(x, 1);

				// if(jQuery.inArray(id, lists) != -1){
				// 	var i = jQuery.inArray(id, lists);

				// 	if($("#msg_add_item").hasClass('hidden'))
				// 		$("#msg_add_item").removeClass('hidden');
				// 	if(!$("#form_account_" + i).hasClass('has-error'))
				// 		$("#form_account_" + i).addClass('has-error');
				// 	if(!$("#form_account_" + x).hasClass('has-error'))
				// 		$("#form_account_" + x).addClass('has-error');
				// 	$("#account_trans_" + x).focus();
				// 	return false;
				// }

				// check if receive is valid number
				if(isNaN(receive)){
					if($("#msg_number_valid").hasClass('hidden'))
						$("#msg_number_valid").removeClass('hidden');
					if(!$("#form_receive_" + x).hasClass('has-error'))
						$("#form_receive_" + x).addClass('has-error');
					$("#receive_trans_" + x).focus();
					return false;
				}

				// check if spend is valid number
				if(isNaN(spend)){
					if($("#msg_number_valid").hasClass('hidden'))
						$("#msg_number_valid").removeClass('hidden');
					if(!$("#form_spend_" + x).hasClass('has-error'))
						$("#form_spend_" + x).addClass('has-error');
					$("#spend_trans_" + x).focus();
					return false;
				}

				// check receive or spend
				if((receive > 0 && spend > 0) || (receive == 0 && spend == 0)){
					if($("#msg_receive_spend").hasClass('hidden'))
						$("#msg_receive_spend").removeClass('hidden');
					if(!$("#form_receive_" + x).hasClass('has-error'))
						$("#form_receive_" + x).addClass('has-error');
					if(!$("#form_spend_" + x).hasClass('has-error'))
						$("#form_spend_" + x).addClass('has-error');
					$("#receive_trans_" + x).focus();
					return false;
				}

				ledger_accounts_add[x].account_id = id;
				ledger_accounts_add[x].account_name = name;
				ledger_accounts_add[x].receive = receive;
				ledger_accounts_add[x].spend = spend;
				ledger_accounts_add[x].note = note;

				render();
				hitungBalance();
			}
		}
		

		return true;
	}

	function addFormLedgerAccount(){
		var position = $(this).data('position');

		if(position == 'new'){
			var note = $("#note_trans").val();
			var account_trans = $("#account_trans").val();
			var account_id_trans = $("#account_id_trans").val();
			var receive_trans = parseInt($("#receive_trans").val());
			var spend_trans = parseInt($("#spend_trans").val());

			// validation
			if(!checkAddNewLedger()){
				return false;
			}

			var input = {
				'account_name' : account_trans,
				'account_id' : account_id_trans,
				'receive' : receive_trans,
				'spend' : spend_trans,
				'note': note
			};

			if (account_trans != '' && account_id_trans != ''){
				ledger_accounts_add.push(input);
				$("#account_trans").val('');
				$("#account_id_trans").val('');
				$("#receive_trans").val(0);
				$("#spend_trans").val(0);
			}

			render();
			hitungBalance();
		}else if(position == 'edit'){
			var idx = parseInt($(this).data('idx'), 10);
			var num = parseInt($(this).data('num'), 10);

			if(!isNaN(idx)){
				var note = $("#edit_note_trans_" + idx).val();
				var account_trans = $("#edit_account_trans_" + idx).val();
				var account_id_trans = parseInt($("#edit_account_id_trans_" + idx).val(), 10);
				var receive_trans = parseInt($("#edit_receive_trans_" + idx).val());
				var spend_trans = parseInt($("#edit_spend_trans_" + idx).val());
			}else{
				return false;
			}

			var input = {
				'account_name' : account_trans,
				'account_id' : account_id_trans,
				'receive' : receive_trans,
				'spend' : spend_trans,
				'description': note
			};

			// validation

			var id_list = [];
			for(var i in ledger_accounts_edit){
				id_list[i] = parseInt(ledger_accounts_edit[i].account_id, 10);

				if($("#edit_form_account_" + idx + "_" + i).hasClass('has-error'))
					$("#edit_form_account_" + idx + "_" + i).removeClass('has-error');
			}

			var acc_id_arr = [];
			var acc_code_name_arr = [];
			for(var i in accounts){
				acc_id_arr[i] = accounts[i].id;
				acc_code_name_arr[i] = accounts[i].code_name;
			}

			if(!$("#msg_account_valid_" + idx).hasClass('hidden'))
				$("#msg_account_valid_" + idx).addClass('hidden');
			if(!$("#msg_add_item_" + idx).hasClass('hidden'))
				$("#msg_add_item_" + idx).addClass('hidden');
			if(!$("#msg_number_valid_" + idx).hasClass('hidden'))
				$("#msg_number_valid_" + idx).addClass('hidden');
			if(!$("#msg_receive_spend_" + idx).hasClass('hidden'))
				$("#msg_receive_spend_" + idx).addClass('hidden');
			if(!$("#msg_unbalance_" + idx).hasClass('hidden'))
				$("#msg_unbalance_" + idx).addClass('hidden');
			if($("#form_edit_account_" + idx).hasClass('has-error'))
				$("#form_edit_account_" + idx).removeClass('has-error');
			if($("#form_edit_receive_" + idx).hasClass('has-error'))
				$("#form_edit_receive_" + idx).removeClass('has-error');
			if($("#form_edit_spend_" + idx).hasClass('has-error'))
				$("#form_edit_spend_" + idx).removeClass('has-error');
			if($("#form_total_balance_" + idx).hasClass('has-error'))
				$("#form_total_balance_" + idx).removeClass('has-error');

			if(account_id_trans == ''){
				if($("#msg_account_valid_" + idx).hasClass('hidden'))
					$("#msg_account_valid_" + idx).removeClass('hidden');
				if(!$("#form_edit_account_" + idx).hasClass('has-error'))
					$("#form_edit_account_" + idx).addClass('has-error');
				$("#edit_account_trans_" + idx).focus();
				return false;
			}

			// check if account_name id is not ''
			if(account_trans == ''){
				if($("#msg_account_valid_" + idx).hasClass('hidden'))
					$("#msg_account_valid_" + idx).removeClass('hidden');
				if(!$("#form_edit_account_" + idx).hasClass('has-error'))
					$("#form_edit_account_" + idx).addClass('has-error');
				$("#edit_account_trans_" + idx).focus();
				return false;
			}

			// check valid account
			if(jQuery.inArray(account_id_trans, acc_id_arr) != -1){
				if(jQuery.inArray(account_trans, acc_code_name_arr) == -1){
					if($("#msg_account_valid_" + idx).hasClass('hidden'))
						$("#msg_account_valid_" + idx).removeClass('hidden');
					if(!$("#form_edit_account_" + idx).hasClass('has-error'))
						$("#form_edit_account_" + idx).addClass('has-error');
					$("#edit_account_trans_" + idx).focus();
					return false;
				}
			}else{
				if($("#msg_account_valid_" + idx).hasClass('hidden'))
					$("#msg_account_valid_" + idx).removeClass('hidden');
				if(!$("#form_edit_account_" + idx).hasClass('has-error'))
					$("#form_edit_account_" + idx).addClass('has-error');
				$("#edit_account_trans_" + idx).focus();
				return false;
			}

			// check if account already exist in list
			// if(jQuery.inArray(account_id_trans, id_list) != -1){
			// 	var i = jQuery.inArray(account_id_trans, id_list);

			// 	if($("#msg_add_item_" + idx).hasClass('hidden'))
			// 		$("#msg_add_item_" + idx).removeClass('hidden');
			// 	if(!$("#edit_form_account_" + idx + "_" + i).hasClass('has-error'))
			// 		$("#edit_form_account_" + idx + "_" + i).addClass('has-error');
			// 	if(!$("#form_edit_account_" + idx).hasClass('has-error'))
			// 		$("#form_edit_account_" + idx).addClass('has-error');
			// 	$("#edit_account_trans_" + idx).focus();
			// 	return false;
			// }

			// check if receive is valid number
			if(isNaN(receive_trans)){
				if($("#msg_number_valid_" + idx).hasClass('hidden'))
					$("#msg_number_valid_" + idx).removeClass('hidden');
				if(!$("#form_edit_receive_" + idx).hasClass('has-error'))
					$("#form_edit_receive_" + idx).addClass('has-error');
				$("#edit_receive_trans_" + idx).focus();
				return false;
			}

			// check if spend is valid number
			if(isNaN(spend_trans)){
				if($("#msg_number_valid_" + idx).hasClass('hidden'))
					$("#msg_number_valid_" + idx).removeClass('hidden');
				if(!$("#form_edit_spend_" + idx).hasClass('has-error'))
					$("#form_edit_spend_" + idx).addClass('has-error');
				$("#edit_spend_trans_" + idx).focus();
				return false;
			}

			// check receive or spend
			if((receive_trans > 0 && spend_trans > 0) || (receive_trans == 0 && spend_trans == 0)){
				if($("#msg_receive_spend_" + idx).hasClass('hidden'))
					$("#msg_receive_spend_" + idx).removeClass('hidden');
				if(!$("#form_edit_receive_" + idx).hasClass('has-error'))
					$("#form_edit_receive_" + idx).addClass('has-error');
				if(!$("#form_edit_spend_" + idx).hasClass('has-error'))
					$("#form_edit_spend_" + idx).addClass('has-error');
				$("#edit_receive_trans_" + idx).focus();
				return false;
			}

			var total = Number($("#total_balance").val());
			if(total != 0 && ledger_accounts_edit.length > 0){
				if($("#msg_unbalance_" + idx).hasClass('hidden'))
					$("#msg_unbalance_" + idx).removeClass('hidden');
				if(!$("#form_edit_receive_" + idx).hasClass('has-error'))
					$("#form_edit_receive_" + idx).addClass('has-error');
				if(!$("#form_edit_spend_" + idx).hasClass('has-error'))
					$("#form_edit_spend_" + idx).addClass('has-error');
				if(!$("#form_total_balance_" + idx).hasClass('has-error'))
					$("#form_total_balance_" + idx).addClass('has-error');
				$("#edit_receive_trans_" + idx).focus();
			}

			if (account_trans != '' && account_id_trans != ''){
				ledger_accounts_edit.push(input);
				$("#edit_note_trans_" + idx).val('');
				$("#edit_account_id_trans_" + idx).val('');
				$("#edit_account_trans_" + idx).val('');
				$("#edit_receive_trans_" + idx).val(0);
				$("#edit_spend_trans_" + idx).val(0);
			}

			renderEdit(num);
		}
	}

	function getIndex() 
	{
		for (idx in ledger_accounts_add) {
			ledger_accounts_add[idx]['_id'] = idx;
			ledger_accounts_add[idx]['no'] = parseInt(idx) + 1;
		}

		for (idx in ledger_accounts_edit) {
			ledger_accounts_edit[idx]['_id'] = idx;
			ledger_accounts_edit[idx]['_idx'] = transaction_edit['_id'];
			ledger_accounts_edit[idx]['no'] = parseInt(idx) + 1;
		}
	}

	function deleteAccountLedger(){
		var i = parseInt($(this).data('id'), 10);
		var position = $(this).data('position');

		if(position == 'new'){
			ledger_accounts_add.splice(i, 1);
			render();
		}else if(position == 'edit'){
			var idx = parseInt($(this).data('idx'), 10);
			ledger_accounts_edit.splice(i, 1);

			renderEdit(idx);
		}
	}

	function accountAutoComplete(event){
		var i = parseInt($(this).data('id'), 10);
		var keycode = (event.keyCode ? event.keyCode : event.which);

		var $form = $('#autocompleteForm'),
			url = $form.attr( "action" );

		if(isNaN(i)){
			if(keycode == '8' || keycode == '46'){
				$('#account_trans').val('');
				$('#account_id_trans').val('');
			}

			$('#account_trans').autocomplete({
				source: url,
				minLength: 0,
				select: function(event, ui){
					$('#account_id_trans').val(ui.item.id);
				}
			});
		}else{
			if(keycode == '8' || keycode == '46'){
				$('#account_trans_' + i).val('');
				$('#account_id_trans_' + i).val('');
			}

			$(".input-account").on('keyup', function(){
				$('#account_trans_' + i).autocomplete({
					source: url,
					minLength: 0,
					select: function(event, ui){
						$('#account_id_trans_' + i).val(ui.item.id);
					}
				});
			});
		}
	}

	function filterAccountAuto(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);

		var $form = $('#autocompleteForm'),
			url = $form.attr( "action" );

		if(keycode == '8' || keycode == '46'){
			$('#filter_account').val('');
			$('#filter_account_id').val('');
		}

		$('#filter_account').autocomplete({
			source: url,
			minLength: 0,
			select: function(event, ui){
				$('#filter_account_id').val(ui.item.id);
			}
		});
	}

	function getAccount(){
		var $form = $('#getAllDataJsonForm'),
			url = $form.attr( "action" );

		var request = $.ajax({
			url: url,
			method: "GET",
			dataType: "json",
			async: false,
			error: function (data) {
				console.log('error get accounts data.');
			} 
		});

		accounts = JSON.parse(request.responseText);
	}

	function hideAddRefForm(){
		if($('#AddRefBtn').hasClass('hidden'))
			$('#AddRefBtn').removeClass('hidden');
		if($('#selectReference').hasClass('hidden'))
			$('#selectReference').removeClass('hidden');
		if(!$('#addReference').hasClass('hidden'))
			$('#addReference').addClass('hidden');
	}

	function showAddRefForm(){
		if(!$('#AddRefBtn').hasClass('hidden'))
			$('#AddRefBtn').addClass('hidden');
		if(!$('#selectReference').hasClass('hidden'))
			$('#selectReference').addClass('hidden');
		if($('#addReference').hasClass('hidden'))
			$('#addReference').removeClass('hidden');
	}

	function submitSelectRefCallback (){
		var c_value = [];
		$('input[name="references[]"]:checked').each(function () {
			c_value.push(this.value);
		});

		var id = $( "#ledger_id" ).val();

		var $form = $('#saveSelectRefForm'),
			url = $form.attr( "action" );

		var formData = {
			_token: token,
			ledger_id: id,
			references : c_value
		}

		if(!$("#msg_rev_success").hasClass('hidden'))
			$("#msg_rev_success").addClass('hidden');
		if(!$("#msg_rev_failed").hasClass('hidden'))
			$("#msg_rev_failed").addClass('hidden');

		var request = $.ajax({
			url: url,
			method: "POST",
			data: formData,
			dataType: "json",
			success: function (data) {
				$( "#referenceName" ).val('');
				$( "#referenceDescription" ).val('');
				render_reference();
				hideAddRefForm();
				
				if(data == true){
					//$('#referenceModal').modal('hide');
					if($("#msg_rev_success").hasClass('hidden'))
						$("#msg_rev_success").removeClass('hidden');
				}else{
					if($("#msg_rev_failed").hasClass('hidden'))
						$("#msg_rev_failed").removeClass('hidden');
				}
				$( "#ledger_id" ).val('');
			},
			error: function (data) {
				console.log('failed');
			} 
		});
	}

	function submitAddRefCallback (){
		var $form = $('#addRefForm'),
			url = $form.attr( "action" );

		var formData = {
			_token: token,
			name: $( "#referenceName" ).val(),
			description : $( "#referenceDescription" ).val()
		}

		if(!$("#msg_rev_success").hasClass('hidden'))
			$("#msg_rev_success").addClass('hidden');
		if(!$("#msg_rev_failed").hasClass('hidden'))
			$("#msg_rev_failed").addClass('hidden');

		var request = $.ajax({
			url: url,
			method: "POST",
			data: formData,
			dataType: "json",
			success: function (data) {
				var id = $( "#ledger_id" ).val();
				$( "#referenceName" ).val('');
				$( "#referenceDescription" ).val('');

				render_reference();
				hideAddRefForm();
				if(data == true){
					//$('#referenceModal').modal('hide');
					if($("#msg_rev_success").hasClass('hidden'))
						$("#msg_rev_success").removeClass('hidden');
				}else{
					if($("#msg_rev_failed").hasClass('hidden'))
						$("#msg_rev_failed").removeClass('hidden');
				}
				$( "#ledger_id" ).val('');
			},
			error: function (data) {
				console.log('failed');
			} 
		});

	}

	function render_reference(){
		var id = parseInt($(this).data('ledger'), 10);

		if(isNaN(id))
			id = $('#ledger_id').val();

		var $form = $('#getRefForm'),
			url = $form.attr( "action" );

		var request = $.ajax({
			url: url,
			method: "GET",
			data: {id: id},
			dataType: "json",
			success: function (msg) {
				var tmpl = $('#referenceSelectList').html();

				Mustache.parse(tmpl);
				var data = { 
					references: msg
				};

				var html = Mustache.render(tmpl, data);

				$('#referenceCheckbox').html(html);
				$('#ledger_id').val(id);
			},
			error: function (data) {
				console.log('error');
			} 
		});
	}


	// ------------------------------------------------------ //
	//                       Edit Section					  //
	// ------------------------------------------------------ //

	var ledger_accounts_edit = [];
	var transaction_edit = [];

	function renderEdit(id) 
	{
		getIndex();
		hitungBalanceEdit(id);

		for(var y in ledger_accounts_edit){
			ledger_accounts_edit[y].num = id;
		}

		transaction_edit['num'] = id;

		$(".edit-transaction-row").html('');
		$(".edit-row-data").html('');
		$(".add-row-data").html('');
		$(".edit-row-section").html('');

		$('#ledger-accounts-edit').val(JSON.stringify(ledger_accounts_edit));
		$('#transaction-edit').val(JSON.stringify(transaction_edit));

		var tmpl = $('#tmplateEditTransaction').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { transaction_edit: transaction_edit });
		$('#edit_transaction_row_' + id).html(html);

		for(var i = 0; i < 201; i++){
			$('#edit_row_data_' + id + '_' + i).html('');
		}
		
		$("#edit_row_" + id).html("");
		$("#addRowTotal_" + id).html("");

		var tmpl = $('#tmplateEditLedger').html();
		Mustache.parse(tmpl);
		var html = Mustache.render(tmpl, { ledger_edit: ledger_accounts_edit, transaction_edit: transaction_edit });
		$('#edit_transaction_row_' + id).after(html);

		bind();

		$("#edit_account_trans_0").focus();
	}

	function getDataEdit(id){
		var $form = $('#getLedgerDataForm'),
			url = $form.attr( "action" );

		var request = $.ajax({
			url: url,
			method: "GET",
			data: {transaction_id: id},
			dataType: "json",
			async: false,
			error: function (data) {
				console.log('error get accounts data.');
			} 
		});

		var data = JSON.parse(request.responseText);
		transaction_edit = data[0].transaction;
		ledger_accounts_edit = data[0].accounts;
	}

	function showEdit(){
		var i = parseInt($(this).data('id'), 10);
		var transaction_id = parseInt($(this).data('transaction'), 10);
		var count = parseInt($(this).data('count'), 10);

		render();

		$("#add_transaction_row").html('');
		$(".edit-row-data").html('');
		$(".add-row-data").html('');
		$("#addRow").html('');
		$("#addRowTotal").html('');

		$("#data_transaction_" + i).html('');
		for(var x = 0; x < count; x++){
			$("#dataRow_" + i + "_" + x).html('');
		}

		getDataEdit(transaction_id);
		renderEdit(i);
	}

	function cancelEdit(){
		var i = parseInt($(this).data('id'), 10);
		var count = parseInt($(this).data('count'), 10);

		if($("#editTrans" + i).hasClass('hidden'))
			$("#editTrans" + i).removeClass('hidden');
		if($("#deleteTrans" + i).hasClass('hidden'))
			$("#deleteTrans" + i).removeClass('hidden');
		if($("#transaction_date_text_" + i).hasClass('hidden'))
			$("#transaction_date_text_" + i).removeClass('hidden');
		if($("#transaction_description_text_" + i).hasClass('hidden'))
			$("#transaction_description_text_" + i).removeClass('hidden');

		if(!$("#saveTrans" + i).hasClass('hidden'))
			$("#saveTrans" + i).addClass('hidden');
		if(!$("#cancelTrans" + i).hasClass('hidden'))
			$("#cancelTrans" + i).addClass('hidden');
		if(!$("#transaction_date_edit_" + i).hasClass('hidden'))
			$("#transaction_date_edit_" + i).addClass('hidden');
		if(!$("#transaction_description_edit_" + i).hasClass('hidden'))
			$("#transaction_description_edit_" + i).addClass('hidden');

		if($("#add_transaction_row").hasClass('hidden'))
			$("#add_transaction_row").removeClass('hidden');
		if($("#addRowData").hasClass('hidden'))
			$("#addRowData").removeClass('hidden');
		if($("#addRow").hasClass('hidden'))
			$("#addRow").removeClass('hidden');
		if($("#addRowTotal").hasClass('hidden'))
			$("#addRowTotal").removeClass('hidden');

		for(var x = 0; x < count; x++){
			if(!$("#ledger_description_edit_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_description_edit_" + i + "_" + x).addClass('hidden');
			if(!$("#ledger_name_edit_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_name_edit_" + i + "_" + x).addClass('hidden');
			if(!$("#ledger_receive_edit_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_receive_edit_" + i + "_" + x).addClass('hidden');
			if(!$("#ledger_spend_edit_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_spend_edit_" + i + "_" + x).addClass('hidden');

			if($("#ledger_description_text_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_description_text_" + i + "_" + x).removeClass('hidden');
			if($("#ledger_name_text_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_name_text_" + i + "_" + x).removeClass('hidden');
			if($("#ledger_receive_text_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_receive_text_" + i + "_" + x).removeClass('hidden');
			if($("#ledger_spend_text_" + i + "_" + x).hasClass('hidden'))
				$("#ledger_spend_text_" + i + "_" + x).removeClass('hidden');
		}
	}

	function checkEditLedger(event){
		var x = parseInt($(this).data('id'), 10);
		var num = parseInt($(this).data('num'), 10);
		var idx = parseInt($(this).data('idx'), 10);

		var acc_id_arr = [];
		var acc_code_name_arr = [];
		for(var i in accounts){
			acc_id_arr[i] = accounts[i].id;
			acc_code_name_arr[i] = accounts[i].code_name;
		}

		var id_list = [];
		for(var i in ledger_accounts_edit){
			id_list[i] = parseInt(ledger_accounts_edit[i].account_id, 10);
			if($("#edit_form_account_" + idx + "_" + i).hasClass('has-error'))
				$("#edit_form_account_" + idx + "_" + i).removeClass('has-error');
			if(!$("#edit_form_note_" + idx + "_" + i + " > p").hasClass('hidden'))
				$("#edit_form_note_" + idx + "_" + i + " > p").addClass('hidden');
			if(!$("#edit_form_account_" + idx + "_" + i + " > p").hasClass('hidden'))
				$("#edit_form_account_" + idx + "_" + i + " > p").addClass('hidden');
			if(!$("#edit_form_receive_" + idx + "_" + i + " > p").hasClass('hidden'))
				$("#edit_form_receive_" + idx + "_" + i + " > p").addClass('hidden');
			if(!$("#edit_form_spend_" + idx + "_" + i + " > p").hasClass('hidden'))
				$("#edit_form_spend_" + idx + "_" + i + " > p").addClass('hidden');
		}

		// Reset classes
		if($("#trans_date_msg_edit_" + idx).hasClass('has-error'))
			$("#trans_date_msg_edit_" + idx).removeClass("has-error");
		if(!$("#trans_date_msg_edit_" + idx + " > span").hasClass('hidden'))
			$("#trans_date_msg_edit_" + idx + " > span").addClass("hidden");
		if($("#trans_desc_msg_edit_" + idx).hasClass('has-error'))
			$("#trans_desc_msg_edit_" + idx).removeClass("has-error");
		if(!$("#trans_desc_msg_edit_" + idx + " > span").hasClass('hidden'))
			$("#trans_desc_msg_edit_" + idx + " > span").addClass("hidden");

		if(!$("#msg_account_valid_" + idx).hasClass('hidden'))
			$("#msg_account_valid_" + idx).addClass('hidden');
		if(!$("#msg_add_item_" + idx).hasClass('hidden'))
			$("#msg_add_item_" + idx).addClass('hidden');
		if(!$("#msg_number_valid_" + idx).hasClass('hidden'))
			$("#msg_number_valid_" + idx).addClass('hidden');
		if(!$("#msg_receive_spend_" + idx).hasClass('hidden'))
			$("#msg_receive_spend_" + idx).addClass('hidden');
		if(!$("#msg_unbalance_" + idx).hasClass('hidden'))
			$("#msg_unbalance_" + idx).addClass('hidden');
		if($("#form_add_account_" + idx).hasClass('has-error'))
			$("#form_add_account_" + idx).removeClass('has-error');
		if($("#form_add_receive_" + idx).hasClass('has-error'))
			$("#form_add_receive_" + idx).removeClass('has-error');
		if($("#form_add_spend_" + idx).hasClass('has-error'))
			$("#form_add_spend_" + idx).removeClass('has-error');
		if($("#form_total_balance_" + idx).hasClass('has-error'))
			$("#form_total_balance_" + idx).removeClass('has-error');

		if(!isNaN(x)){
			hitungBalanceEdit(num);
			var keycode = (event.keyCode ? event.keyCode : event.which);

			// check if data change
			var id = parseInt($("#edit_account_id_trans_" + idx + "_" + x).val(), 10);
			var name = $("#edit_account_trans_" + idx + "_" + x).val();
			if(ledger_accounts_edit[x].account_id != id || ledger_accounts_edit[x].account_name != name){
				if($("#edit_form_account_" + idx + "_" + x + " > p").hasClass('hidden'))
					$("#edit_form_account_" + idx + "_" + x + " > p").removeClass('hidden');
			}

			var receive = $("#edit_receive_trans_" + idx + "_" + x).val();
			if(ledger_accounts_edit[x].receive != receive){
				if($("#edit_form_receive_" + idx + "_" + x + " > p").hasClass('hidden'))
					$("#edit_form_receive_" + idx + "_" + x + " > p").removeClass('hidden');
			}

			var spend = $("#edit_spend_trans_" + idx + "_" + x).val();
			if(ledger_accounts_edit[x].spend != spend){
				if($("#edit_form_spend_" + idx + "_" + x + " > p").hasClass('hidden'))
					$("#edit_form_spend_" + idx + "_" + x + " > p").removeClass('hidden');
			}

			var note = $("#edit_note_trans_" + idx + "_" + x).val();
			if(ledger_accounts_edit[x].description != note){
				if($("#edit_form_note_" + idx + "_" + x + " > p").hasClass('hidden'))
					$("#edit_form_note_" + idx + "_" + x + " > p").removeClass('hidden');
			}

			if(keycode == '16'){
				// check if account id is not ''
				if(id == ''){
					if($("#msg_account_valid_" + idx).hasClass('hidden'))
						$("#msg_account_valid_" + idx).removeClass('hidden');
					if(!$("#edit_form_account_" + idx + "_" + x).hasClass('has-error'))
						$("#edit_form_account_" + idx + "_" + x).addClass('has-error');
					$("#edit_account_trans_" + idx + "_" + x).focus();
					return false;
				}

				// check if account_name id is not ''
				if(name == ''){
					if($("#msg_account_valid_" + idx).hasClass('hidden'))
						$("#msg_account_valid_" + idx).removeClass('hidden');
					if(!$("#edit_form_account_" + idx + "_" + x).hasClass('has-error'))
						$("#edit_form_account_" + idx + "_" + x).addClass('has-error');
					$("#edit_account_trans_" + idx + "_" + x).focus();
					return false;
				}

				// check valid account
				if(jQuery.inArray(id, acc_id_arr) != -1){
					if(jQuery.inArray(name, acc_code_name_arr) == -1){
						if($("#msg_account_valid_" + idx).hasClass('hidden'))
							$("#msg_account_valid_" + idx).removeClass('hidden');
						if(!$("#edit_form_account_" + idx + "_" + x).hasClass('has-error'))
							$("#edit_form_account_" + idx + "_" + x).addClass('has-error');
						$("#edit_account_trans_" + idx + "_" + x).focus();
						return false;
					}
				}else{
					if($("#msg_account_valid_" + idx).hasClass('hidden'))
						$("#msg_account_valid_" + idx).removeClass('hidden');
					if(!$("#edit_form_account_" + idx + "_" + x).hasClass('has-error'))
						$("#edit_form_account_" + idx + "_" + x).addClass('has-error');
					$("#edit_account_trans_" + idx + "_" + x).focus();
					return false;
				}

				// check if account already exist in list
				var lists = id_list;
				lists.splice(x, 1);

				// if(jQuery.inArray(id, lists) != -1){
				// 	var i = jQuery.inArray(id, lists);

				// 	if($("#msg_account_valid_" + idx).hasClass('hidden'))
				// 		$("#msg_account_valid_" + idx).removeClass('hidden');
				// 	if(!$("#edit_form_account_" + idx + "_" + i).hasClass('has-error'))
				// 		$("#edit_form_account_" + idx + "_" + i).addClass('has-error');
				// 	$("#edit_account_trans_" + idx + "_" + i).focus();
				// 	return false;
				// }

				// check if receive is valid number
				if(isNaN(receive)){
					if($("#msg_number_valid_" + idx).hasClass('hidden'))
						$("#msg_number_valid_" + idx).removeClass('hidden');
					if(!$("#edit_form_receive_" + idx + "_" + x).hasClass('has-error'))
						$("#edit_form_receive_" + idx + "_" + x).addClass('has-error');
					$("#edit_receive_trans_" + idx + "_" + x).focus();
					return false;
				}

				// check if spend is valid number
				if(isNaN(spend)){
					if($("#msg_number_valid_" + idx).hasClass('hidden'))
						$("#msg_number_valid_" + idx).removeClass('hidden');
					if(!$("#edit_form_spend_" + idx + "_" + x).hasClass('has-error'))
						$("#edit_form_spend_" + idx + "_" + x).addClass('has-error');
					$("#edit_spend_trans_" + idx + "_" + x).focus();
					return false;
				}

				// check receive or spend
				if((receive > 0 && spend > 0) || (receive == 0 && spend == 0)){
					if($("#msg_receive_spend_" + idx).hasClass('hidden'))
						$("#msg_receive_spend_" + idx).removeClass('hidden');
					if(!$("#edit_form_receive_" + idx + "_" + x).hasClass('has-error'))
						$("#edit_form_receive_" + idx + "_" + x).addClass('has-error');
					if(!$("#edit_form_spend_" + idx + "_" + x).hasClass('has-error'))
						$("#edit_form_spend_" + idx + "_" + x).addClass('has-error');
					$("#edit_receive_trans_" + idx + "_" + x).focus();
					return false;
				}

				ledger_accounts_edit[x].account_id = id;
				ledger_accounts_edit[x].account_name = name;
				ledger_accounts_edit[x].receive = receive;
				ledger_accounts_edit[x].spend = spend;
				ledger_accounts_edit[x].description = note;

				renderEdit(num);
				hitungBalanceEdit(num);
			}
		}

		return true;
	}

	function hitungBalanceEdit(idx){
		var idx = parseInt(idx, 10);
		var id = parseInt($(this).data('id'), 10);

		if(isNaN(idx))
			idx = parseInt($(this).data('idx'), 10);
			
		if (isNaN(id)){
			var receive = $('#edit_receive_trans_' + idx).val();
			var spend = $('#edit_spend_trans_' + idx).val();
			/*var balance = Number(receive) - Number(spend);*/
			var balance = 0;
			for(var s in ledger_accounts_edit){
				balance = balance + (Number(ledger_accounts_edit[s].receive) - Number(ledger_accounts_edit[s].spend));
				ledger_accounts_edit[s].balance = balance;
				$("#edit_balance_trans_" + idx + '_' + s).val(balance);
			}

			balance = balance + (Number(receive) - Number(spend));
			
			$('#edit_balance_trans_' + idx).val(balance);
			$('#total_balance_' + idx).val(balance);
		}else{
			$('#edit_receive_trans_' + idx).val(0);
			$('#edit_spend_trans_' + idx).val(0);

			var balance = 0;
			for(var s in ledger_accounts_edit){
				if(id != s){
					balance = balance + (Number(ledger_accounts_edit[s].receive) - Number(ledger_accounts_edit[s].spend));
				}else{
					var receive = $('#edit_receive_trans_' + idx + '_' + s).val();
					var spend = $('#edit_spend_trans_' + idx + '_' + s).val();

					balance = balance + (Number(receive) - Number(spend));
				}
				ledger_accounts_edit[s].balance = balance;
				$("#edit_balance_trans_" + idx + '_' + s).val(balance);
			}

			$('#edit_balance_trans_' + idx).val(balance);
			$('#total_balance_' + idx).val(balance);
		}
	}

	function saveEdit(){
		var i = parseInt($(this).data('id'), 10);
		var count = parseInt($(this).data('count'), 10);

		console.log('Save Edit >> You clicked data: ' + i + ', where count: ' + count);
	}

	function deleteConfTrans(){
		var transaction_id = parseInt($(this).data('transaction'), 10);
		$("#trans_del_id").val('');
		$("#trans_del_id").val(transaction_id);
	}

	function deleteTrans(){
		var transaction_id = parseInt($("#trans_del_id").val(), 10);
		var $form2 = $('#checkBeforeSaveForm'),
			url2 = $form2.attr( "action" );

		var formData2 = {
			_token: token,
			transaction_id : transaction_id
		};

		var request = $.ajax({
			url: url2,
			method: "GET",
			data: formData2,
			dataType: "json",
			success: function (data) {
				console.log(data[0]);
				$("#msgInformationRollBack").html('');
				if(data[0] == 'date_out_of_range'){
					return false;
				}else if(data[0] == 'book_not_active'){
					$("#msgInformationRollBack").html('Warning!!! Tanggal yang dimasukan termasuk buku yang sudah ditutup (' + data[1] + '). <strong style="color: red;">Buku pada tanggal yang dimasukan dan buku selanjutnya akan berubah!!!</strong>');
				}else if(data[0] == 'book_not_found'){
					return false;
				}

				if(data[0] == 'book_not_active'){
					var formData = {
						_token: token,
						id: transaction_id,
						book_roll_back_id : data[2]
					};
				}else if(data[0] == 'book_is_oke'){
					var formData = {
						_token: token,
						id: transaction_id
					};
				}

				$("#confirmRollBackModal").modal('show');

				$("#btn_ok_rollback").off('click').on('click', function(){
					// ajax post
					var $form = $('#deleteLedgerForm'),
						url = $form.attr( "action" );

					var request = $.ajax({
						url: url,
						method: "GET",
						data: formData,
						dataType: "json",
						success: function (data) {
							render();
							$("#confirmationModal").modal('hide');
						},
						error: function (data) {
							console.log('error');
						} 
					});
				});
			},
			error: function (data) {
				return false;
			} 
		});
		render();
	}

	function accountEditAutoComplete(event){
		var i = parseInt($(this).data('id'), 10);
		var idx = parseInt($(this).data('idx'), 10);
		var keycode = (event.keyCode ? event.keyCode : event.which);

		var $form = $('#autocompleteForm'),
			url = $form.attr( "action" );

		if(isNaN(i)){
			if(keycode == '8' || keycode == '46'){
				$('#edit_account_trans_' + idx).val('');
				$('#edit_account_id_trans_' + idx).val('');
			}

			$('#edit_account_trans_' + idx).autocomplete({
				source: url,
				minLength: 0,
				select: function(event, ui){
					$('#edit_account_id_trans_' + idx).val(ui.item.id);
				}
			});
		}else{
			if(keycode == '8' || keycode == '46'){
				$('#edit_account_trans_' + idx + '_' + i).val('');
				$('#edit_account_id_trans_' + idx + '_' + i).val('');
			}

			$(".input-edit-account").on('keyup', function(){
				$('#edit_account_trans_' + idx + '_' + i).autocomplete({
					source: url,
					minLength: 0,
					select: function(event, ui){
						$('#edit_account_id_trans_' + idx + '_' + i).val(ui.item.id);
					}
				});
			});
		}
	}

	function saveEditTrans(idx){
		var date = $("#date_transaction_edit_" + idx).val();
		var description = $("#desc_transaction_edit_" + idx).val();
		var updated_at = $("#edit_updated_at_trans_" + idx).val();

		// validation
		if(!checkEditTransaction(idx)){
			return false;
		}

		transaction_edit['date_ori'] = date;
		transaction_edit['description'] = description;
		transaction_edit['updated_at'] = updated_at;
	}

	function checkEditTransaction(idx){
		var date = $("#date_transaction_edit_" + idx).val();
		var description = $("#desc_transaction_edit_" + idx).val();

		// Reset classes
		if($("#trans_date_msg_edit_" + idx).hasClass('has-error'))
			$("#trans_date_msg_edit_" + idx).removeClass("has-error");
		if(!$("#trans_date_msg_edit_" + idx + " > span").hasClass('hidden'))
			$("#trans_date_msg_edit_" + idx + " > span").addClass("hidden");
		if($("#trans_desc_msg_edit_" + idx).hasClass('has-error'))
			$("#trans_desc_msg_edit_" + idx).removeClass("has-error");
		if(!$("#trans_desc_msg_edit_" + idx + " > span").hasClass('hidden'))
			$("#trans_desc_msg_edit_" + idx + " > span").addClass("hidden");

		// check transaction date
		if(date == ''){
			if(!$("#trans_date_msg_edit_" + idx).hasClass('has-error'))
				$("#trans_date_msg_edit_" + idx).addClass("has-error");
			if($("#trans_date_msg_edit_" + idx + " > span").hasClass('hidden'))
				$("#trans_date_msg_edit_" + idx + " > span").removeClass("hidden");
			$("#trans_date_msg_edit_" + idx + " > span").html('You must fill transaction date.');
			$("#date_transaction_edit_" + idx).focus();
			return false;
		}

		// check transaction description
		if(description == ''){
			if(!$("#trans_desc_msg_edit_" + idx).hasClass('has-error'))
				$("#trans_desc_msg_edit_" + idx).addClass("has-error");
			if($("#trans_desc_msg_edit_" + idx + " > span").hasClass('hidden'))
				$("#trans_desc_msg_edit_" + idx + " > span").removeClass("hidden");
			$("#desc_transaction_edit_" + idx).focus();
			return false;
		}

		return true;
	}

	function saveEditLedger(){
		var idx = parseInt($(this).data('id'), 10);
		var transaction_id = parseInt($(this).data('transaction'), 10);

		if(!$("#msg_unbalance_" + idx).hasClass('hidden'))
			$("#msg_unbalance_" + idx).addClass('hidden');
		if(!$("#msg_less_than_one_" + idx).hasClass('hidden'))
			$("#msg_less_than_one_" + idx).addClass('hidden');

		for(x in ledger_accounts_edit){
			if($("#form_balance_" + idx + "_" + x).hasClass('has-error'))
				$("#form_balance_" + idx + "_" + x).removeClass('has-error');
		}

		if(ledger_accounts_edit.length < 2){
			if($("#msg_less_than_one_" + idx).hasClass('hidden'))
				$("#msg_less_than_one_" + idx).removeClass('hidden');
			$("#edit_account_trans_" + idx).focus();
			return false;
		}

		var total_balance = 0;
		for(x in ledger_accounts_edit){
			total_balance = total_balance + (ledger_accounts_edit[x]['receive'] - ledger_accounts_edit[x]['spend']);
		}

		if(total_balance != 0){
			if($("#msg_unbalance_" + idx).hasClass('hidden'))
				$("#msg_unbalance_" + idx).removeClass('hidden');
			if(!$("#balance_trans_" + idx + "_" + (ledger_accounts_edit.length - 1)).hasClass('has-error'))
				$("#balance_trans_" + idx + "_" + (ledger_accounts_edit.length - 1)).addClass('has-error');
			return false;
		}

		// render to make sure json string for the last
		saveEditTrans(idx);
		renderEdit(idx);

		var trans_xxx = JSON.parse($("#transaction-edit").val());

		var $form2 = $('#checkBeforeSaveForm'),
			url2 = $form2.attr( "action" );

		var formData2 = {
			_token: token,
			date : trans_xxx.date_ori
		};

		var request = $.ajax({
			url: url2,
			method: "GET",
			data: formData2,
			dataType: "json",
			success: function (data) {
				console.log(data[0]);
				$("#msgInformationRollBack").html('');
				if(data[0] == 'date_out_of_range'){
					if(!$("#trans_date_msg_edit_" + idx).hasClass('has-error'))
						$("#trans_date_msg_edit_" + idx).addClass("has-error");
					if($("#trans_date_msg_edit_" + idx + " > span").hasClass('hidden'))
						$("#trans_date_msg_edit_" + idx + " > span").removeClass("hidden");
					$("#trans_date_msg_edit_" + idx + " > span").html('Tanggal tidak ditemukan di semua buku.');
					return false;
				}else if(data[0] == 'book_not_active'){
					if(!$("#trans_date_msg_edit_" + idx).hasClass('has-error'))
						$("#trans_date_msg_edit_" + idx).addClass("has-error");
					if($("#trans_date_msg_edit_" + idx + " > span").hasClass('hidden'))
						$("#trans_date_msg_edit_" + idx + " > span").removeClass("hidden");
					$("#trans_date_msg_edit_" + idx + " > span").html('Tanggal yang dimasukan termasuk buku yang sudah ditutup (' + data[1] + ').');
					$("#msgInformationRollBack").html('Warning!!! Tanggal yang dimasukan termasuk buku yang sudah ditutup (' + data[1] + '). <strong style="color: red;">Buku pada tanggal yang dimasukan dan buku selanjutnya akan berubah!!!</strong>');
				}else if(data[0] == 'book_not_found'){
					return false;
				}

				if(data[0] == 'book_not_active'){
					var formData = {
						_token: token,
						id: transaction_id,
						transaction : $("#transaction-edit").val(),
						ledger_accounts : $("#ledger-accounts-edit").val(),
						book_roll_back_id : data[2]
					};
				}else if(data[0] == 'book_is_oke'){
					var formData = {
						_token: token,
						id: transaction_id,
						transaction : $("#transaction-edit").val(),
						ledger_accounts : $("#ledger-accounts-edit").val()
					};
				}

				$("#confirmRollBackModal").modal('show');

				$("#btn_ok_rollback").off('click').on('click', function(){
					// ajax post
					var $form = $('#saveEditLedgerForm'),
						url = $form.attr( "action" );

					var request = $.ajax({
						url: url,
						method: "POST",
						data: formData,
						dataType: "json",
						success: function (data) {
							if(data.status == 'danger'){
								if(data.code == 'date_out_of_range'){
									if(!$("#trans_date_msg_edit_" + idx).hasClass('has-error'))
										$("#trans_date_msg_edit_" + idx).addClass("has-error");
									if($("#trans_date_msg_edit_" + idx + " > span").hasClass('hidden'))
										$("#trans_date_msg_edit_" + idx + " > span").removeClass("hidden");
									$("#trans_date_msg_edit_" + idx + " > span").html('Date is out of range.');
								}

								if(data.code == 'already_edited'){
									render();
								}
							}else{
								render();
							}

							$.notify({
								title: '<strong>Notification!</strong>',
								message: data.message
							},{
								type: data.status
							});
						},
						error: function (data) {
							console.log('error');
						} 
					});
					ledger_accounts_edit = [];
					transaction_edit = [];
				});
			},
			error: function (data) {
				return false;
			} 
		});
	}

	getAccount();
	render();
});