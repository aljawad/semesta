//FOR GRID AND LIST //
$(document).ready(function() {
		$('.sp-pro-grid-b p').hide();
		$('.sp-pro-grid-b div').show();
		var d=$('.display');   
		var sg=$('.sp-grid');
		var sl=$('.sp-list');
		var sga=$('.sp-pro-grid-a');
		var sgb=$('.sp-pro-grid-b');	
		$('#viewcontrols button').on('click',function(e) 
		{
			if ($(this).hasClass('grid')) //JIKA GRID
			{
				d.fadeOut(500, function () 
				{
					$('.grid div').ready(function()
					{	
						d.removeClass('list').addClass('grid');			
						sg.removeClass('sp-list').addClass('sp-grid');			

						sg.removeClass('col-sm-12').addClass('col-sm-4');		
						sga.removeClass('col-sm-3').addClass('col-sm-12');
						sgb.removeClass('col-sm-9').addClass('col-sm-12');
						sga.removeClass('sp-pro-list-a').addClass('sp-pro-grid-a');	
						sgb.removeClass('sp-pro-list-b').addClass('sp-pro-grid-b');
						$('.sp-pro-grid-b center').removeClass('text-left');							
						$('.sp-pro-grid-b p').hide();
						$('.sp-pro-grid-b div').show();	
						$('.sp-pro-grid-b button').addClass('center-block');									
					});
					d.fadeIn(500);
		        });      
		 	}	
		 	else if($(this).hasClass('list')) 
		 	{
		  		d.fadeOut(500, function () 
		  		{
					$('.list div').ready(function()
					{	
						d.removeClass('grid').addClass('list');
						sg.removeClass('sp-grid').addClass('sp-list');

						sg.removeClass('col-sm-4').addClass('col-sm-12');			
						sga.removeClass('col-sm-12').addClass('col-sm-3');
						sgb.removeClass('col-sm-12').addClass('col-sm-9');
						sga.removeClass('sp-pro-grid-a').addClass('sp-pro-list-a');	
						sgb.removeClass('sp-pro-grid-b').addClass('sp-pro-list-b');	
						$('.sp-pro-list-b center').addClass('text-left');										
						$('.sp-pro-list-b .qv').hide();
						$('.sp-pro-list-b p').show();
						$('.sp-pro-list-b div').show();			
						$('.sp-pro-list-b button').removeClass('center-block');						
					}); 
		  			d.fadeIn(500);
		        });         
		 	}
	 	
		});
});