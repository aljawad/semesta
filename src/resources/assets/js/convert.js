$(function(){
	CreatePicklist('storage', '/storage/list?');
		
		var storage_id = $('#storageId').val();
		CreatePicklist('item_source', '/conversion/list?storage='+storage_id+'&');

		var id = $('#item_sourceId').val();
		CreatePicklist('item_dst', '/conversion/list?source='+id+'&');

		function getMultiplier(){
			$('#dstQty').val('Menghitung...');
			$('#btn_save').attr("disabled", true);
			var source = $('#item_sourceId').val();
			var dst = $('#item_dstId').val();
			$.ajax({
				
			    url:'/conversion/mtp?source='+source+'&dst='+dst,
			    type:"GET",
			    success:function(msg){
			        $('#dst_multiplier').val(JSON.parse(msg));
			        dstCalculate();
			        $('#btn_save').removeAttr("disabled");
			    },
			    dataType:"json"
			});
		}

		function dstCalculate(){
			var stock_quantity = $('#stock_quantity').val();
			var sourceQty = $('#sourceQty').val();
			if(sourceQty > stock_quantity){
				$('#sourceQty').val(stock_quantity);
			}
			var a = $('#sourceQty').val() * $('#dst_multiplier').val();
			if (isNaN(a)){
				$('#dstQty').val('');
			} else{
				$('#dstQty').val(a);
			}
		}
					
		$('#storageId').on('change', changeStorage);
		$('#item_sourceId').on('change', changeSource);
		$('#sourceQty').on('keyup', dstCalculate);
		$('#item_dstId').on('change', function (e, obj) {
			getMultiplier();
		});

		function changeStorage()
		{
			$('#item_dstName').val(null);
			$('#item_sourceName').val(null);
			$('#dstQty').val(null);
			var storage_id = $('#storageId').val();
			CreatePicklist('item_source', '/conversion/list?storage='+storage_id+'&');
		}

		function changeSource()
		{
			$('#item_dstName').val(null);
			$('#dstQty').val(null);
			var id = $('#item_sourceId').val();
			CreatePicklist('item_dst', '/conversion/list?source='+id+'&');
		}
});