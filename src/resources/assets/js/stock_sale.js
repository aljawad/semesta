$(function(){
	var purchase_items = JSON.parse($('#purchase-items').val());
	var purchase_shipping_cost_account = JSON.parse($('#shipping_accounts').val());
	var storage_id = $('#storageId').val();
	var date = encodeURIComponent($("#date").val());
	if($("#date").val() != '')
		CreatePicklist('stock', '/stock/list?storage='+storage_id+'&date='+date+'&');
			
		function render() 
		{
			getIndex();
			$('#purchase-items').val(JSON.stringify(purchase_items));

			var tmpl = $('#purchase-item-table').html();
			Mustache.parse(tmpl);
			var data = { item : purchase_items };
			var html = Mustache.render(tmpl, attachHelper(data));
			$('#tbody-purchase-item').html(html);
			
			hitungTotalSemua();
			bind();

			$('#cash_label').hide();
			$('#credit_label').hide();
			$('#cash_account').hide();
		}

		function attachHelper(data){
			data.formatMoney = function() {
				return function(val, render) {
					val = render(val);
					var i = parseFloat(val);
					return i.formatMoney();
				}
			};

			return data;
		}

		function bind()
		{
			$('#AddItemButton').on('click', tambahItem);
			$('.btn-delete-item').on('click', deleteItem);
			$('.btn-edit-item').on('click', editItem);
			$('.btn-save-item').on('click', saveItem);
			$('.btn-cancel-item').on('click', cancelEdit);

			$('.input-price').on('keyup', hitungTotalItem);
			$('.input-qty').on('keyup', hitungTotalItem);
			$('.input-discount').on('keyup', hitungTotalItem);
			$('.input-item').on('keyup', hitungTotalItem);
			$('.input-tax').on('keyup', hitungTotalItem);

			$('.input-all').on('keyup', hitungTotalSemua);
			$('#total_paid').on('keyup', hitungKreditOrCash);

			$('#storageId').on('change', changeStorage);
			$('#stockId').on('change', checkMaxQuantity);
			$('#date').on('change', changeDate);

			$('.input-new').keypress(function(e) {
				if (e.keyCode == 13) {
					e.preventDefault();
					tambahItem();
				}
			});

			$('.input-edit').keypress(function(e) {
				var i = $(this).data('id');

				if (e.keyCode == 13) {
					e.preventDefault();
					$('#simpan_' + i).click();
				}
			});
		}

		function getIndex() 
		{
			for (idx in purchase_items) {
				purchase_items[idx]['_id'] = idx;
				purchase_items[idx]['no'] = parseInt(idx) + 1;
			}
		}

		function tambahItem() 
		{
			var item_name = $('#stockName').val();
			var item_id = $('#stockId').val();
			var harga = $('#harga').val();
			var qty = $('#qty').val();
			var tax = $('#tax').val();
			var discount = $('#discount').val();
			var total = $('#total').val();

			if(discount == '')
				discount = 0;

			var input = {
				'item_id' : item_id,
				'item_name' : item_name,
				'price' : harga,
				'qty' : qty,
				'tax' : tax,
				'discount' : discount,
				'total' : total,
				'tcop' : Number(harga) - Number(discount),
			};

			var diff = checkItem(item_id, harga);
			if (!diff) {
				$('#errorInput').removeClass('hidden');
				return;
			}
			
			if (harga > 0 && qty > 0 && discount >= 0 && total >= 0 && item_name && item_id)
				purchase_items.push(input);

			render();
		}

		function checkItem(item_id, price, index)
		{
			for (var i in purchase_items) {
				var item = purchase_items[i];
				if (i == index)
					continue;

				if (item.item_id == item_id)
					return false;
			}

			return true;
		}

		function deleteItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			purchase_items.splice(i, 1);
			render();
		}

		function editItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			//$('#item_' + i).addClass('hidden');
			$('#price_' + i).addClass('hidden');
			$('#qty_' + i).addClass('hidden');
			$('#tax_' + i).addClass('hidden');
			$('#discount_' + i).addClass('hidden');
			$('#total_' + i).addClass('hidden');
			
			$('#edit_' + i).addClass('hidden');
			$('#delete_' + i).addClass('hidden');
			
			//$('#itemInputName_' + i).removeClass('hidden');
			$('#priceInput_' + i).removeClass('hidden');
			$('#qtyInput_' + i).removeClass('hidden');
			$('#taxInput_' + i).removeClass('hidden');
			$('#discountInput_' + i).removeClass('hidden');
			$('#totalInput_' + i).removeClass('hidden');
			$('#cogs_text_' + i).removeClass('hidden');
			$('#stock_max_text_' + i).removeClass('hidden');

			$('#simpan_' + i).removeClass('hidden');
			$('#cancel_' + i).removeClass('hidden');
		}

		function cancelEdit() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			//$('#item_' + i).removeClass('hidden');
			$('#price_' + i).removeClass('hidden');
			$('#qty_' + i).removeClass('hidden');
			$('#tax_' + i).removeClass('hidden');
			$('#discount_' + i).removeClass('hidden');
			$('#total_' + i).removeClass('hidden');
			
			$('#edit_' + i).removeClass('hidden');
			$('#delete_' + i).removeClass('hidden');
			
			//$('#itemInputName_' + i).addClass('hidden');
			$('#priceInput_' + i).addClass('hidden');
			$('#qtyInput_' + i).addClass('hidden');
			$('#taxInput_' + i).addClass('hidden');
			$('#discountInput_' + i).addClass('hidden');
			$('#totalInput_' + i).addClass('hidden');
			$('#cogs_text_' + i).addClass('hidden');
			$('#stock_max_text_' + i).addClass('hidden');		

			$('#simpan_' + i).addClass('hidden');
			$('#cancel_' + i).addClass('hidden');
			$('#errorInput_' + i).addClass('hidden');
		}

		function saveItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			var item_id = $('#itemInputId_' + i).val();
			var price = $('#priceInput_' + i).val();
			var qty = $('#qtyInput_' + i).val();
			var tax = $('#taxInput_' + i).val();
			var discount = $('#discountInput_' + i).val();
			var total = $('#totalInput_' + i).val();

			var diff = checkItem(item_id, price, i);
			if (!diff) {
				$('#errorInput').addClass('hidden');
				$('#errorInput_' + i).removeClass('hidden');
				return;
			}

			if (price >= 0 && qty >= 0 && discount >= 0 && total >= 0) {
				purchase_items[i].price = price;
				purchase_items[i].qty = qty;
				purchase_items[i].tax = tax;
				purchase_items[i].discount = discount;
				purchase_items[i].total = total;
				purchase_items[i].tcop = Number(price) - Number(discount);
			}

			render();
		}

		function hitungTotalItem(){
			var i = parseInt($(this).data('id'), 10);
			
			if (isNaN(i)){
				var stock_max = $( "#stockMax" ).val();

				if (isNaN($('#qty').val())){
					$('#qty').val(0);
				}

				if(Number($('#qty').val()) > stock_max){
					$('#qty').val(stock_max);
				}

				getRecomendedPrice(i);

				var harga = $('#harga').val();
				if(isNaN(harga))
					harga = 0;

				var qty = $('#qty').val();
				if(isNaN(qty))
					qty = 0;

				var tax = $('#tax').val();
				if(isNaN(tax))
					tax = 0;

				var discount = $('#discount').val();
				if(isNaN(discount))
					discount = 0;

				var total = (harga*qty)-(discount*qty);
				
				$('#total').val(total);

			}
			else
			{
				getRecomendedPrice(i);
				var price = $('#priceInput_' + i).val();
				if(isNaN(price))
					price = 0;

				var qty = $('#qtyInput_' + i).val();
				if(isNaN(qty))
					qty = 0;

				var tax = $('#taxInput_' + i).val();
				if(isNaN(tax))
					tax = 0;

				var discount = $('#discountInput_' + i).val();
				if(isNaN(discount))
					discount = 0;

				var total = (price*qty)-(discount*qty)+(tax*qty);
				$('#totalInput_' + i).val(total);
			}
		}

		function hitungTotalSemua(){
			var total_item = 0;

			for (i in purchase_items) {
				total_item = total_item + Number(purchase_items[i].total);
			}

			var diskon_total = 0;
			var biaya_kirim = 0;
			var dynamic_costs = 0;

			for(x in purchase_shipping_cost_account){
				if(purchase_shipping_cost_account[x].type == '-'){
					dynamic_costs = dynamic_costs - Number($('#biaya-kirim-' + x).val());
				}else if(purchase_shipping_cost_account[x].type == '+'){
					dynamic_costs = dynamic_costs + Number($('#biaya-kirim-' + x).val());
				}
			}

			var total_semua = Number(total_item) - Number(diskon_total) + Number(biaya_kirim) + Number(dynamic_costs);
			
			$('#total-semua').val(total_semua);
			hitungKreditOrCash();
		}

		function hitungKreditOrCash(){
			var a = parseInt($('#total-semua').val());
			var b = parseInt($('#total_paid').val());
			var cek;

			if(b == a){
				cek = true;
			}else if(b < a){
				cek = false;
			}else if(b > a){
				cek = true;
				$('#total_paid').val(a);
			}

			if(cek)
				showCashLabel();
			else
				showCreditLabel();
			console.log(a);
		}

		function showCashLabel(){
			$('#credit_label').hide();
			$('#cash_label').show();
			$('#cash_account').show();
		}

		function showCreditLabel(){
			$('#credit_label').show();
			$('#cash_label').hide();
			$('#cash_account').hide();
		}

		function changeStorage(){
			render();
			$("#stockId").val('');
			$("#stockName").val('');
			$("#stockMax").val('');
			$('#harga').val(0);
			$('#qty').val(0);
			$('#tax').val(0);
			$('#discount').val(0);
			$('#total').val(0);
			var storage_id = $('#storageId').val();
			var date = encodeURIComponent($("#date").val());
			//console.log(date);
			if($("#date").val() != '')
				CreatePicklist('stock', '/stock/list?storage='+storage_id+'&date='+date+'&');
		}

		function changeDate(){
			$("#storageId").val('');
			$("#storageName").val('');
			$("#stockId").val('');
			$("#stockName").val('');
			$("#stockMax").val('');
			purchase_items = [];
		}

		function checkMaxQuantity(i){
			if(isNaN(i)){
				var id = $("#stockId").val();
				var date = $("#date").val();
				var stock_max;

				if(id != '' && date != ''){
					var request = $.ajax({
						url: base_url + '/stock/quantity',
						method: "GET",
						data:{id: id, date: date},
						dataType: "json",
						success: function (msg) {
							stock_max = msg.quantity;
							for(i in purchase_items){
								if(purchase_items[i].item_id == id){
									stock_max = stock_max - purchase_items[i].qty;
								}
							}

			                $("#stockMax").val( stock_max );
			                $("#stock_max_text").text("Max: " + stock_max);
							//console.log(stock_max);
			            },
			            error: function (data) {
			                console.log('error!!!');
			            } 
					});
				}
			}else{
				var id = $("#itemInputId_" + i).val();
				var date = $("#date").val();
				var stock_max;

				if(id != '' && date != ''){
					var request = $.ajax({
						url: base_url + '/stock/quantity',
						method: "GET",
						data:{id: id, date: date},
						dataType: "json",
						success: function (msg) {
							stock_max = msg.quantity;
							for(x in purchase_items){
								if(purchase_items[x].item_id == id && purchase_items[x]._id != i){
									stock_max = stock_max - purchase_items[x].qty;
								}
							}

			                $("#itemInputMaxQty_" + i).val( stock_max );
			                $("#stock_max_text_" + i).text("Max: " + stock_max);
							//console.log(stock_max);
			            },
			            error: function (data) {
			                console.log('error!!!');
			            } 
					});
				}
			}
			
		}

		function getRecomendedPrice(i){
			checkMaxQuantity(i);
			if(isNaN(i)){
				var id = $("#stockId").val();
				var date = $("#date").val();
				var qty = $("#qty").val();
				var discount = $("#discount").val();
				var used = 0;
				var recomended;

				for(i in purchase_items){
					if(purchase_items[i].item_id == id){
						used = Number(used) + Number(purchase_items[i].qty);
					}
				}

				if(id != '' && date != ''){
					var request = $.ajax({
						url: base_url + '/stock/price',
						method: "GET",
						data:{id: id, date: date, used: used, qty: qty},
						dataType: "json",
						success: function (data) {
							if(isNaN(discount))
								discount = 0;

							recomended = Number(discount) + Number(data.price);
			                //$( "#cogs_text" ).text("Recomended: > " + recomended);
			            },
			            error: function (msg) {
			                console.log('failed');
			            } 
					});
				}
			}else{
				var id = $("#itemInputId_" + i).val();
				var date = $("#date").val();
				var qty = $("#qtyInput_" + i).val();
				var discount = $("#discountInput_" + i).val();
				var used = 0;
				var recomended;

				for(x in purchase_items){
					if(purchase_items[x].item_id == id && purchase_items[x]._id != i){
						used = Number(used) + Number(purchase_items[x].qty);
					}
				}

				if(id != '' && date != ''){
					var request = $.ajax({
						url: base_url + '/stock/price',
						method: "GET",
						data:{id: id, date: date, used: used, qty: qty},
						dataType: "json",
						success: function (data) {
							if(isNaN(discount))
								discount = 0;

							recomended = Number(discount) + Number(data.price);
			                $( "#cogs_text_" + i).text("Recomended: > " + recomended);
			            },
			            error: function (msg) {
			                console.log('failed');
			            } 
					});
				}
			}
			
		}

		render();
});