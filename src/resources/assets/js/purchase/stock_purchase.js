$(function(){
	var purchase_items = JSON.parse($('#purchase-items').val());
			
		function render() 
		{
			getIndex();
			$('#purchase-items').val(JSON.stringify(purchase_items));

			var tmpl = $('#purchase-item-table').html();
			Mustache.parse(tmpl);
			var data = { item : purchase_items };
			var html = Mustache.render(tmpl, attachHelper(data));
			$('#tbody-purchase-item').html(html);
			
			bind();
			hitungTotalSemua();
			CreatePicklist('item', '/stocker/item/list?');
		}

		function attachHelper(data){
			data.formatMoney = function() {
				return function(val, render) {
					val = render(val);
					var i = parseFloat(val);
					return i.formatMoney();
				}
			};

			return data;
		}

		function bind()
		{
			$('#AddItemButton').on('click', tambahItem);
			$('.btn-delete-item').on('click', deleteItem);
			$('.btn-edit-item').on('click', editItem);
			$('.btn-save-item').on('click', saveItem);
			$('.btn-cancel-item').on('click', cancelEdit);

			$('.input-price').on('keyup', hitungTotalItem);
			$('.input-qty').on('keyup', hitungTotalItem);
			$('.input-discount').on('keyup', hitungTotalItem);
			$('.input-item').on('keyup', hitungTotalItem);

			$('.input-all').on('keyup', hitungTotalSemua);

			$('.input-new').keypress(function(e) {
				if (e.keyCode == 13) {
					e.preventDefault();
					tambahItem();
				}
			});

			$('.input-edit').keypress(function(e) {
				var i = $(this).data('id');

				if (e.keyCode == 13) {
					e.preventDefault();
					$('#simpan_' + i).click();
				}
			});
			
		}

		function getIndex() 
		{
			for (idx in purchase_items) {
				purchase_items[idx]['_id'] = idx;
				purchase_items[idx]['no'] = parseInt(idx) + 1;
			}
		}

		function tambahItem() 
		{
			var item_name = $('#itemName').val();
			var item_id = $('#itemId').val();
			var harga = $('#harga').val();
			var qty = $('#qty').val();
			var discount = $('#discount').val();
			var total = $('#total').val();

			var input = {
				'item_id' : item_id,
				'item_name' : item_name,
				'price' : harga,
				'qty' : qty,
				'discount' : discount,
				'total' : total,
			};

			var diff = checkItem(item_id, harga);
			if (!diff) {
				$('#errorInput').removeClass('hidden');
				return;
			}
			
			if (harga >= 0 && qty >= 0 && discount >= 0 && total >= 0 && item_name && item_id)
				purchase_items.push(input);

			render();
		}

		function checkItem(item_id, price, index)
		{
			for (var i in purchase_items) {
				var item = purchase_items[i];
				if (i == index)
					continue;

				if (item.item_id == item_id && item.price == price)
					return false;
			}

			return true;
		}

		function deleteItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			purchase_items.splice(i, 1);
			render();
		}

		function editItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			//$('#item_' + i).addClass('hidden');
			$('#price_' + i).addClass('hidden');
			$('#qty_' + i).addClass('hidden');
			$('#discount_' + i).addClass('hidden');
			$('#total_' + i).addClass('hidden');
			
			$('#edit_' + i).addClass('hidden');
			$('#delete_' + i).addClass('hidden');
			
			//$('#itemInputName_' + i).removeClass('hidden');
			$('#priceInput_' + i).removeClass('hidden');
			$('#qtyInput_' + i).removeClass('hidden');
			$('#discountInput_' + i).removeClass('hidden');
			$('#totalInput_' + i).removeClass('hidden');			

			$('#simpan_' + i).removeClass('hidden');
			$('#cancel_' + i).removeClass('hidden');
		}

		function cancelEdit() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			//$('#item_' + i).removeClass('hidden');
			$('#price_' + i).removeClass('hidden');
			$('#qty_' + i).removeClass('hidden');
			$('#discount_' + i).removeClass('hidden');
			$('#total_' + i).removeClass('hidden');
			
			$('#edit_' + i).removeClass('hidden');
			$('#delete_' + i).removeClass('hidden');
			
			//$('#itemInputName_' + i).addClass('hidden');
			$('#priceInput_' + i).addClass('hidden');
			$('#qtyInput_' + i).addClass('hidden');
			$('#discountInput_' + i).addClass('hidden');
			$('#totalInput_' + i).addClass('hidden');			

			$('#simpan_' + i).addClass('hidden');
			$('#cancel_' + i).addClass('hidden');
			$('#errorInput_' + i).addClass('hidden');
		}

		function saveItem() 
		{
			var i = parseInt($(this).data('id'), 10);
			
			var item_id = $('#itemInputId_' + i).val();
			var price = $('#priceInput_' + i).val();
			var qty = $('#qtyInput_' + i).val();
			var discount = $('#discountInput_' + i).val();
			var total = $('#totalInput_' + i).val();

			var diff = checkItem(item_id, price, i);
			if (!diff) {
				$('#errorInput').addClass('hidden');
				$('#errorInput_' + i).removeClass('hidden');
				return;
			}

			if (price >= 0 && qty >= 0 && discount >= 0 && total >= 0) {
				purchase_items[i].price = price;
				purchase_items[i].qty = qty;
				purchase_items[i].discount = discount;
				purchase_items[i].total = total;
			}

			render();
		}

		function hitungTotalItem(){
			var i = parseInt($(this).data('id'), 10);
			
			if (isNaN(i)){
				var harga = $('#harga').val();
				var qty = $('#qty').val();
				var discount = $('#discount').val();
				var total = (harga*qty)-(discount*qty);
				
				$('#total').val(total);

			}
			else
			{
				var price = $('#priceInput_' + i).val();
				var qty = $('#qtyInput_' + i).val();
				var discount = $('#discountInput_' + i).val();
				var total = (price*qty)-(discount*qty);
				$('#totalInput_' + i).val(total);
			}
		}

		function hitungTotalSemua(){
			var total_item = 0;

			for (i in purchase_items) {
				total_item = total_item + Number(purchase_items[i].total);
			}

			var diskon_total = $('#diskon-total').val();
			var biaya_kirim = $('#biaya-kirim').val();
			var pajak = $('#pajak').val();
			if(typeof pajak == 'undefined')
				pajak = 0;
			console.log(pajak);
			var total_semua = Number(total_item) - Number(diskon_total) + Number(biaya_kirim) + Number(pajak);
			
			$('#total-semua').val(total_semua);
		}
		render();
});