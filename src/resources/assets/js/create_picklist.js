function CreatePicklist(name, url) {
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';

	function itemAjax()
	{	
		var q = $(search).val();
		
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');
		
		$.ajax({
	  		url: url + '&q=' + q,		  
		})
		.done(function(data) {
		    $(table).html(data);
		    pagination(name);

		    $(table).removeClass('hidden');
		    $(modal).find('.shade-screen').addClass('hidden');
		    $(modal).find('.form-search').removeClass('hidden');
		    
		    $(table).find('.btn-choose').on('click', chooseItem);
	  	});
	}

	function chooseItem()
	{
		var id = $(this).data('id');
		var name = $(this).data('name');
		var obj = $(this).data('obj');
		
		$(item_id).val(id);
		$(item_name).val(name);

		if (!obj || obj.length == 0)
			obj = [{}];

		$(item_id).trigger('change', obj);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}
	
	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');
	});

	itemAjax();
}
		