function CheckPrice(data, order_id)
{
	var user_data = data.user_data;

	function send(data, order_id)
	{
		var total = $('#totprice');
		var discount = $('#discount');

		if (user_data.key >= 0) {
			total = $('#totprice_' + user_data.key);
			discount = $('#discount_' + user_data.key);
		}
		
		total.val('Menghitung...');
		discount.val('Menghitung...');

		$.ajax ({
			url: '/order/price/' + order_id,
			type: 'GET',
			success: success,
			data: data,
			cache: false,
			dataType: 'json',
		});
	}

	function calculate(key, data)
	{
		var total = 0;
		var error = false;
		var i = 0;

		$('.subtotal').each(function () {
			//adjust price
			var item_id = $(this).data('id');
			if (data && data.update && data.update[item_id])  {
				$(this).data('total', data.update[item_id].total);
				$(this).data('subtotal', data.update[item_id].total - data.update[item_id].disc_amount);
				$(this).data('discount', data.update[item_id].disc_amount);
				$(this).data('discountpercent', data.update[item_id].disc_percent);
			}

			// skip on subtotal parsing error
			var subtotal = parseInt($(this).data(key), 10);
			if (isNaN(subtotal)) {
				error = true;
				return;
			}
			var html = 'Rp' + subtotal.formatMoney();
			$(this).html(html);
			$(this).data('subtotal', subtotal);

			var disc = $('#discountLabel_' + i).data('discount');
			if (disc > 0)
				$('#discountLabel_' + i).html('Rp' + disc.formatMoney());
			else
				$('#discountLabel_' + i).html('-');

			i++;
			total += subtotal;
		});

		if (error)
			$('#total').html('-ERROR-');
		else
			$('#total').html('Rp' + total.formatMoney());
	}

	function success(data)
	{
		var compare = {
			'random' : parseInt(data.user_data.random, 10),
			'key' : parseInt(data.user_data.key, 10)
		};

		if (JSON.stringify(compare) == JSON.stringify(user_data)) {
			var total = $('#totprice');
			var discount = $('#discount');
			var disc_amount = 0;

			//apply minimum order
			if (data.get_discount)
				disc_amount = data.disc_amount;

			var calctotal = data.total - disc_amount;
			if (!isNaN(compare.key)) {
				total = $('#totprice_' + compare.key);
				discount = $('#discount_' + compare.key);
				$('#subtotal_' + compare.key).data('subtotal', calctotal);
				calculate('subtotal', data);
			}

			total.val('Rp' + calctotal.formatMoney());
			total.data('subtotal', calctotal);
			discount.val('Rp' + disc_amount.formatMoney());
		}
	}

	if (typeof data === 'object')
		send(data, order_id);
	else
		calculate(data);
}