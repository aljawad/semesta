function CheckSubmitBarcode() {
	var time = new Date();
	var paste = false;
	var obj = $('.input-barcode input:text');

	function checkHash()
	{
		var hash = window.location.hash;

		$('.item-hash').removeClass('active');
		if (hash)
			$(hash).addClass('active');

		obj.focus();
	}

	obj.on('paste', function() {
		paste = true;
	});

	obj.on('change', function() {
		if (!paste)
			time = new Date();
	});

	$('#formSearch').on('submit', function() {
		var submit = new Date();
		if (time == submit)
			return true;

		var diff = submit.getTime() - time.getTime();
		if (diff < 1000) {
			$('#inputBarcode').val('true');
			return true;
		}
	});

	checkHash();
	$(window).on('hashchange', checkHash);
}