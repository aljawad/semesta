<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContactUs;

use Auth;
use Session;
use DB;

class ContactUsController extends Controller
{
    public function index(){
        $name = "";
        $email = "";
        if(Auth::customer()->get()){
            $name = Auth::customer()->get()->first_name ." ".Auth::customer()->get()->last_name;
            $email = Auth::customer()->get()->email;
        }
        return view('simpleshop.pages.contact_us', compact('name', 'email'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactUs $contactUs, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:127',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        $customer = Auth::customer()->get();

        DB::beginTransaction();
            $contactUs->name = $request->name;
            $contactUs->email = $request->email;
            $contactUs->message = $request->message;
            $contactUs->status = "new";
            $contactUs->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $contactUs->save();
        DB::commit();

        return redirect()->back()
            ->with('_SENT_SUCCESS', true);
    }
}
