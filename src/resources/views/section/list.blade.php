@extends('master', ['active' => 'section'])
@section('sidebar')
 	@include('section.sidebar', ['active' => 'section'])
@endsection
@section('content')

<div class="container-fluid">
    <div class="col-xs-12"> 
    @include('form.search',['url'=>'/admin/section','link'=>'/admin/section'])
    </div>
   
        <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Section</th>
                            <th>Chapter</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($section) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($section as $key => $section)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $section->name }}</td>
                                <td>{{ $section->chapter->name }}</td>

                                <td>
                                    <a class="btn btn-primary btn-xs" href="{{ action('SectionController@edit', $section->id) }}">edit</a>
                                    <a class="btn btn-primary btn-xs" href="{{ action('ContentController@add', $section->id) }}">add content</a>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    <a class="btn btn-xs btn-danger" href="{{ action('SectionController@delete', $section->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                
            </div>
            <div class="pull-right">
                
            </div>
        </div>
    </div>
</div>
@endsection
