@extends('master', ['active' => 'section'])
@section('sidebar')
    @include('section.sidebar', ['active' => 'add'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Add New Section</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/admin/section') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('ContentController@addcontent'),
                    'enctype'=> 'multipart/form-data', 'files',
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Name',
                'placeholder' => 'Name',
                'default' => ''
            ])

            @include('form.select', [
                'field' => 'section_id',
                'label' => 'Chapter',
                'options' => 
                    [''=>'']+App\Section::pluck('name','id')->all(), null,              
                'default' => ''
            ])

            @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}


@endsection

