@extends('master', ['active' => 'section'])
@section('sidebar')
    @include('section.sidebar', ['active' => 'add'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Add New Section</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/admin/section') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('SectionController@store'),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Name',
                'placeholder' => 'Name',
                'default' => ''
            ])

            @include('form.select', [
                'field' => 'chapter_id',
                'label' => 'Chapter',
                'options' => 
                    [''=>'']+App\Chapter::pluck('name','id')->all(), null,              
                'default' => ''
            ])

            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Description',
                'placeholder' => 'Description',
                'default' => ''
            ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}


@endsection

