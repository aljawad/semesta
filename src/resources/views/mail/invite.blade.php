<p>
    Hello {{ $user->name }}. 
</p>
<p>
    You have been added to SAAS Web TV as a teacher. You can manage your courses by logging in to our official website through:
    <br>

    <a href="{{ $login = url('/login') }}">{{ $login }}</a>.
</p>
<p>
    Here is your account information to access website.
</p>
		<table align="left" style="margin-top: 10px; margin-bottom: 10px;">
                    
                    <tr>
                        <th align="left">Name</th>
                        <td>:</td>
                        <td>{{$user->name}}</td>
                    </tr>
                    <tr>
                        <th align="left">Email </th>
                        <td>:</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <th align="left">Password</th>
                        <td>:</td>
                        <td>{{ $password }}</td>
                    </tr>
        </table>

        <br>
        <br>
<div style="margin-top: 80px">
        <p>Sincerely,</p>
        <p>Semesta Web TV </p>
        <p>===</p>
</div>
            <br>
        <p>If you have any inquiries, please kindly email us at {{config('settings.email')}}. We will reply you as soon as we could and please do not reply this email.</p>

    
    

    <div style="font-size: 11px; margin-top: 10px">

    This email was sent automatically by Semesta.

    </div>