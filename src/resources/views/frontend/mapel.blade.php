@extends('layouts.frontend')

@section('content')
<div style="padding-top:120px;">
	<div class="container">
		<ul class="breadcrumb-customs">
			<li><a href="{{ url('/') }}">Home</a></li> >
			<li><a class="active" href="{{ url('/subjects') }}">Mata pelajaran</a></li>
		</ul>
		{{-- pelajaran pilihan --}}
		<div class="underlined-title title-content text-center">
			<h3>Mapel Pilihan</h3>
			<hr>
		</div>
		<div class="row">
			<div class="col-sm-10 col-xs-12 col-sm-offset-1 text-center">

				<br><br>
			</div>
		</div>
		<div class="row">
			@forelse ($subjects as $subject)
			<div class="col-sm-4 course-subject">
				<a href="{{ url('subject/'.$subject->id)}}">
					<img src="{{ url('/images/'.$subject->images)}}">
					<div class="course">
						<p>{{ $subject->name }}</p>
					</div>
				</a>
			</div>
			@empty
			{{-- empty expr --}}
			<h1> oopps.. subject not found</h1>
			@endforelse
		</div>
	</div>
</div>
@endsection
@section('content-js')
@endsection
