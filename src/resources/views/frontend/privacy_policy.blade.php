@extends('layouts.frontend')
@section('content-css')
<style media="screen">
	.about-header{
		padding-top: 50px;
		height: 170px;
	}
	.mycontent{
	  margin-top: 10vh;
		margin-bottom: 10vh;
    text-align: justify;
	}
</style>
@endsection
@section('content')
<div class="content-margin-top about-header">
		<h1>Privacy Policy</h1>
</div>
<div class="mycontent container">
	<h1>Privacy Policy</h1>
<p>We encourage you to read this Policy before using our Site. By using the Site, you agree to the terms and conditions of this Policy. If you do not agree to the terms and conditions of this Policy, please do not use the Site.</p>
<h3>Personal Information.</h3>
<p>You are not ordinarily required to register or provide Personal Information in order to visit our Site, though certain functionalities (such as a membership program) may require registration. Personal Information may include your contact information (such as your name, telephone numbers and email address).</p>
<h3>Use and Disclosure of Information Semesta Web TV Gathers Through the Site</h3>
<ol>
	<li>Personal Information. </li>
	<p>We may use your Personal Information in any of the following ways.</p>
	<li>Communications.</li>
	<p> We encourage Visitors to contact us with questions and comments. Personal Information may be used in order to respond to your questions and comments.</p>
	<li>Business Purposes. </li>
	<p>We may also use your Personal Information for internal business purposes, such as analysing and managing our businesses. The Personal Information you provide through the Site may be combined with demographic information and other information that is publicly available, or with other information about you that Semesta Web TV may otherwise obtain online or offline.</p>
	<li>Special Promotions and Product Information. </li>
	<p>In addition, Semesta Web TV may use your Personal Information to provide you with email updates and announcements concerning our services. You may opt-out of receiving these messages from us at any time (see &ldquo;Contact Us&rdquo; below).</p>
	<li>Critical Communications. </li>
	<p>From time to time we may use your Personal Information to email you important information regarding the Site, or changes to our terms, conditions, and policies (&ldquo;Critical Emails&rdquo;). Because this information may be critical to your use of the Site, you may not opt-out of receiving these emails.</p>
	<li>Third Party Service Provider. </li>
	<p>We work with third parties who provide services, including web site hosting, data analysis, promotional activities, and other administrative services. We may share your Personal Information with such third parties for the purpose of enabling such third parties to provide such services.</p>
	<p>Miscellaneous. Finally, we may disclose your Personal Information if we believe that we are required to do so: (i) by law; (ii) to comply with legal process or governmental requests; (iii) to enforce our Terms of Service; (iv) to protect our operations; (v) to protect the rights, privacy, safety or property of Semesta Web TV, you or others; and (vi) to permit us to pursue available remedies or limit the damages that we may sustain.</p>

</ol>
<h3>Other Important Notes Regarding Our Privacy Practices.</h3>
<ol>
	<li>Security.</li> <p>No method of transmitting or storing data is completely secure. As a result, although we strive to protect your Personal Information, we cannot guarantee the security of any information you transmit to us through or in connection with the Site. If you have reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of any account you might have with us has been compromised), you must immediately notify us of the problem by contacting us in accordance with "Contact Us," below (note that physical mail notification will delay the time it takes for us to respond to the problem).</p>
	<li>Security Measures. </li><p>Semesta Web TV has set up appropriate security measures, but despite its best efforts, it cannot completely prevent hacking. Semesta Web TV assumes no liability in the event that an account is hacked.</p>

</ol>
<h3>Updating Your Information and Contacting Us.</h3>
<p> You may review the information we have collected and where appropriate you may request that it be corrected. We also provide you with you several options for reviewing, correcting, updating or otherwise modifying information you have previously provided:</p>
<p>You may email us at&nbsp;<a href="mailto:admin@semestaweb.tv"><span style="font-family: Helvetica, serif;"><span style="font-size: small;">admin@semestaweb.tv</span></span></a></p>
<p>Please clearly indicate the information that you wish to review or have changed. We will endeavour to comply with your request as soon as reasonably possible. Note that despite any removal of or change to Personal Information requested there may also be residual information that will remain within our databases and other records, which will not be removed or changed.</p>
<p>Choice.&nbsp;From time to time, we may ask you to indicate whether you are interested in receiving emails and other information from Semesta Web TV. If you elect to receive these communications, we will occasionally send you emails or other information that match your requests and offer you promotions and coupons.&nbsp;<br /> If at any time you wish to stop receiving these email communications from us, please just let us know by emailing, calling or writing to us using the contact information listed above in Contacting Us or by using the unsubscribe feature in the email you received. Please indicate that you wish to stop receiving email communications from Semesta Web TV.<br /> In addition, we do not disclose your Personal Information to third parties for the third-party's direct marketing purposes unless we have received and processed your consent to such sharing.</p>
<p><a name="_GoBack"></a> Changes to This Policy.&nbsp;We reserve the right to change this Policy, and any of our policies or procedures concerning the treatment of information collected through the Site, without prior notice. You can determine when this Policy was last revised by referring to the "Last Updated" legend at the top of this page. Any changes to our Policy will become effective upon posting of the revised Policy on the Internet, accessible through the Site. Use of the Site following such changes constitutes your acceptance of the revised Policy then in effect. We encourage you to bookmark this page and to periodically review it to ensure familiarity with the most current version of our Policy.<br /> This Policy represents the sole, authorized statement of Semesta Web TV's practices with respect to the collection of Personal Information (defined below) through the Site and Semesta Web TV's use of such information. Any summaries of this Policy generated by third party software or otherwise shall have no legal effect and will not bind Semesta Web TV.</p>
<p>&nbsp;</p>
</div>
@endsection
