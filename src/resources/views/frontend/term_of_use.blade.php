@extends('layouts.frontend')
@section('content-css')
<style media="screen">
	.about-header{
		padding-top: 50px;
		height: 170px;
	}
	.mycontent{
	  margin-top: 10vh;
		margin-bottom: 10vh;
    text-align: justify;
	}
</style>
@endsection
@section('content')
<div class="content-margin-top about-header">
		<h1>Term Of Use</h1>
</div>
<div class="mycontent container">
	<h1>Terms &amp; Conditions</h1>
<p>We are the authorized provider of the Service (as defined herein) in Indonesia. It is a condition precedent to you being able to access, or register for any Service from us that you must read and agree to be bound by each and every one of the terms and conditions contained in this Agreement. Should you access any component of the Service or use or register for the Service in any way, such activities on your part expressly means that you have read this Agreement and agree to be bound by the terms and conditions contained herein.</p>
<p>Should you not agree to be bound by each and every term and condition contained in this Agreement, you must not access, buy, use or register for the Service in any way.</p>
<h3>1. The Service</h3>
<p>The Service comprises online provisions of video lessons, text, quiz and or other materials provided for learning purpose.</p>
<p>We reserve the right to refuse to allow use of Service to any party for any reason we consider appropriate in our absolute discretion.</p>
<h3>2. Your Account</h3>
<p>You can only subscribe for the Service if you have registered as a member or become a user on any of SAAS service platform. </p>
<p>Don't reveal your Access Code information to anyone else. You are solely responsible for maintaining the confidentiality and security of your Access Code and for all activities that occur on or through your Access Code, and you agree to immediately notify us of any security breach of your Access Code. We are not responsible for any losses arising out of the unauthorized use of your Access Code.</p>
<p>You understand and agree that your domain administrator may have access to your account, and may suspend or terminate your account access and your ability to modify your account.</p>
<h3>3. Digital Content</h3>
<p>The Service will allow you to access the Digital Content and subsequently downloading the Digital Content to your own devices for your further use for indefinite period of time </p>
<p>The basis on which the Digital Content is available on the Service will be indicated on the product detail page for that Digital Content on the Service. From time to time, we may add or remove the Digital Content from the Service and may change the basis on which the Digital Content is available on the Service.</p>
<p>We reserve the right to include such Digital Content as we consider appropriate in the Service at our absolute discretion.</p>
<p>In relation to SAAS Video, you may stream the videos online through your Web browser and tablets or any other devices which are compatible with the Service. You may stream up to one video at a time. You may stream the same video to no more than one device at a time. You may watch and re-watch your streaming videos as often as you want and as long as you want (subject to the limitations described in this Agreement).</p>
<p>When you stream the Digital Content, the resolution and quality of the Digital Content you receive will depend on a number of factors, including the type of compatible device on which you are streaming the Digital Content and your bandwidth, which may go up and down over the course of your viewing. If we detect that your streaming to the Digital Content may be interrupted or otherwise not play properly due to bandwidth constraints or other factors, we may decrease the resolution and file size of the Digital Content we stream to you in an effort to provide an uninterrupted viewing experience. While we strive to provide you with a high quality viewing experience, we make no guarantee as to the resolution or quality of the Digital Content you will receive when streaming.</p>
<p>When you download the Digital Content, you are responsible for all risks or loss of Digital Content after download. As you may not be able to subsequently download certain previously-acquired SAAS Non-Video Content, once you have downloaded an item of SAAS Non-Video Content, you may also want to back up such content for your own benefit. If you are unable to complete a download, please contact our customer service.</p>
<h3>4. Intellectual Property</h3>
<p>You acknowledge and agree that the Service including but not limited to the Digital Contents or any other content provided within or via the Service, contain proprietary information and material and are protected by the applicable intellectual property and other laws, including but not limited to copyright. You agree that you will not use such proprietary information and material in any way whatsoever except for use of the Service in compliance with this Agreement. You agree not to modify rent, lease, loan, sell, distribute or create derivative works in a whole or in part based on the Service in any manner. You agree not to copy, publish, reproduce or broadcast the Service in any form by any means, except as expressly permitted in this Agreement.</p>
<p>You agree that you have been suitably notified of any trade mark, trade dress, service mark, copyright, patent or any other intellectual property rights or property rights of any nature applicable to the Service and any violation by you of any such property rights is fairly deemed to be wilful in nature.</p>
<p><a name="_GoBack"></a> We ask others to respect our intellectual property rights, and we respect the intellectual property rights of others. If you believe that material linked to by us violates your copyright, you are encouraged to notify us in accordance with our copyright infringement policy.</p>
<p>If a user infringes or repeatedly infringes any of our intellectual property rights or others, we may, in our sole discretion, terminate or deny access to and use of the Service. In this case, we will have no obligation to provide a refund of any amounts previously paid to us.</p>
<h3>5. Provision of the Service</h3>
<p>We use third party vendors and hosting partners to provide hardware, software, networking storage and related technology needed to provide our Service to you.</p>
<p>You understand that the technical processing and transmission of the Service, including the Digital Content, may be transferred unencrypted and involve transmissions over various networks and/or modifications of the content needed to conform and adapt the content to technical requirements of connecting networks or devices.</p>
<p>You are responsible for taking precautions as necessary to protect yourself and your computer systems from viruses, worms, Trojan horses, and other harmful or destructive content.</p>
</div>
@endsection
