@extends('layouts.frontend')
@section('content-css')
<style>
.course-title{
  text-align: center;
}
.list-group-item{
  border-radius: 0px !important;
  padding-top: 2px !important;
  padding-bottom: 0 !important;
  border-left: 0px;
  border-right: 0px;
}
.list-group-item:first-child{
  border-top: 0px;
}
.list-group-item > h5 > i {
  margin-right: 10px;
  color: #06af8f;
  font-size: 1.3em;
}
.course-title > h2 {
      color: #06af8f;
}
.chapter-list{
  padding-left: 3vw;
  padding-right: 3vw;
  text-align: justify;
}
.chapter-list> h3 {
  color: #fc4242cc;
}
.breadcrumb-customs{
  margin-top: 50px;
  margin-bottom: 50px;
}
.chapter-sections{
  padding-left: 2vw;
}
</style>
@endsection
@section('content')
<div class="content-margin-top">
  <div class="container-fluid">
    <ul class="breadcrumb-customs">
      <li><a href="{{ url('/') }}">Home</a></li> >
      <li><a href="{{ url('/subjects') }}">Subjects</a></li> >
      <li><a href="{{ URL::route('detail_subject', $course->subject->id) }}">{{$course->subject->name}}</a></li> >
      <li><a class="active">{{$course->name}}</a></li>
    </ul>
    <div class="container">
      <div class="panel panel-default">
        <div class="panel-body">
          @if(Auth::user()->role == 'member')
            @if(!$course->is_joined)
            <form action = "{{ url('/course/join/'.$course->id) }}" method = "post">
                                      {{ csrf_field() }}
                <input type="submit" class="btn btn-success pull-right" name="upvote" value="Join this course" />
            </form>
            @else
            <form action = "{{ url('/course/leave/'.$course->id) }}" method = "post">
                                      {{ csrf_field() }}
                <input type="submit" class="btn btn-danger pull-right" name="upvote" value="Leave this course" />
            </form>
            @endif
          @endif

          <div class="col-md-12">
            <div class="course-title">
              <h2>{{$course->name}}</h2>
              <p>
                {{$course->description}}
              </p>
            </div>
            @if(count($course->chapters)>0)
            @foreach($course->chapters as $key=>$chapter)
              <div class="col-md-12 chapter-list">


                  <h3>{{$chapter->name}}</h3>
                  <p>
                    {{$chapter->description}}
                  </p>
                  <div class="chapter-sections">
                    <div class="list-group">
                      @foreach($chapter->sections as $key => $section)
                          <a
                            @if(!$course->is_joined && Auth::user()->role == 'member')
                              data-toggle="modal" data-target="#modaljoin" href="#"
                            @else
                              href="{{url('/section/'.$section->id)}}"
                            @endif
                            class="list-group-item"><h5><i class="fa fa-play-circle-o"></i> {{$section->name}}</h5></a>
                      @endforeach
                    </div>
                  </div>

              </div>
              @endforeach
              @else
              <br>
                <div class="notfound alert alert-warning">
                  <h4>Chapters not available</h4>
                  <p>Chapters of this course is not available yet</p>
                </div>
              @endif
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- Modal -->
<div class="modal fade" id="modaljoin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action = "{{ url('/course/join/'.$course->id) }}" method = "post">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Join Kelas</h4>
      </div>
      <div class="modal-body">
        You have to join this course to see the content
      </div>
      <div class="modal-footer clearfix">
                  {{ csrf_field() }}
          <input type="submit" class="btn btn-default pull-right" name="upvote" value="Join this course" />

      </div>
      </form>
    </div>
  </div>
</div>




@endsection
