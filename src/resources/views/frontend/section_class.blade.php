@extends('layouts.frontend')

@section('content-css')
<style media="screen">
	.feedback-admin{
		background-color : rgba(6, 175, 143, 0.84);
		font-style: italic;
	}
	.not-found{
		min-height: 70vh;
		color: #aaaaaa;
	}
	.jumbotron{
		background-color: inherit;
	}
</style>
@endsection

@section('content')
<div class="content-margin-top" style="margin-bottom:40px">
	<div class="container">
		<br><br>
		<ul class="breadcrumb-customs">
			<li><a href="{{ url('/') }}">Home</a></li> >
			<li><a href="{{ url('/subjects') }}">Mata Pelajaran</a></li> >
			<li><a href="{{ url('/subject/'.$section->chapter->course->subject->id) }}">{{$section->chapter->course->subject->name}}</a></li> >
			<li><a href="{{ url('/course/'.$section->chapter->course->id) }}">{{$section->chapter->course->name}}</a></li> >
			<li><a href="{{ url('/course/'.$section->chapter->course->id) }}">{{$section->chapter->name}}</a></li> >
			<li><a class="active">{{ $section->name }}</a></li>
		</ul>
	</div>
	@include('layouts._flash')
	<!--container-->
	<div class="container">
		<div class="row">
{{-- 				<div class="col-sm-2">
					<ul class="video-action text-right">
						<li><a href="#"><span class="glyphicon glyphicon-heart"></span></a></li>
						<li><a href="#"><span class="fa fa-share-alt"></span></a></li>
						<li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
					</ul>
				</div> --}}

				@if(count($section->section_contents) > 0 )
				<div class="col-sm-10 col-sm-offset-1 video-section">
					<h3>{{ $section->name }}</h3>
					<select id="courserate">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select>
					<hr>
					<blockquote>{{ $section->description}}</blockquote>
					<hr>
					{{-- video html content --}}
					@foreach($section->section_contents as $content)
					@if($content->content->category == 'Html/Video')
					<h4>{{ $content->name}}</h4>
					<p>{{ $content->description }}</p>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="{{ url(asset('videos/'.$content->content->url.$content->content->file)) }}" allowfullscreen></iframe>		
					</div>
					<div class="clearfix"></div>
					@endif
					@endforeach
					{{-- video content --}}
					@foreach($section->section_contents as $content)
					@if($content->content->category == 'Video')
					<hr>
					<h4>{{ $content->name}}</h4>
					<p>{{ $content->description }}</p>
					<video width="100%" height="100%" controls>
						<source src="{{ route('getVideo', $content->content->id)  }}" type="video/mp4">
							Your browser not supported this video format.
						</video>
						@endif
						@endforeach
						<div class="clearfix"></div>
						{{-- pdf content --}}
						@foreach($section->section_contents as $content)
						@if($content->content->category == 'PDF')
						<hr>
						<div class="row">
							<div class="col-sm-8">
								<h4>{{ $content->name}}</h4>
								<p>{{ $content->description }}</p>
							</div>
							<div class="col-sm-4 text-right">
								<a class="btn btn-custom btn-lg" target="_blank" href="{{ url('/download/get/'.$content->content->id)}}">
									DOWNLOAD MATERIAL
								</a>
							</div>
						</div>
						<div class="clearfix"></div>
						@endif
						@endforeach
						{{-- quiz content --}}
						@foreach($section->section_contents as $content)
						@if($content->content->category == 'Quiz')
						<hr>
						<div class="row">
							<div class="col-sm-8">
								<h4>{{ $content->name}}</h4>
								<p>{{ $content->description }}</p>
							</div>
							<div class="col-sm-4 text-right">
								<a class="btn btn-warning btn-lg" role="button" data-toggle="collapse" href="#collapse{{$content->id}}" aria-expanded="false" aria-controls="collapse{{$content->id}}">
									TAKE QUIZ
								</a>
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="collapse" id="collapse{{$content->id}}">
							<div class="well">
								<iframe src="{{ url(asset('quiz/'.$content->content->url.$content->content->file)) }}" style="width:100%; min-height:600px;" frameborder="0" allowfullscreen style="border: 1px solid #DDD;"></iframe>
							</div>
						</div>
						@endif
						@endforeach
						<hr>
						<div class="row">
							<div class="col-md-2"><h4>Rate this course</h4>

							</div>
							<div class="col-md-4 userrate">
								<h4>
									<select id="courseratebyuser">
										<option value=""></option>
									  <option value="1">1</option>
									  <option value="2">2</option>
									  <option value="3">3</option>
									  <option value="4">4</option>
									  <option value="5">5</option>
									</select>
								</h4>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-2"><h4>Viewer</h4>

							</div>
							<div class="col-md-4 userrate">
								<h4><i class="fa fa-eye" aria-hidden="true"></i> {{$section->view_count}}</h4>

							</div>
						</div>
					</div>
					<!--end video section-->
					<div class="col-sm-10 col-sm-offset-1 feedback">
						<br>
						<br>
						<h3><i class="fa fa-comments"></i> Feedbacks</h3>
						<hr>
						{!!
							Form::open([
								'role' => 'form',
								'url' => action('FeedbackController@store', $section->id),
								'method' => 'post'
								])
								!!}
								<textarea class="form-control" id="comment" rows=3 name="comment" placeholder="add feedback" required></textarea>
								<input type="hidden" name="section_id" value="{{$section->id}}">
								<div class="text-right" id="buttonComment" style="padding: 20px 0; display: none;">
									<button type="button" id="hide" class="btn btn-default">Cancel</button>
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
								{!! Form::close() !!}
								<hr>
								@foreach ($section->feedbacks as $feedback)
								@if($feedback->status == 'published' AND $feedback->parent_id == 0)
								<div class="media">
									<div class="media-left">
										<div class="comment-avatar">
											<i class="fa fa-user"></i>
										</div>
									</div>
									<div class="media-body">
										<h4 class="media-heading ">{{ $feedback->user->name}}<br>
											<small>{{ $feedback->created_at }}</small></h4>
										{{ $feedback->comment }}
										<hr>
										@foreach($feedback->replies as $reply)
										<div class="media">
											<div class="media-left">
												<div class="comment-avatar feedback-admin">
													<i class="fa fa-user"></i>
												</div>
											</div>
											<div class="media-body">
												<h4 class="media-heading">{{ $reply->user->name}}<br>
													<small>{{ $reply->created_at }}</small>
												</h4>
												<p>

													{{ $reply->comment }}
												</p>
												<hr>
											</div>
										</div>
										@endforeach
									</div>
								</div>
								@endif
								@endforeach
							</div>
							@else
								<div class="col-md-12 not-found">
									<br>
										<br>
											<br>
									  <h1 class="text-center">Opps.. Content not available yet</h1>
								</div>
							@endif
						</div>
						<!--end row-->
					</div>
					<!--end container-->

		{{--
		<div class="container">
			<div class="row">
			<div class="col-xs-6">
				<a href="#">
					<p>Sebelumnya</p>
				</a>
			</div>
			<div class="col-xs-6">

			</div>
				<div class="col-sm-12 col-xs-12 recommended-section">
					<span>Recommended Section</span>
					<div class="section-control">
						<a href="#theCarousel" data-slide="prev">Prev</a>
						<span>|</span>
						<a href="#theCarousel" data-slide="next">Next</a>
					</div>
				</div>
				<div class="carousel slide multi-item-carousel" id="theCarousel">
			        <div class="carousel-inner">
			          	<div class="item active">
			            	<div class="col-sm-4 col-xs-6 text-center">
			            		<img src="/images/course1.png" class="img-recommend-course">
			            	</div>
			          	</div>
			          	<div class="item">
			            	<div class="col-sm-4 col-xs-6 text-center">
			            		<img src="/images/course2.png" class="img-recommend-course">
			            	</div>
			          	</div>
			          	<div class="item">
			            	<div class="col-sm-4 col-xs-6 text-center">
			            		<img src="/images/course3.png" class="img-recommend-course">
			            	</div>
			          	</div>
			          	<div class="item">
			            	<div class="col-sm-4 col-xs-6 text-center">
			            		<img src="/images/course4.png" class="img-recommend-course">
			            	</div>
			          	</div>
			        </div>
			        <!-- <a class="left carousel-control carousel-control-customs" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-menu-left"></i></a>
	        		<a class="right carousel-control carousel-control-customs" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-menu-right"></i></a> -->
			    </div>
			</div>
		</div> --}}
	</div>

<!--modal-->
	<div class="modal fade" id="modalRateOk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header clearfix">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        Thanks For Your Rate
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	@endsection
	@section('content-js')
	<script type="text/javascript">
		$(document).ready(function(){
			$('#comment').focus(function(){
				$('#buttonComment').show('slow');
			});
			$('#hide').click(function(){
				$('#comment').val('');
				$('#buttonComment').hide('slow');
			});
		});
		$(function() {
		 $('#courserate').barrating('show',
		 {
			 theme: 'fontawesome-stars-o',
			 readonly: true,
			 initialRating: {{$section->rates}}
		 });
	});
	$(function() {
	 $('#courseratebyuser').barrating('show',
	 {
		 theme: 'fontawesome-stars',
		 initialRating: {{$section->user_rate}},
		 onSelect: function(v,t,a){
			 console.log(v);
			 $.ajax
				({
						type: "PUT",
						//the url where you want to sent the userName and password to
						url: "{{ URL::route('setrate') }}",
						dataType: 'json',
						async: false,
						//json object to sent to the authentication url
						data: JSON.stringify({ "section_id": {{$section->id}} , "value":v }),
						success: function (o) {
							 $('#modalRateOk').modal('show');
							 $('#courserate').barrating('set', o.rate);
						}
				})
		 }
	 });
	});


	</script>
	@endsection
