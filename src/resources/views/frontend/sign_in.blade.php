@extends('layouts.frontend')

@section('content')
<div class="col-sm-12 col-xs-12 sign-in-header">
	<div class="container">
		<div class="col-sm-8 col-sm-offset-2">
			<br><br><br>
			<ul class="nav nav-tabs nav-justified tab-sign-in" role="tablist">
				<li role="presentation"  class="active"><a href="#sign_up" aria-controls="sign_up" role="tab" data-toggle="tab">MASUK</a></li>
				<li role="presentation"><a href="#sign_in" aria-controls="sign_in" role="tab" data-toggle="tab">DAFTAR</a></li>
			</ul>
			<div class="tab-content tab-content-sign-in">
				<div role="tabpanel" class="tab-pane fade in active" id="sign_up">
					<div class="sign-in-content">
						<h3>Masuk ke akun anda</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore</p>
							<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
								{{ csrf_field() }}
								<div class="form-group">
									<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Alamat email">
									@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
									@endif
								</div>
								<div class="form-group">
									<input id="password" type="password" class="form-control" name="password" required placeholder="Password">
									@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
									@endif
								</div>
								{{-- <div class="form-group">
									<div class="col-md-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="remember"> Ingat saya
											</label>
										</div>
									</div>
								</div> --}}
								<div class="form-group">
									<button type="submit" class="btn btn-default btn-sign-in">Masuk</button>
								</div>
							</form>
							<hr>
							<h5>atau masuk dengan cara akun berikut</h5>
							<a href="redirect" class="btn btn-default btn-other-sign" style="margin-right: 20px;"><img src="/images/fb-color.png"><span>Facebook</span></a>
							<a href="googleauth" class="btn btn-default btn-other-sign"><img src="/images/google.png"><span>Google</span></a>
							<hr>
							<p><a href="">Lupa password?</a></p>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="sign_in">
						<div class="sign-in-content">
							<h3>Log Into Your Account</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore</p>
								<form method="post" action="">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-10">
											<input type="email" class="form-control" id="inputEmail3" placeholder="Email">
										</div>
									</div>
									<div class="form-group">
										<input type="email" name="email" class="form-control" placeholder="Email Address">
									</div>
									<div class="form-group">
										<input type="password" name="password" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-default btn-sign-in">Sign In</button>
									</div>
								</form>
								<hr>
								<h5>or sign up with one of this services</h5>
								<a href="#" class="btn btn-default btn-other-sign" style="margin-right: 20px;"><img src="/images/fb-color.png"><span>Facebook</span></a>
								<a href="#" class="btn btn-default btn-other-sign"><img src="/images/google.png"><span>Google</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="strip-2"></div>
		@endsection
