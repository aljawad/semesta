<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<title>SAAS</title>

		<link rel="stylesheet" href="{{ asset(elixir("css/app.css")) }}">
		<link rel="stylesheet" href="{{ asset(elixir("css/custom.css")) }}">
		<noscript><meta http-equiv="refresh" content="1;url={{ action('HomeController@nojs') }}"></noscript>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		
		<style type="text/css">
			.datepicker{
				z-index: 9999;
			}
		</style>

		<script>
			var base_url = "{!! url('/') !!}";
		</script>

	</head>
	<body>
		{{-- <div class="container-fluid" style=""> --}}
			@include('navbar')

			<div class="page-wrapper">
				@if (isset(View::getSections()['sidebar']))
				<div class="sidebar-wrapper">
					<div class="sidebar">
						<!-- TODO: REMAKE THIS MENU TO UL -->
						<div class="list-group">
							@yield('sidebar')
						</div>
					</div>
				</div>
				<div class="content-wrapper">
					<div class="content">
						@include('top_notif')
						@include('layouts._flash')

						@yield('content')
					</div>
				</div>
				@else
				<div class="content-wrapper content-wrapper-no-sidebar">
					<div class="content">
						@include('top_notif')

						@yield('content')
					</div>
				</div>
				@endif

			</div>
			
		{{-- </div>end of container-fluid --}}

			<footer>
				<!-- <div class="copyright">
					Semesta by Durenworks &copy;
				</div> -->
			</footer>

			@yield('content-modal')
			
		

		<script src="{{ asset(elixir("js/app.js")) }}"></script>
		<script type="text/javascript">
			var token = "{!! csrf_token() !!}";
		</script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

		<script>
		     $('.btn-show').on('click', function(){

		        var id = $(this).data('id');
		        $('#data_id').val(id);
		        var name = $(this).data('name');
		        $('#name').val(name);
		        var description = $(this).data('description');
		        $('#description').val(description);
		        var href = $(this).data('href');
		        $('#update').attr('action', href);

		        console.log('as');
		    });
		</script>
		
		@yield('content-js')
	</body>
</html>