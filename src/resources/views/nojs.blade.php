<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Transcalc</title>

    <link rel="stylesheet" href="{{ asset(elixir("css/app.css")) }}">
    <noscript><meta http-equiv="refresh" content="1;url={{ action('HomeController@nojs') }}"></noscript>
  </head>
  <body>
    <div class="container" style="">
      <div class="row" style="margin-top: 20px;">
        <div class="alert alert-danger" role="alert">
          <strong>Error!</strong> It seems your browser javascript is disable.
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-body">
          <p>How to enable javascript on Mozila : <a href="http://www.wikihow.com/Turn-on-Javascript-in-Mozilla-Firefox" target="_blank">www.wikihow.com</a></p>
          <p>How to enable javascript on Chrome : <a href="https://support.google.com/adsense/answer/12654?hl=en" target="_blank">support.google.com</a></p>
          <p>How to enable javascript on Opera : <a href="http://www.opera.com/docs/browserjs/" target="_blank">www.opera.com</a></p>
        </div>
      </div>   
    </div><!-- end of container-fluid -->

    <script src="{{ asset(elixir("js/app.js")) }}"></script>
  </body>
</html>