@extends('master', ['active' => 'course'])
@section('sidebar')
    @include('course.sidebar', ['active' => 'add'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Tambah kelas baru</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/course') }}">Kembali</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('CourseController@store'),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Nama',
                'placeholder' => 'Nama',
                'default' => ''
            ])

            @include('form.select', [
                'field' => 'teacher_id',
                'label' => 'Guru',
                'options' => 
                    [''=>'']+App\User::pluck('name','id')->all(), null,              
                'default' => ''
            ])

            @else


            @include('form.picklist', [
                'field' => 'subject_id',
                'label' => 'Mapel',
                'placeholder' => 'Mapel',
                'name'  => 'subject',
            ])

            @include('form.picklist', [
                'field' => 'ladder_id',
                'label' => 'Tingkatan',
                'placeholder' => 'Tingkatan',
                'name'  => 'ladder',
            ])

            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Deskripsi',
                'placeholder' => 'Deskripsi',
                'default' => ''
            ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">simpan</button>
            </div>

            {!! Form::close() !!}


@endsection

@section('content-modal')   
    @include('subject.modal_picklist', [
        'name' => 'subject',
        'title' => 'Daftar Mapel',
        'placeholder' => 'Cari berdasarkan nama',
    ])

    @include('ladder.modal_picklist', [
        'name' => 'ladder',
        'title' => 'Daftar tingkatan',
        'placeholder' => 'Cari berdasarkan nama',
    ])
@endsection

@section('content-js')
    <script>
    $(function () {
        CreatePicklist('subject', '/subject/list?');
        CreatePicklist('ladder', '/ladder/list?');
    });
    </script>
@endsection