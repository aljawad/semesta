@extends('master', ['active' => 'course'])
@section('sidebar')
    @include('course.sidebar', ['active' => 'add'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Tambah Kelas</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/course') }}">Kembali</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('CourseController@store'),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Nama',
                'placeholder' => 'Nama',
                'default' => ''
            ])

            
            @include('form.select', [
                'field' => 'subject_id',
                'label' => 'Mapel',
                'options' => 
                    [''=>'Pilih Mapel']+App\Subject::where('status', '=', 'active')->pluck('name','id')->all(), null,              
                'default' => '',
                'placeholder' => 'Select Subject',
            ])

            @include('form.select', [
                'field' => 'ladder_id',
                'label' => 'Tingkatan',
                'options' => 
                    [''=>'Pilih Tingkatan']+App\Ladder::where('status', '=', 'active')->pluck('name','id')->all(), null,              
                'default' => '',
                'placeholder' => 'Select Ladder'
            ])

            @if (Auth::user()->role=='admin')

            @include('form.select', [
                'field' => 'teacher_id',
                'label' => 'Guru',
                'options' => 
                    [''=>'Pilih Guru']+App\User::where('role', '=', 'teacher')->pluck('name','id')->all(), null,              
                'default' => '',
                'placeholder' => 'Select teacher'
            ])

            @else

            <input class="form-control" type="hidden" name="teacher_id" value={{ Auth::user()->id }}>
            
            @endif

            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Deskripsi',
                'placeholder' => 'Deskripsi',
                'default' => ''
            ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>

            {!! Form::close() !!}


@endsection

@section('content-modal')   
    @include('subject.modal_picklist', [
        'name' => 'subject',
        'title' => 'Daftar Mapel',
        'placeholder' => 'Cari berdasarkan nama',
    ])

    @include('ladder.modal_picklist', [
        'name' => 'ladder',
        'title' => 'Daftar Tingkatan',
        'placeholder' => 'Cari berdasarkan nama',
    ])
@endsection

@section('content-js')
    <script>
    $(function () {
        CreatePicklist('subject', '/subject/list?');
        CreatePicklist('ladder', '/ladder/list?');
    });
    </script>
@endsection