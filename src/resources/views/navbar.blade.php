 <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/admin/home">SAAS</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        @if (Auth::guest())
        <li></li>
        @else
        @role(['admin', 'sekolah'])
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Master Data <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li class="{{ isset($active) && $active == 'ladder' ? 'active' : '' }}">
              <a href="/admin/ladder">Tingkatan</a>
            </li>
            <li class="{{ isset($active) && $active == 'subject' ? 'active' : '' }}">
              <a href="/admin/subject">Mata Pelajaran</a>
            </li>
            <li class="{{ isset($active) && $active == 'teacher' ? 'active' : '' }}">
              <a href="/admin/teacher">Guru</a>
            </li>
            <!-- <li class="{{ isset($active) && $active == 'sekolah' ? 'active' : '' }}">
              <a href="/admin/sekolah">Sekolah</a>
            </li> -->
          </ul>
        </li>
        @endrole
        <li class="{{ isset($active) && $active == 'course' ? 'active' : '' }}">
          <a href="/admin/course">Kelas</a>
        </li>

        @role('admin')

        <li class="{{ isset($active) && $active == 'feedback' ? 'active' : '' }}">
          <a href="/admin/feedback">Komentar</a>
        </li>
        
        <li class="{{ isset($active) && $active == 'member' ? 'active' : '' }}">
          <a href="/admin/member">Member</a>
        </li>
        @endrole

        <li class="{{ isset($active) && $active == 'repo' ? 'active' : '' }}">
          <a href="/admin/repo">Repository</a>
        </li>
<!-- 
        <li class="{{ isset($active) && $active == 'student' ? 'active' : '' }}">
          <a href="#">My Students</a>
        </li> -->

        @role('admin')
        <!-- <li class="{{ isset($active) && $active == 'message' ? 'active' : '' }}">
          <a href="/admin/message">Messages</a>
        </li> -->

        <li class="{{ isset($active) && $active == 'config' ? 'active' : '' }}">
          <a href="/admin/setting/featured">Setting</a>
        </li>
        @endrole
        
        @endif
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::guest())
        <li><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
        @else
        <li><a target="_blank" href="{{ url('/') }}">View Site</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
          <!-- @role('admin')
            <li><a href="/user-manual/User-Manual-Admin.pdf" target="_blank"><i class="fa fa-btn fa-lock"></i> Download User Manual</a></li>
          @endrole
          @role('teacher')
            <li><a href="/user-manual/User-Manual-Teacher.pdf" target="_blank"><i class="fa fa-btn fa-lock"></i> Download User Manual</a></li>
          @endrole -->
            <li><a href="{{ url('/admin/password') }}"><i class="fa fa-btn fa-lock"></i> Change Password</a></li>
            <li>
            <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout</a>

                      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                     </form>
            </li>
          </ul>
        </li>
        @endif
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>

