@extends('master', ['active' => 'feedback'])
@section('sidebar')
@include('feedback.sidebar', ['active' => 'feedback'])
@endsection
@section('content')

<div class="container-fluid">
    <div class="col-xs-12">
    <div class="row" style="margin-left: -30px">
        {!!
                Form::open(array(
                    'class' => 'form-signin',
                    'role' => 'form',
                    'url' => action('FeedbackController@index'),
                    'method' => 'get',
                ))
            !!}
            <!-- <div class="col-md-2">
                @include('form.option', [
                'field' => 'subject',
                'label' => 'Subject',
                'options' => [''=>'All Subject']+App\Subject::pluck('name','id')->all(), null,
                'default' => (request()->input('subject') ? request()->input('subject') : 'ALL'),
                ])
            </div>

            <div class="col-md-2">
                @include('form.option', [
                'field' => 'course',
                'label' => 'Course',
                'options' => [''=>'All Course']+App\Course::pluck('name','id')->all(), null,
                'default' => (request()->input('course') ? request()->input('subject') : 'ALL'),
                ])
            </div>
            -->

            <!-- <div class="col-md-2">
                @include('form.option', [
                'field' => 'chapter',
                'label' => 'Chapter',
                'options' => [''=>'All Chapter']+App\Chapter::pluck('name','id')->all(), null,
                'default' => (request()->input('chapter') ? request()->input('chapter') : 'ALL'),
                ])
            </div> --> 

            <div class="col-md-2">
                @include('form.option', [
                'field' => 'section',
                'label' => 'Sub Bab',
                'options' => [''=>'Semua']+App\Section::pluck('name','id')->all(), null,
                'default' => (request()->input('section') ? request()->input('section') : 'ALL'),
                ])
            </div>

            <div class="col-md-2">
                @include('form.option', [
                'field' => 'status',
                'label' => 'Status',
                'options' => [
                ''=>'Semua Status',
                'published'=>'publish',
                'pending' => 'tidak publish'
                ], null,
                'default' => (request()->input('status') ? request()->input('status') : 'ALL'),
                ])
            </div>

            <div class="col-md-2" style="margin-top : 25px">
                <button type="submit" class="btn btn-primary">Cari</button>
                {!! Form::close() !!}
            </div>
    </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">

                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 200px;">Nama</th>
                            <th>Mapel</th>
                            <th style="width: 150px;">Kelas</th>
                            <th>Bab</th>
                            <th>Sub Bab</th>
                            <th>Komentar</th>
                            <th>Status</th>
                            <th style="width: 200px;" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($feedbacks) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif

                        @foreach ($feedbacks as  $key => $feedback)
                        <?php $section = $feedback->section->id;?>
                        <tr>

                        @if(count($feedback->section->chapter->course) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                            <td>{{ ++$key }}</td>
                            <td>{{$feedback->user->name}}</td>
                            <td>{{$feedback->section->chapter->course->subject->name}}</td>
                            <td>{{$feedback->section->chapter->course->name}}</td>
                            <td>{{$feedback->section->chapter->name}}</td>
                            <td>{{$feedback->section->name}} </td>
                            <td>{{$feedback->comment}}</td>
                            <td>{{$feedback->status}}</td>

                            <td class="text-right">
                                <a class="btn btn-success btn-xs" href="{{ action('FeedbackController@view', $feedback->id) }}">Lihat</a>
                                @if($feedback->status == 'pending')
                                <a class="btn btn-primary btn-xs" href="{{ action('FeedbackController@approve', $feedback->id) }}" onclick="return confirm('Are you sure you want to approve this comment?')">publikasi</a>
                                @elseif($feedback->status == 'published')
                                <a class="btn btn-warning btn-xs" href="{{ action('FeedbackController@unapprove', $feedback->id) }}" onclick="return confirm('Are you sure you want to unapprove this comment?')">tidak dipublikasi</a>
                                @endif
                                <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                <a class="btn btn-xs btn-danger" href="{{ action('FeedbackController@delete', $feedback->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">hapus</a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                
            </div>
            <div class="pull-right">
                {!! $feedbacks->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection

