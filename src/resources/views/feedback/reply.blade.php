@extends('master', ['active' => 'feedback'])
@section('sidebar')
@include('feedback.sidebar', ['active' => 'feedback'])
@endsection
@section('content')
<h3><i class="glyphicon glyphicon-user"></i> {{ $feedbacks->user->name}}</h3>
<small>{{ $feedbacks->created_at }}</small>
<hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Mata Pelajaran</p>
                    <p class="medium-title">{{$feedbacks->section->chapter->course->subject->name}}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Kelas</p>
                    <p class="medium-title">{{$feedbacks->section->chapter->course->name}}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Sub Bab</p>
                    <p class="medium-title">{{$feedbacks->section->name}}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <p>Komentar</p>
            <blockquote class="blockquote-custom">
                {{$feedbacks->comment}}
            </blockquote>
            
        </div>
    </div>
<hr>
<h3>Replies</h3>

{!! Form::open(['role' => 'form','id' =>'edit' ,'url' => action('FeedbackController@reply', [$feedbacks->id]),'method' => 'post']) !!}
<textarea class="form-control" id="comment" rows=3 name="comment" placeholder="Balas"></textarea>
<div class="text-right" id="buttonComment" style="padding: 20px 0; display: none;">
    <button type="button" id="hide" class="btn btn-default">Keluar</button>
    <button type="submit" class="btn btn-primary">Simpan</button>
</div>
{!! Form::close() !!}
<br>
<div class="media">
    @foreach($reply as $reply)
    <div class="media-body">
        <h4 class="media-heading">{{ $reply->user->name}}</h4>
        <small>{{ $reply->created_at }}</small>
        <a class="btn btn-primary btn-xs edit-reply" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{$reply->id}}" data-comment="{{$reply->comment}}"  data-target="#as" data-href="{{action('FeedbackController@updateReply', [$reply->id])}}">ubah</a>
        <a class="btn btn-danger btn-xs" href="{{action('FeedbackController@deleteReply', $reply->id)}}" onclick="return confirm('Are you sure you want to move this item to trash?')">hapus</a>
        <br>
        {{ $reply->comment }}
        
        <hr>
        @endforeach
    </div>
</div>
@endsection

@section('content-js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#comment').focus(function(){
            $('#buttonComment').show('slow');
        });
        $('#hide').click(function(){
            $('#comment').val('');
            $('#buttonComment').hide('slow');
        });
    });

    $('.edit-reply').on('click', function(){

       $('#buttonComment').show('slow')

        var id = $(this).data('id');
        $('#data_id').val(id);
        var comment = $(this).data('comment');
        $('#comment').val(comment);
        var href = $(this).data('href');
        $('#edit').attr('action', href);


    });

</script>
@endsection