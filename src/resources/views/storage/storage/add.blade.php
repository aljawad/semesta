@extends('master', ['active' => 'storage'])

@section('sidebar')
	@include('storage.storage.sidebar', ['active' => 'create'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Create Storage
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Storage\StorageController@store'),
					'method' => 'post'
				])
			!!}

			@include('form.text', [
				'field' => 'name',
				'label' => 'Storage Name',
				'placeholder' => 'Storage Name',
				'default' => ''
			])

			@include('form.select', [
				'field' => 'transfer_mode',
				'label' => 'Type',
				'options' => [
					'sme' => 'Storage',
					'ltl' => 'Store',
				]
			])

			@include('form.select', [
				'field' => 'is_active',
				'label' => 'Status',
				'options' => [
					'1' => 'Active',
					'0' => 'Inactive',
				]
			])

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}				
		</div>
	</div>
@endsection

@section('content-js')
@endsection