@extends('master', ['active' => 'storage'])

@section('sidebar')
	@include('storage.storage.sidebar', ['active' => 'index'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-banner">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Transfer Mode</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($storages) == 0)
						<tr>
							<td colspan="5">There is no storage.</td>
						</tr>
						@endif
						@foreach ($storages as $key => $storage)
							<tr>
								<td>{{ ++$key }}</td>
								<td>{{ $storage->name }}</td>
								<td>@if($storage->transfer_mode == 'sme') Storage @else Store @endif</td>
								<td>@if($storage->is_active == true) Active @else Inactive @endif</td>
								<td>
									<a href="{{ action('Storage\StorageController@edit', [$storage->id]) }}" class="btn btn-primary btn-xs">
										Edit
									</a>
									<a href="{{ action('Storage\StorageController@delete', [$storage->id]) }}" class="btn btn-danger btn-xs" onclick="confirm('Are you sure want to delete this data?')">
										Delete
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				{!! $storages->render() !!}
			</div>
		</div>
	</div>
@endsection