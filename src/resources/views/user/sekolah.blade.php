@extends('master', ['active' => 'sekolah'])
@section('sidebar')
 	@include('user.sidebarsekolah', ['active' => 'sekolah'])
@endsection
@section('content')

<div class="container-fluid">
    <div class="col-xs-12">
    <div class="row" style="margin-left: -30px">
        {!!
                Form::open(array(
                    'class' => 'form-signin',
                    'role' => 'form',
                    'url' => action('UserController@sekolah'),
                    'method' => 'get',
                ))
            !!}
            
            <div class="col-md-2">
                @include('form.option', [
                'field' => 'suspended',
                'label' => 'Status',
                'options' => ['ALL' => 'All', true => 'Active', 'false' => 'Inactive'],
                'default' => (request()->input('suspended') ? request()->input('suspended') : 'ALL')
                ])
            </div>
            <div class="col-md-4">
                @include('form.text', [
                    'label' => 'Find by',
                    'field' => 'search',
                    'placeholder' => "Search with name, email, phone",
                    'default' => (request()->input('search') ? request()->input('search') : '')
                ])
            </div>
            <div class="col-md-2" style="margin-top : 25px">
                <button type="submit" class="btn btn-primary">Search</button>
                {!! Form::close() !!}
            </div>
    </div>
    </div>
   
        <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($users) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($users as $key => $user)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if($user->suspended==true)
                                    <a class="btn-success btn-xs" disabled>Active</a>
                                    
                                    @else
                                    <a class="btn-danger btn-xs" disabled>Inactive</a>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-primary btn-xs" href="{{ action('UserController@editsekolah', $user->id) }}">edit</a>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    <a class="btn btn-xs btn-danger" href="{{ action('UserController@delete', $user->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">delete</a>

                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="pull-right">
                {!! $users->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection
