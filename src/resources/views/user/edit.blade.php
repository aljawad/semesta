@extends('master', ['active' => 'user'])
@section('sidebar')
    @include('user.sidebar', ['active' => 'user'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Ubah Guru</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/admin/teacher') }}">Kembali</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('UserController@update',[$user->id]),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Nama',
                'placeholder' => 'Nama',
                'default' => $user->name
            ])

            @include('form.text', [
                'field' => 'email',
                'label' => 'Email',
                'placeholder' => 'Email',
                'default' => $user->email
            ])

            @include('form.option', [
                'field' => 'suspended',
                'label' => 'Status',
                'options' => [
                    0 => 'Tidak Aktif',
                    1 => 'Aktif',
                ],
                'default' => $user->suspended
            ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>

            {!! Form::close() !!}


@endsection