
<a class="list-group-item {{ isset($active) && $active == 'course' ? 'active' : '' }}" href="{{ url('/admin/course') }}">
	Daftar Kelas
</a>

<a class="list-group-item {{ isset($active) && $active == 'add' ? 'active' : '' }}" href="{{ url('/admin/course/add') }}">
	Tambah Kelas
</a>

<div class="separate-sidebar"></div>

<a class="list-group-item {{ isset($active) && $active == 'trash' ? 'active' : '' }}" href="{{ url('/admin/course/trash') }}">
	Kelas di tong sampah
</a>