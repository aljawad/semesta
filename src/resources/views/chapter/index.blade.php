@extends('master', ['active' => 'chapter'])
@section('sidebar')
@include('chapter.sidebar', ['active' => 'chapter'])
@endsection
@section('content')

<div class="container-fluid">
    <h3>Physic for dummies</h3>
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Subject</p>
                    <p class="medium-title">Physic</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Ladder</p>
                    <p class="medium-title">Primary School</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Teacher</p>
                    <p class="medium-title">Jhon Doe</p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <p>Description</p>
            <blockquote class="blockquote-custom">
                lkghsdlgosdh gsdgh sdkjgh skdgjh skdgj skgjlh skgjhs kdglh skdjgh sdkgj
            </blockquote>
        </div>
    </div>
    <h3>Chapter</h3>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <button type="button" class="btn btn-success btn-lg col-sm-12 text-center"><i class="glyphicon glyphicon-plus"></i> Add Chapter</button>
            <div class="clearfix"></div>
            <br>
            <div class="list-group">
                <?php $index=1; ?>
                @foreach ($chapters as $chapter)
                @if ($index == 1)
                <button type="button" class="list-group-item chapter active" data-id="{{ $chapter->id }}">{{ $chapter->name }}</button>
                <?php $id_awal = $chapter->id; ?>
                @else
                <button type="button" class="list-group-item chapter" data-id="{{ $chapter->id }}">{{ $chapter->name }}</button>
                @endif
                <?php $index++; ?>
                @endforeach
            </div>
        </div>
        <div class="col-md-9" style="border-left: 1px solid #DDD;">
            <div class="panel panel-default">
              <div class="panel-body">
                  <input type="hidden" class="id_chapter" name="id_chapter" value="0">
                  <div class="row">
                    <div class="col-xs-9">
                        <p>Chapter Name</p>
                        <p id="chapterTitle" class="medium-title">Energy</p>
                    </div>
                    <div class="col-xs-3 text-right">
                        <button class="btn btn-sm btn-default">Edit</button>
                        <button class="btn btn-sm btn-danger">Delete</button>
                    </div>
                </div>
                <p>Description</p>
                <blockquote class="blockquote-custom" id="chapterDescription">
                    {{-- populate from ajax --}}
                </blockquote>
            </div>
        </div>
        {{-- section --}}
        <div class="row">
            <div class="col-xs-6">
                <p style="font-size: 20px;"> Section(s)</p>
            </div>
            <div class="col-xs-6 text-right">
                <button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Add Section</button>
            </div>
        </div>
        <div id="accordion">
            <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1"><i class="glyphicon glyphicon-chevron-right"></i> Section 1
                    </a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                    <p style="color: #808080;">kjsdkgj sdkgjhs dkgls dgkjsd gksjd gskdjgh skdg sdkg sdkjg sdjkg skdjgl sdklgh sjk ksdkg sdgjkhs kdjg sdkgjh sdkjg sdkghs dkgjdsh gkjsd gskdjg skdghsidgshdigw4hsdg</p>
                    <hr>
                    <div class="row">
                       <div class="col-xs-6">
                        <p style="font-size: 20px;"> Content(s)</p>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">
                            <i class="glyphicon glyphicon-plus"></i> Add content
                        </button>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px;">NO</th>
                            <th>Content Name</th>
                            <th>Type</th>
                            <th class="text-center" width="100px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Content 1</td>
                            <td>html video</td>
                            <td class="text-center"><a href="#">remove</a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Content 2</td>
                            <td>video</td>
                            <td class="text-center"><a href="#">remove</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Content 3</td>
                            <td>pdf</td>
                            <td class="text-center"><a href="#">remove</a></td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <a href="#">edit</a> | <a href="#">delete</a> 
                </div>
            </div>
        </div>
        
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapse2"><i class="glyphicon glyphicon-chevron-right"></i> Section 2
            </a>
        </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            <p style="color: #808080;">kjsdkgj sdkgjhs dkgls dgkjsd gksjd gskdjgh skdg sdkg sdkjg sdjkg skdjgl sdklgh sjk ksdkg sdgjkhs kdjg sdkgjh sdkjg sdkghs dkgjdsh gkjsd gskdjg skdghsidgshdigw4hsdg</p>
            <hr>
            <div class="row">
               <div class="col-xs-6">
                <p style="font-size: 20px;"> Content(s)</p>
            </div>
            <div class="col-xs-6 text-right">

                <button type="button" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-plus"></i> Add content</button>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 50px;">NO</th>
                    <th>Content Name</th>
                    <th>Type</th>
                    <th class="text-center" width="100px">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Content 1</td>
                    <td>html video</td>
                    <td class="text-center"><a href="#">remove</a></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Content 2</td>
                    <td>video</td>
                    <td class="text-center"><a href="#">remove</a></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Content 3</td>
                    <td>pdf</td>
                    <td class="text-center"><a href="#">remove</a></td>
                </tr>
            </tbody>
        </table>
        <div>
            <a href="#">edit</a> | <a href="#">delete</a> 
        </div>
    </div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
    </div>
    <div class="modal-body">
        ...
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
    </div>
</div>
</div>
</div>

@endsection
@section('content-js')
<script type="text/javascript">
    $(document).ready(function(){

        init();

        $(document).on('click', '.chapter', function () {
            var id_chapter = $(this).data('id');

            //refreesh active class for button
            refreshButtonChapter(this);

            //refresh current id chapter
            setIdChapter(id_chapter)

            //populate data chapter
            populateChapter(id_chapter);
        });
    });

    function init(){
        var id_chapter = <?= $id_awal ?>

        setIdChapter(id_chapter);
        populateChapter(id_chapter);
    }

    function setIdChapter(id_chapter){
        $('.id_chapter').val(id_chapter);
    }

    function refreshDataSection(id_chapter){
        $('#chapterTitle').html(id_chapter);   
    }

    function refreshButtonChapter(now){
        $( ".list-group button" ).each(function( index ) {
          $( this ).removeClass('active');
      });
        $(now).addClass('active');
    }

    //pupulating data
    function populateChapter(id_chapter){
        $.ajax({
            type: "GET",
            url:'/populate-chapter/'+id_chapter,
            success: function (data) {
                $('#chapterTitle').html(data.name);
                $('#chapterDescription').html(data.description);
                populateSection(data.id);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    function populateSection(id_chapter){
        $.ajax({
            type: "GET",
            url:'/populate-section-list/'+id_chapter,
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });   
    }
</script>
@endsection
