@extends('master', ['active' => 'ladder'])
@section('sidebar')
 	@include('ladder.sidebar', ['active' => 'ladder'])
@endsection
@section('content')

<div class="container-fluid">
    <div class="col-xs-12"> 
    @include('form.search',['url'=>'/admin/ladder','link'=>'/admin/ladder'])
    </div>
   
        <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>Aksi</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($ladders) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($ladders as $key => $ladder)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $ladder->name }}</td>
                                <td>
                                    @if($ladder->status=='active')
                                    <a class="btn-success btn-xs" disabled>Aktif</a>
                                    
                                    @else
                                    <a class="btn-danger btn-xs" disabled>Tidak Aktif</a>
                                    @endif
                                </td>
                                <td>
                                    @if($ladder->status=='active')
                                    <a class="btn btn-xs btn-warning" href="{{ action('LadderController@inactive', $ladder->id) }}" onclick="return confirm('Apakah anda yakin untuk menonaktifkan `{{$ladder->name}}`?')">Tidak aktif</a>
                                    @else
                                    <a class="btn btn-xs btn-success" href="{{ action('LadderController@active', $ladder->id) }}" onclick="return confirm('Apakah anda yaki mengaktifkan `{{$ladder->name}}`?')">Aktif</a>
                                    @endif
                                    <a class="btn btn-primary btn-xs" href="{{ action('LadderController@edit', $ladder->id) }}">Ubah</a>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    <a class="btn btn-xs btn-danger" href="{{ action('LadderController@delete', $ladder->id) }}" onclick="return confirm('Apakah anda yakin untuk menghapus ini?')">hapus</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>               
            </div>
            <div class="pull-right">
                {!! $ladders->render() !!} 
            </div>
        </div>
    </div>
</div>
@endsection
