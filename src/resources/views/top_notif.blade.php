@if (Session::has('UPDATE.OK'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Successfully saved
</div>
@elseif (Session::has('DELETE.OK'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Successfully deleted
</div>
@elseif (Session::has('SUSPEND.OK'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Successfully suspend
</div>
@elseif (Session::has('APPROVE.OK'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Successfully publish feedback
</div>
@elseif (Session::has('SEND.OK'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Registration successfull, please check your email to activate your account
</div>
@elseif (Session::has('UNSUSPEND.OK'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Successfully activate
</div>
@elseif (Session::has('UNUPROVE.OK'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Successfully unpublish feedback
</div>
@elseif (Session::has('UPDATE.FAIL'))
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Save failed
</div>
@elseif (Session::has('DELETE.FAIL'))
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Delete failed
</div>
@elseif (Session::has('UPD.BLOG.FAIL'))
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Permalink has been used.
</div>
@elseif (Session::has('UPD.ACCTYPE.FAIL'))
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Account type name has been used.
</div>
@elseif (Session::has('BOOK.INACTIVE'))
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Current book is non active.
</div>
@elseif (Session::has('DELETE.DEACTIVATE'))
<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Data can not be deleted because it is already used.
</div>
@elseif (Session::has('ERR.DATE'))
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	Input date and time invalid.
</div>
@endif