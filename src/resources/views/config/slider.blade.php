@extends('master', ['active' => 'config'])
@section('sidebar')
@include('config.sidebar', ['active' => 'slider'])
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Slider Setting</h2>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
</div>
<tr>
    <a class="btn btn-primary" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#myModal">Add Slider</a>
</tr>
<div class="form-group table-responsive">
    <table class="table-responsive table table-bordered">
        <thead>
            <tr>
                <th style="width:5%">No</th>
                <th style="width:15%">Name</th>
                <th style="width:25%">Description</th>
                <th style="width:15%">Status</th>
                <th style="width:15%">Action</th>
            </tr>
        </thead>

            @if(count($sliders) == 0)
            <tr>
                <td colspan="7">There is no data.</td>
            </tr>
            @endif
            @foreach ($sliders as $key => $slider)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $slider->name }}</td>
                    <td>{{ $slider->description }}</td>
                    <td>{{ $slider->status }}</td>
                    <td>
                        <a class="btn btn-success btn-xs" href="{{action('ConfigController@editSlider', $slider->id)}}">Edit</a>
                        <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                        <a class="btn btn-xs btn-danger" href="{{action('ConfigController@deleteSlider', $slider->id)}}" onclick="return confirm('Are you sure you want to delete this item?')">delete</a>
                    </td>

                   
                </tr>
            @endforeach

    </table>
</div>

            

@endsection

@section('content-modal')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Slider</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ConfigController@sliderStore'),
                'enctype'=> 'multipart/form-data',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Slider Name',
            'placeholder' => 'Slider Name',
            'default' => ''
        ])

        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.option', [
                'field' => 'status',
                'label' => 'Status',
                'options' => [
                    'published' => 'Published',
                    'unpublished'  => 'Unpublished',
                    ],                             
        ])
        
        @include('form.file', [
            'field' => 'image',
            'label' => 'image',
            'placeholder' => 'image',
            
        ])

        
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection
