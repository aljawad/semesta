@extends('master', ['active' => 'config'])
@section('sidebar')
@include('config.sidebar', ['active' => 'website'])
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Website Setting</h2>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
</div>


@if (count($errors) > 0)

<div class="alert alert-danger">

    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>

        @foreach ($errors->all() as $error)

        <li>{{ $error }}</li>

        @endforeach

    </ul>

</div>

@endif


{!!
    Form::open([
        'role' => 'form',
        'url' => action('ConfigController@websiteStore'),
        'method' => 'post'
        ])
        !!}

        <!-- @include('form.text', [
            'field' => 'address_1',
            'label' => 'Address 1',
            'placeholder' => 'Address 1',
            'default' => $featured->address_1
            ])

        @include('form.text', [
            'field' => 'address_2',
            'label' => 'Address 2',
            'placeholder' => 'Address 2',
            'default' => $featured->address_2
            ])

        @include('form.text', [
            'field' => 'address_3',
            'label' => 'Address 3',
            'placeholder' => 'Address 3',
            'default' => $featured->address_3
            ])

        @include('form.text', [
            'field' => 'phone',
            'label' => 'Phone',
            'placeholder' => 'Phone',
            'default' => $featured->phone
            ])

        @include('form.text', [
            'field' => 'fax',
            'label' => 'Fax',
            'placeholder' => 'Fax',
            'default' => $featured->fax
            ]) -->

        @include('form.text', [
            'field' => 'email',
            'label' => 'Email',
            'placeholder' => 'Email',
            'default' => $featured->email
            ])

        @include('form.text', [
            'field' => 'facebook',
            'label' => 'Facebook Url',
            'placeholder' => 'Facebook Url',
            'default' => $featured->facebook
            ])

        @include('form.text', [
            'field' => 'twitter',
            'label' => 'Twitter Url',
            'placeholder' => 'Twitter Url',
            'default' => $featured->twitter
            ])

        @include('form.text', [
            'field' => 'youtube',
            'label' => 'Youtube Url',
            'placeholder' => 'Youtube Url',
            'default' => $featured->youtube
            ])

        @include('form.text', [
            'field' => 'instagram',
            'label' => 'instagram Url',
            'placeholder' => 'Instagram Url',
            'default' => $featured->instagram
            ])

        @include('form.text', [
            'field' => 'video_url',
            'label' => 'Video Url',
            'placeholder' => 'Example: https://www.youtube.com/watch?v=whMk8aC0IVw',
            'default' => $featured->video_url
            ])
<hr>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}

            @endsection