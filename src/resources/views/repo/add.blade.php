@extends('master', ['active' => 'repo'])
@section('sidebar')
 	@include('repo.sidebar', ['active' => 'add'])
@endsection
@section('content')

		<div class="row">
            <div class="col-sm-6" style="margin-bottom: 20px">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false"  data-target="#addContentpdf">PDF Content</a>
            </div>
            <div class="col-sm-6">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false"  data-target="#addContentvideo">  Video Content</a>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-sm-6" style="margin-bottom: 20px">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false"  data-target="#addContenthtml">  Html/Video Content</a>
            </div>
            <div class="col-sm-6">
                 <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false"  data-target="#addContentquiz">  Quiz Content</a>
            </div>
        </div> -->

@endsection

@section('content-modal')

<div class="modal fade" id="addContentpdf" tabindex="-1" role="dialog" aria-labelledby="addContentpdfLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Tambah Konten PDF</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('RepoController@pdfstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Nama',
            'placeholder' => 'Nama',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="PDF">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Deskripsi',
            'placeholder' => 'Deskripsi',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContentvideo" tabindex="-1" role="dialog" aria-labelledby="addContentvideoLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Tambah Konten Video</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('RepoController@videostore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Nama',
            'placeholder' => 'Nama',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Video">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Deskripsi',
            'placeholder' => 'Deskripsi',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContenthtml" tabindex="-1" role="dialog" aria-labelledby="addContentvideoLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Html Video Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('RepoController@htmlstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Html/Video">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContentquiz" tabindex="-1" role="dialog" aria-labelledby="addContentquizLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Quiz Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('RepoController@quizstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Quiz">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection