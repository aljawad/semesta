@extends('layouts.frontend')

@section('content')
<div class="col-sm-12 col-xs-12 sign-in-header">
    @include('layouts._flash')
    <div class="container">
        <div class="col-sm-8 col-sm-offset-2">
            <br><br><br>
            <ul class="nav nav-tabs nav-justified tab-sign-in" role="tablist">
                <li role="presentation"  class="active"><a href="#sign_up" aria-controls="sign_up" role="tab" data-toggle="tab">Sign In</a></li>
                <li role="presentation"><a href="#sign_in" aria-controls="sign_in" role="tab" data-toggle="tab">Sign Up</a></li>
            </ul>
            <div class="tab-content tab-content-sign-in">
                <div role="tabpanel" class="tab-pane fade in active" id="sign_up">
                    <div class="sign-in-content">
                        <h3>Sign in to your account</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email address">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                                {{-- <div class="form-group">
                                    <div class="col-md-6">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember"> Ingat saya
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-sign-in">Sign In</button>
                                </div>
                            </form>
                            <hr>
                            <h5>or sign in with one of these service</h5>
                            <a href="redirect" class="btn btn-default btn-other-sign" style="margin-right: 20px;"><img src="/images/fb-color.png"><span>Facebook</span></a>
                            <a href="googleauth" class="btn btn-default btn-other-sign"><img src="/images/google.png"><span>Google</span></a>
                            <hr>
                            <p><a href="{{ url('/password/reset')}}">Forgot password?</a></p>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="sign_in">
                        <div class="sign-in-content">
                            <h3>Sign Up to Semesta Web TV</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore</p>
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">Name</label>
                                        <div class="col-sm-10">
                                            <input id="name" type="text" class="form-control" name="name" placeholder="Your name" value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input id="email" type="email" class="form-control" placeholder="Email address" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gender" class="col-sm-2 control-label">Gender</label>
                                        <div class="col-sm-10">
                                            {!! Form::select('gender', ['male'=>'male', 'female'=>'female'],null, [
                                            'class'=>'form-control']) !!}
                                            {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-2 control-label">Phone</label>
                                        <div class="col-sm-10">
                                            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone number" required>
                                            @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password-confirm" class="col-sm-2 control-label">Confirm Password</label>
                                        <div class="col-sm-10">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            {!! app('captcha')->display() !!}
                                            {!! $errors->first('g-recaptcha-response', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-default btn-sign-in">Sign Up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
