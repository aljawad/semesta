@extends('layouts.frontend')

<!-- Main Content -->
@section('content')
<div class="col-sm-12 col-xs-12 sign-in-header">
    <div class="container">
        <div class="underlined-title text-center">
            <br><br><br>
            <h3>Forgot Password</h3>
            <hr>
        </div>
        <div class="col-sm-6 col-sm-offset-3">
            <p class="text-center">Enter your registered email address, we will send the reset link to yor inbox.</p>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control input-lg" placeholder="Email address" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-custom btn-lg">
                            Send Password Reset Link
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
