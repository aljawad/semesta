<p><b>Confirmation of your account with SAAS</b></p>
<p>
	Thank you for signing up with Semesta Web TV!
	Please kindly confirm your account by clicking below URL and for further activity:
</p>

		<div style="margin-top: 20px">
            <p>
			<a href="{{ $link = url('auth/verify', $token).'?email='.urlencode($user->email) }}"> {{ $link }} </a>
			</p>
        </div>



<div style="margin-top: 10px">
        <p>Happy Learning!</p>
        <p>SAAS</p>
        <p>===</p>
</div>

	<p>If you have any inquiries, please kindly email us  at {{config('settings.email')}}.</p>
  

    <div style="font-size: 11px; margin-top: 10px">

    This email was sent automatically by SAAS.

    </div>