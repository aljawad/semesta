@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {!! $errors->has('gender') ? 'has-error' : '' !!}">
                          {!! Form::label('gender', 'Gender', ['class'=>'col-md-4 control-label']) !!}
                          <div class="col-md-4">
                        {!! Form::select('gender', ['male'=>'male', 'female'=>'female'],null, [
                          'class'=>'js-selectize col-md-6']) !!}
                            {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                          </div>
                        </div>

                        <div class="form-group {!! $errors->has('city_id') ? 'has-error' : '' !!}">
                          {!! Form::label('ladder_id', 'City', ['class'=>'col-md-4 control-label']) !!}
                          <div class="col-md-4">
                        {!! Form::select('ladder_id', [''=>'']+App\Ladder::pluck('name','id')->all(), null, [
                          'class'=>'js-selectize col-md-6 js-example-basic-single']) !!}
                            {!! $errors->first('ladder_id', '<p class="help-block">:message</p>') !!}
                          </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {!! $errors->has('city_id') ? 'has-error' : '' !!}">
                          {!! Form::label('city_id', 'City', ['class'=>'col-md-4 control-label']) !!}
                          <div class="col-md-4">
                        {!! Form::select('city_id', [''=>'']+App\City::pluck('name','id')->all(), null, [
                          'class'=>'js-selectize col-md-6 js-example-basic-single']) !!}
                            {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
                          </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                            <div class="col-md-offset-4 col-md-6">
                                {!! app('captcha')->display() !!}
                                {!! $errors->first('g-recaptcha-response', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-js')
    <script type="text/javascript">
    $(document).ready(function() {
      $(".js-example-basic-single").select2();
    });
    </script>
@endsection