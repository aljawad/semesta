@extends('master', ['active' => 'subject'])
@section('sidebar')
    @include('subject.sidebar', ['active' => 'subject'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Ubah Mata Pelajaran</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/admin/subject') }}">Kembali</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('SubjectController@update',[$subject->id]),
                    'enctype'=> 'multipart/form-data', 'files',
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Nama',
                'placeholder' => 'Nama',
                'default' => $subject->name
            ])

            @include('form.text', [
                'field' => 'url',
                'label' => 'Url video',
                'placeholder' => 'Url Youtube',
                'default' => $subject->url
            ])

            @include('form.file', [
                'field' => 'images',
                'label' => 'Thumbnail',
                'placeholder' => 'Thumbnail',
                'default' => '',
                'hidden-value' => $subject->images
            ])

            <img class="thumbnail" src="{{ action('SubjectController@images', $subject->images) }}" height="170" style="display:inline; margin-right:20px;">
                
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>

            {!! Form::close() !!}


@endsection

