@extends('master', ['active' => 'subject'])
@section('sidebar')
 	@include('subject.sidebar', ['active' => 'subject'])
@endsection
@section('content')

<div class="container-fluid">
    <div class="col-xs-12"> 
    @include('form.search',['url'=>'/admin/subject','link'=>'/admin/subject'])
    </div>
   
        <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>Aksi</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($subjects) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($subjects as $key => $subject)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $subject->name }}</td>
                                <td>
                                    @if($subject->status=='active')
                                    <a class="btn-success btn-xs" disabled>Aktif</a>
                                    
                                    @else
                                    <a class="btn-danger btn-xs" disabled>Tidak Aktif</a>
                                    @endif
                                </td>
                                <td>
                                @if($subject->status=='active')
                                    <a class="btn btn-xs btn-warning" href="{{ action('SubjectController@inactive', $subject->id) }}" onclick="return confirm('Apakah anda yakin menonaktifkan `{{$subject->name}}`?')">Tidak Aktif</a>
                                    @else
                                    <a class="btn btn-xs btn-success" href="{{ action('SubjectController@active', $subject->id) }}" onclick="return confirm('pakah anda yakin mengaktifkan `{{$subject->name}}`?')">Aktif</a>
                                    @endif
                                    <a class="btn btn-primary btn-xs" href="{{ action('SubjectController@edit', $subject->id) }}">Ubah</a>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    <a class="btn btn-xs btn-danger" href="{{ action('SubjectController@delete', $subject->id) }}" onclick="return confirm('Apakah anda yakin menghapus ini?')">hapus</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div>
                <div class="pull-right">
                {!! $subjects->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
