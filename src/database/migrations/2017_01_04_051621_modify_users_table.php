<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('gender', ['Male', 'Female'])->nullable();;
            $table->string('phone')->nullable();
            $table->string('avatar')->nullable();
            $table->boolean('suspended');
            // $table->integer('sekolah_id')->nullable();
            $table->string('role')->default('member');
            $table->string('verification_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // $table->dropForeign(['ladder_id', 'city_id']);
            // $table->dropForeign('users_city_id_foreign');
            // $table->dropForeign('users_ladder_id_foreign');

            // $table->dropColumn('gender');
            $table->dropColumn('phone');
            $table->dropColumn('avatar');
            $table->dropColumn('suspended')->default(0);
            $table->dropColumn('verification_token');

        });
    }
}
