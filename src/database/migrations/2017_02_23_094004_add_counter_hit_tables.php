<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCounterHitTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('counters', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('ip_address');    
            $table->integer('section_id')->unsigned();
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('counters');
    }
}
