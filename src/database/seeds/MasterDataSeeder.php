<?php

use Illuminate\Database\Seeder;
use App\City;
use App\Ladder;
use App\Subject;
use App\Slider;
use App\Setting;

class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //create sample ladder
      $ladder1 = Ladder::create(['name'=>'SD']);
      $ladder2 = Ladder::create(['name'=>'SMP']);
      $ladder3 = Ladder::create(['name'=>'SMA']);

      //create sample subject
      $subject1 = Subject::create(['name'=>'Matematika', 'images' => 'phpFHnNCa.jpg', 'url' => 'https://www.youtube.com/embed/67i4SOb-2pA']);
      $subject2 = Subject::create(['name'=>'Fisika', 'images' => 'phpeAJaGH.jpg', 'url' => 'https://www.youtube.com/embed/67i4SOb-2pAE']);
      $subject3 = Subject::create(['name'=>'Biologi', 'images' => 'phpFgCmIN.jpg', 'url' => 'https://www.youtube.com/embed/67i4SOb-2pA']);
      $subject4 = Subject::create(['name'=>'Ips', 'images' => 'phpFEOKpI.jpg', 'url' => 'https://www.youtube.com/embed/67i4SOb-2pA']);
      $subject5 = Subject::create(['name'=>'Inggris', 'images' => 'phpomDEco.jpg', 'url' => 'https://www.youtube.com/embed/67i4SOb-2pA']);

      //create sample slider
      $slider1 = Slider::create(['name' => 'slider 1', 'image' => 'slider-1.jpg', 'description' => 'sample description', 'status' => 'published']);
      $slider2 = Slider::create(['name' => 'slider 2', 'image' => 'slider-2.jpg', 'description' => 'sample description', 'status' => 'published']);
      $slider3 = Slider::create(['name' => 'slider 3', 'image' => 'slider-3.jpg', 'description' => 'sample description', 'status' => 'published']);


      //create sample setting
      $setting = Setting::create([
        'featured_subject_1' => '1',
        'featured_subject_2' => '2',
        'featured_subject_3' => '3',
        'featured_subject_4' => '4',
        'address_1' => 'Jl. Lorem ipsum no. 43',
        'address_2' => 'Lorem ipsum dolor si amet',
        'address_3' => 'Jakarta',
        'phone' => '(021) 534533',
        'fax' => '(021) 537373',
        'email' => 'info@semestaweb.tv',
        'facebook' => 'https://www.facebook.com/semestawebtv/',
        'twitter' => 'https://twitter.com/semestawebtv',
        'youtube' => 'https://www.youtube.com/channel/UCB4Sgqm3PD3eVywSIlfx8cQ',
        'instagram' => 'https://www.instagram.com/semestawebtv/',
        'video_url' => 'https://www.youtube.com/embed/67i4SOb-2pA'
        ]);
    }
  }
