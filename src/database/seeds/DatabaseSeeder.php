<?php

use Illuminate\Database\Seeder;
use App\City;
use App\Ladder;
use App\Subject;
use App\Section;
use App\Course;
use App\Chapter;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MasterDataSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CourseSeeder::class);
    }
}
