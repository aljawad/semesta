<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Membuat role admin
      $adminRole = new Role();
      $adminRole->name = "admin";
      $adminRole->display_name = "Admin";
      $adminRole->save();

      // Membuat role teacher
      $teacherRole = new Role();
      $teacherRole->name = "teacher";
      $teacherRole->display_name = "Teacher";
      $teacherRole->save();

      // Membuat role member
      $memberRole = new Role();
      $memberRole->name = "member";
      $memberRole->display_name = "Member";
      $memberRole->save();

      // Membuat sample admin
    	$admin = new User();
    	$admin->name = 'Administrator';
    	$admin->email = 'admin@admin.com';
    	$admin->password = bcrypt('password');
      $admin->role = 'admin';
      $admin->suspended = 1;
    	$admin->save();
    	$admin->attachRole($adminRole);
// 

      // Membuat sample teacher
    	$teacher = new User();
    	$teacher->name = 'Math Teacher';
    	$teacher->email = 'math@admin.com';
    	$teacher->password = bcrypt('password');
    	$teacher->role = 'teacher';
      $teacher->suspended = 1;
      // $teacher->sekolah_id = 2;
      $teacher->save();
    	$teacher->attachRole($teacherRole);

      $teacher = new User();
      $teacher->name = 'Physics Teacher';
      $teacher->email = 'physics@admin.com';
      $teacher->password = bcrypt('password');
      $teacher->role = 'teacher';
      $teacher->suspended = 1;
      // $teacher->sekolah_id = 2;
      $teacher->save();
      $teacher->attachRole($teacherRole);


      // Membuat sample member
      $faker = Faker\Factory::Create();

      for ($i=0; $i < 20; $i++) {
        $member = new User();
        $member->name = $faker->name;
        $member->email = $faker->email;
        $member->password = bcrypt('password');
        $member->role = 'member';
        $member->gender = array_rand(['Male' => 1, 'Female' => 1],1);
        $member->suspended = 1;
        $member->save();
        $member->attachRole($memberRole);
      }
    }
}
