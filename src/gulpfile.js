process.env.DISABLE_NOTIFIER = true;
var gulp = require('gulp');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
 var bowerDir = './resources/assets/bower/';




elixir(function(mix) {
    mix.scripts(
		[
			"jquery.min.js",
			"bootstrap.js",
			"bootstrap-notify.min.js",
			"bootstrap-datepicker.js",
			"mustache.js",
			"is.js",
			"app.js",
			"gridlist.js",
			"commonmark.min.js",
			"jquery-ui.min.js",
			"jquery.mjs.nestedSortable.js",
			"create_picklist.js",
			"select2.min.js",
			"shr.js",
		],
		"public/js/app.js"
	).less(
		[
			"app.less",
			"jquery-ui.min.less"
		],
		"public/css/app.css"
	).less(
		[
			"style.less",
			"gridlist.less",
			"custom.less",
			"media-queries.less",
			"media-news.less"
		],
		"public/css/custom.css"
	).less(
		[
			"frontend.less",
			"shr.css",
		],
		"public/css/frontend.css"
	).less(
		[
			"font-awesome.less"
		],
		"public/css/font-awesome.css"
	).less(
		[
			"contact-us.less"
		],
		"public/css/contact-us.css"
	)
	.version(
		[
			"js/app.js",
			"css/app.css",
			"css/font-awesome",
			"css/custom.css"
		],
		"public/build"
	).copy(
		"resources/fonts",
		"public/build/fonts"
	).copy(
		"resources/assets/less/images",
		"public/build/css/images"
	).copy(
    "resources/assets/bower",
    "public/bower"
  );
});
